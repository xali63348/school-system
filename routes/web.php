<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\UserController;
use App\Http\Controllers\HomeController;
use App\Http\Controllers\ClassesController;
use App\Http\Controllers\SchoolController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\StudentController;
use App\Http\Controllers\AbsenceController;
use App\Http\Controllers\TeachersController;
use App\Http\Controllers\TableController;
use App\Http\Controllers\PDFController;
use App\Http\Controllers\NotificationsController;
use App\Http\Controllers\ExamScheduleController;
use App\Http\Controllers\ExamResultsController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

// add PreventBackHistory
Route::group(['middleware'=>'PreventBackHistory'],function(){ //هذه الداله تقوم بعمل رفرش كل ثانيه لمتصفح

    // school Management
    Route::get('/', [HomeController::class, 'home']);
        Route::group(['middleware'=>'auth'],function(){
            Route::get('/home', [HomeController::class, 'dashboard'])->name('home');
        });
        
    Route::prefix('users')->group(function(){ 
        Route::get('/login',[UserController::class,'login'])->name('login');
        Route::post('/login',[UserController::class,'logintry']);
        Route::get('/register',[UserController::class,'store']);
        Route::post('/store',[UserController::class,'create']);

        Route::group(['middleware'=>'auth'],function(){
            Route::get('/logout',[UserController::class,'logout']);

            Route::group(['middleware'=>'CustemAuth'],function(){
                Route::get('/profile',[UserController::class,'profile']);
                Route::get('/show/status',[UserController::class,'status']);
                Route::post('/edit/name/{user}',[UserController::class,'editname']);
                Route::post('/edit/numberphone/{user}',[UserController::class,'numberphone']);
                Route::post('/edit/email/{user}',[UserController::class,'email']);
                Route::post('/edit/password/{user}',[UserController::class,'password']);
            });

        });
    });
    Route::group(['prefix'=>'class','middleware'=>'auth','middleware'=>'CustemAuth'],function(){
        Route::post('/create/{school}',[ClassesController::class,'store']);
        Route::get('/show/{classes}',[ClassesController::class,'show']);
        Route::get('/delete/{classes}',[ClassesController::class,'destroy']);
        Route::get('/search/{school}',[ClassesController::class,'search']);
        Route::post('/edit/{classes}',[ClassesController::class,'update']);

    });
    Route::group(['prefix'=>'schools','middleware'=>'auth'],function(){
        Route::get('/create',[SchoolController::class,'create']);
        Route::post('/create',[SchoolController::class,'store']);
        
        Route::group(['middleware'=>'CustemAuth'],function(){
            Route::get('/show',[SchoolController::class,'show']);
            Route::get('/edit/profile',[SchoolController::class,'edit']);
            Route::post('/edit/profile/{school}',[SchoolController::class,'update']);
            Route::get('/date',[SchoolController::class,'date']);
            Route::get('/delete',[SchoolController::class,'destroy']);
        });
    });
    Route::group(['prefix'=>'category','middleware'=>'auth','middleware'=>'CustemAuth'],function(){
        Route::post('/create/{classes}',[CategoryController::class,'store']);
        Route::get('/delete/{category}',[CategoryController::class,'destroy']);
        Route::get('/search/{classes}',[CategoryController::class,'search']);
        Route::post('/edit/{category}',[CategoryController::class,'update']);
        Route::get('/show/{category}',[CategoryController::class,'show']);
    });
    Route::group(['prefix'=>'students','middleware'=>'auth','middleware'=>'CustemAuth'],function(){
        Route::get('/show/{category}',[StudentController::class,'show']);
        Route::get('/create/{category}',[StudentController::class,'create']);
        Route::post('/create/{category}',[StudentController::class,'store']);
        Route::get('/delete/{student}',[StudentController::class,'destroy']);
        Route::get('/edit/{student}',[StudentController::class,'edit']);
        Route::post('/edit/{student}',[StudentController::class,'update']);
        Route::get('/search/{category}',[StudentController::class,'search']);
    });
    Route::group(['prefix'=>'absences','middleware'=>'auth','middleware'=>'CustemAuth'],function(){
        Route::get('/show/{category}',[AbsenceController::class,'show']);
        Route::get('/search/{category}',[AbsenceController::class,'search']);
        Route::get('/create/{category}',[AbsenceController::class,'create']);
        Route::post('/create/{student}',[AbsenceController::class,'store']);
        Route::get('/update/{absence}',[AbsenceController::class,'edit']);
        Route::post('/update/{absence}',[AbsenceController::class,'update']);
        Route::get('/search1/{category}',[AbsenceController::class,'search1']);
        
    });
    Route::group(['prefix'=>'teachers','middleware'=>'auth','middleware'=>'CustemAuth'],function(){
        Route::get('/show/{category}',[TeachersController::class,'show']);
        Route::get('/create/{category}',[TeachersController::class,'create']);
        Route::post('/create/{category}',[TeachersController::class,'store']);
        Route::get('/search/{category}',[TeachersController::class,'search']);
        Route::post('/add/{category}',[TeachersController::class,'add']);
        Route::get('/edit/{teacher}/{category}',[TeachersController::class,'edit']);
        Route::post('/edit/{teacher}',[TeachersController::class,'update']);
        Route::get('/delete/{teacher}/{category}',[TeachersController::class,'destroy']);
        Route::get('/permanently/delete/{teacher}/{category}',[TeachersController::class,'permanently']);
    });
    Route::group(['prefix'=>'tables','middleware'=>'auth','middleware'=>'CustemAuth'],function(){
        Route::get('/show/{category}',[TableController::class,'show']);
        Route::get('/create/{category}',[TableController::class,'create']);
        Route::post('/create/{category}',[TableController::class,'store']);
        Route::get('/delete/{table}',[TableController::class,'destroy']);
        Route::get('/edit/{table}',[TableController::class,'edit']);
        Route::post('/edit/{table}',[TableController::class,'update']);
    });
    
    Route::prefix('pdf')->group(function(){
        Route::get('/download/table/{tables}',[PDFController::class,'generatePDF']);
        Route::get('/download/exam_schedules/{examschedule}',[PDFController::class,'generatePDF2']);
    });

    Route::group(['prefix'=>'notifications','middleware'=>'auth','middleware'=>'CustemAuth'],function(){
    Route::get('/show/{category}',[NotificationsController::class,'show']);
    Route::post('/create/{category}',[NotificationsController::class,'store']);
    Route::get('/delete/{notific}',[NotificationsController::class,'destroy']);
    });

    Route::group(['prefix'=>'examschedules','middleware'=>'auth','middleware'=>'CustemAuth'],function(){
        Route::get('/show/{category}',[ExamScheduleController::class,'show']);
        Route::get('/create/{category}',[ExamScheduleController::class,'create']);
        Route::post('/create/{category}',[ExamScheduleController::class,'store']);
        Route::get('/edit/{examschedule}',[ExamScheduleController::class,'edit']);
        Route::post('/edit/{examschedule}',[ExamScheduleController::class,'update']);
        Route::get('/delete/{examschedule}',[ExamScheduleController::class,'destroy']);
        Route::get('/search/{category}',[ExamScheduleController::class,'search']);
    });

    Route::group(['prefix'=>'examresults','middleware'=>'auth','middleware'=>'CustemAuth'],function(){
        Route::get('/show/{category}',[ExamResultsController::class,'show']);
        Route::get('/create/{category}',[ExamResultsController::class,'create']);
        Route::post('/create/{student}',[ExamResultsController::class,'store']);
        Route::get('/search/{category}',[ExamResultsController::class,'search']);
        Route::get('/search1/{category}',[ExamResultsController::class,'search1']);
        Route::get('/delete/{examresults}',[ExamResultsController::class,'destroy']);
        Route::get('/edit/{examresults}',[ExamResultsController::class,'edit']);
        Route::post('/edit/{examresults}',[ExamResultsController::class,'update']);
    });

    // vister
    Route::prefix('student')->group(function(){
        Route::get('/search', [HomeController::class,'search']);
        Route::get('/absences/{student}', [HomeController::class,'absences']);
        Route::get('/profile/{student}', [HomeController::class,'profile']);
        Route::get('/school/{student}', [HomeController::class,'school']);
        Route::get('/notifications/{student}', [HomeController::class,'notifications']);
        Route::get('/table/{student}', [HomeController::class,'table']);
        Route::get('/examschedules/{student}', [HomeController::class,'examschedules']);
        Route::get('/examresults/{student}', [HomeController::class,'examresults']);
    });
    Route::prefix('teacher')->group(function(){
        Route::get('/search', [HomeController::class,'Tsearch']);
        Route::get('/profile/{teacher}', [HomeController::class,'Tprofile']);
        Route::get('/school/{teacher}', [HomeController::class,'Tschool']);
        Route::get('/notifications/{teacher}', [HomeController::class,'Tnotifications']);
        Route::get('/table/{teacher}', [HomeController::class,'Ttable']);
        Route::get('/examschedules/{teacher}', [HomeController::class,'Texamschedules']);
    });
        
    // Route::get('/test', [HomeController::class, 'test']);

    // Auth::routes();
    // Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

});
// end PreventBackHistory