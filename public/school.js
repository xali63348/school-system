var animation = bodymovin.loadAnimation({
  container: document.getElementById('animContainer'),
  renderer: 'svg',
  loop: true,
  autoplay: true,
  path: 'https://assets4.lottiefiles.com/packages/lf20_JYe5t7/data2.json' // lottie file path
})