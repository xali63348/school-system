
var animation = bodymovin.loadAnimation({
    container: document.getElementById('animerror'),
    renderer: 'svg',
    loop: true,
    autoplay: true,
    path: 'https://assets9.lottiefiles.com/packages/lf20_aiphuevx.json' // lottie file path
})
