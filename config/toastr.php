<?php

return [
    'options' => [
        "closeButton" => true,
        "debug" => true,
        "newestOnTop" => true,
        "progressBar" => false,
        "positionClass" => "toast-bottom-center",
        "preventDuplicates" => false,
        // "onclick" => null,
        "showDuration" => "300",
        "hideDuration" => "1000",
        "timeOut" => "6000",
        "extendedTimeOut" => "1000",
        "showEasing" => "swing",
        "hideEasing" => "linear",
        "showMethod" => "fadeIn",
        "hideMethod" => "fadeOut"

    ],
];
