<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;

class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        \App\Models\School::factory()->count(1)->create();
        \App\Models\User::factory()->count(1)->create();
        \App\Models\Classes::factory()->count(3)->create();
        \App\Models\Category::factory()->count(10)->create();
        \App\Models\Student::factory()->count(100)->create();
    }
}
