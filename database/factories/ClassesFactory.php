<?php

namespace Database\Factories;

use App\Models\Classes;
use App\Models\School;
use Illuminate\Database\Eloquent\Factories\Factory;

class ClassesFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Classes::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'supervisor' => $this->faker->name(),
            'school_id'=> School::all()->random()->id
        ];
    }
}
