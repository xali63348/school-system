<?php

namespace Database\Factories;

use App\Models\School;
use App\Models\User;
use Illuminate\Database\Eloquent\Factories\Factory;

class SchoolFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = School::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'manager' => $this->faker->name(),
            'address'=>([
                'country'=>$this->faker->city(),
                'city'=>$this->faker->city(),
                'area'=>rand(1,8),
                'extra'=>rand(1,25),
            ]),
            
        ];
    }
}
