<?php

namespace Database\Factories;

use App\Models\Student;
use App\Models\Category;
use Illuminate\Database\Eloquent\Factories\Factory;

class StudentFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Student::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name'=>$this->faker->firstNameMale(),
            'middle_name'=>$this->faker->firstNameMale(),
            'last_name'=>$this->faker->lastName(),                                  
            'age'=>$this->faker->postcode(),                            
            'address'=>([
                'country'=>$this->faker->country(),
                'city'=>$this->faker->city(),                                
                'area'=>rand(1,8),
                'extra'=>rand(1,25),
            ]),
            'number_phone'=>$this->faker->phoneNumber(),
            'number_code'=>$this->faker->e164PhoneNumber(),
            'category_id'=>Category::all()->random()->id,
        ];
    }
}
