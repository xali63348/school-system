<?php

namespace Database\Factories;

use App\Models\Category;
use App\Models\Classes;
use Illuminate\Database\Eloquent\Factories\Factory;

class CategoryFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Category::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        return [
            'name' => $this->faker->company(),
            'table' => $this->faker->company(),
            'teachers' => $this->faker->name(),
            'student' => $this->faker->name(),
            'absence_record' => $this->faker->name(),
            'classes_id'=> Classes::all()->random()->id
        ];
    }
}
