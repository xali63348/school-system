<style>
    .main-content {
        padding-top: 2px;
        padding-bottom: 2px;
    }

    .info-card {
        background: #fff;
        text-align: center;
        padding: 50px 30px;
        margin-bottom: 30px;
        border-radius: 3px;
        box-shadow: 0 10px 30px rgba(0, 0, 0, 0.1);
    }

    .info-card .info-card_icon {
        height: 125px;
        width: 125px;
        margin: 0 auto 50px auto;
        border: 5px solid #4caf50;
        border-radius: 125px;
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;
    }

    .info-card .info-card_icon i {
        font-size: 50px;
        color: #4caf50;
    }

    .info-card .info-card_icon .info-card_img-icon {
        height: 60px;
        width: 60px;
        object-fit: contain;
    }

    .info-card .info-card_label {
        margin-bottom: 15px;
    }

    .info-card .info-card_message {
        margin-bottom: 15px;
    }

    .info-card .btn {
        background: #03a9f4;
        border-color: #03a9f4;
        box-shadow: 0 3px 10px rgba(0, 0, 0, 0.1);
    }

    .info-card--warning .info-card_icon {
        border-color: #ff9800;
    }

    .info-card--warning .info-card_icon i {
        color: #ff9800;
    }
</style>
<section class="main-content">
    <div class="container">
        <div class="row">
            <div class="col-sm-8 offset-sm-2 col-md-6 offset-md-3 col-lg-5 offset-lg-3 offset-xl-4 col-xl-4">
                <div class="info-card info-card--warning">
                    <div class="info-card_icon">
                        <img class="info-card_img-icon" src="/images/warning.svg" alt="warning">
                    </div>
                    <h2 class="info-card_label">تنبيه</h2>
                    <div class="info-card_message">لا يمكنك إضافة نتائج الامتحانات للطلاب لأنك لم تقم بإضافتها
                    أي طلاب. الرجاء الانتقال إلى صفحة إضافة طلاب</div>
                    <a class="btn btn-primary" href="/students/show/{{$category->id}}">انتقل إلى إنشاء طالب</a>

                </div>
            </div>
        </div>
    </div>