<ul class="nav nav-pills justify-content-center">
    <li class="nav-item">
        <a class="nav-link {{$information ?? ''}}" aria-current="page" href="/users/profile"> معلومات الحساب</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{$profile ?? ''}}" href="/schools/edit/profile"> الملف الشخصي</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{$status ?? ''}}" href="/users/show/status">الحالة</a>
    </li>
    <li class="nav-item">
        <a class="nav-link {{$date ?? ''}}" href="/schools/date">اعدادات الحذف</a>
    </li>
</ul>