
<section class="main-content">
		<div class="container">
			<div class="row">
                <div class="col-sm-6 offset-sm-4 col-md-4 offset-md-4 col-lg-4 offset-lg-4 offset-xl-4 col-xl-4">
					<div class="info-card info-card--warning">
						<div class="info-card_icon">
							<img class="info-card_img-icon" src="/images/warning.svg" alt="warning">
						</div>
						<h2 class="info-card_label">Notice</h2>
						<div class="info-card_message">Note, you do not have any table, add a table</div>
						<a class="btn btn-primary" href="/tables/create/{{$category->id}}">Create Table</a>
					</div>
				</div>
			</div>
		</div>
	</section>
