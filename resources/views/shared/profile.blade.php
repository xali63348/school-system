<style>
    .banner {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 175px;
        background-position: center;
        background-size: cover !important;
    }

    .img-circle {
        height: 180px;
        width: 180px;
        border-radius: 150px;
        border: 3px solid #fff;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        z-index: 1;
        margin-top: 40px;
    }

    .shadow {
        box-shadow: 0 5px 20px rgba(0, 0, 0, 0.06) !important;
    }
</style>

<div class="profile-card card rounded-lg shadow   mb-4 text-center position-relative overflow-hidden">
    <div class="banner">
        @isset(auth()->user()->school->background)
            <img src="{{asset(auth()->user()->school->background)}}" alt="" class="banner">
        @endisset
        @empty(auth()->user()->school->background)
            <img src="/images/school-background.jpg" alt="" class="banner">
        @endempty
    </div>
    @isset(auth()->user()->school->image)
        <img src="{{asset(auth()->user()->school->image)}}" alt="" class="img-circle mx-auto mb-3">
    @endisset
    @empty(auth()->user()->school->image)
        <img src="/images/user.png" alt="" class="img-circle mx-auto mb-3">
    @endempty
   
    <h3 class="mb-1">{{auth()->user()->school->name}}</h3>
    <div class="text-left mb-4">
        <p class="mb-2">المدير: {{auth()->user()->school->manager}}</p>
        <p class="mb-2"> {{auth()->user()->number_phone}}</p>
        <p class="mb-2"> {{auth()->user()->school->address['country']}}, {{auth()->user()->school->address['city']}},
            {{auth()->user()->school->address['area']}}, {{auth()->user()->school->address['extra']}}</p>
    </div>
</div>