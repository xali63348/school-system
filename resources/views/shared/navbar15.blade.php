<nav class="navbar navbar-expand-md navbar-dark bg-dark" aria-label="Fourth navbar example">
    <div class="container-fluid">
        <a href="/examresults/show/{{$category->id}}"><svg xmlns="http://www.w3.org/2000/svg" width="40" height="40"
                fill="currentColor" class="bi bi-arrow-left" viewBox="0 0 16 16">
                <path fill-rule="evenodd"
                    d="M15 8a.5.5 0 0 0-.5-.5H2.707l3.147-3.146a.5.5 0 1 0-.708-.708l-4 4a.5.5 0 0 0 0 .708l4 4a.5.5 0 0 0 .708-.708L2.707 8.5H14.5A.5.5 0 0 0 15 8z" />
            </svg></a>

        <a class="navbar-brand ms-4" href="/examresults/create/{{$category->id}}">جدول النتائج</a>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarsExample04"
            aria-controls="navbarsExample04" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

        <div class="collapse navbar-collapse" id="navbarsExample04">
            <ul class="navbar-nav me-auto mb-2 mb-md-0">

            </ul>
            <form class="d-flex ms-auto my-3 my-lg-0" action="/examresults/search1/{{$category->id}}" method="get">
                @csrf
                <div class="input-group">
                    <input class="form-control" type="search" placeholder="البحث .." aria-label="Search"
                        name="search" />
                    <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                </div>
            </form>
        </div>
    </div>
</nav>