@extends('layouts.app1')
@section('styles')
<style>

    #animerror {
        width: 400px;
        height: 400px;
    }
</style>
@endsection
@section('contant')
<div class="container">
    <div class="row">
        <div class="col-12 col-sm-12 col-md-8 offset-md-4 col-lg-8 offset-lg-4">
            <div id="animerror"></div>
        </div>

    </div>
</div>
@endsection