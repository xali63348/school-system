<style>
    .learn {
        background-color: white;
    }

    .Copyright {
        text-decoration: none;
        color: white;
    }

    .Copyright:hover {
        text-decoration: none;
        color: #3f37c9;
    }
    .fof{
        margin-top:7%;
    }
    .fa-facebook{
        font-size: 1.7em;
        color:white;
    }
    .fa-facebook:hover{
        color:#1565c0;
    }
    .fa-instagram{
        font-size: 1.8em;
        color:white;
    }
    .fa-instagram:hover{
        color:#ed1da4;
    }
    .face{
        margin-right:15px;
    }
</style>
<footer class="py-4 bg-dark text-white text-center position-relative fof">
    <div class="container">
        <p class="lead">
           <a href="" class="Copyright">علي حسين </a> 2022 &copy; جميع الحقوق محفوظة 
        </p>
    </div>
  
    <a href="https://www.facebook.com/xali63348/" class="face"><i class="fab fa-facebook"></i></a>
    <a href="https://www.instagram.com/700yz/"><i class="fab fa-instagram "></i></a>
</footer>