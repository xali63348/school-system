<!-- top navigation bar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#sidebar"
            aria-controls="offcanvasExample">
            <span class="navbar-toggler-icon" data-bs-target="#sidebar"></span>
        </button>
        <a class="navbar-brand me-auto ms-lg-0 ms-3 text-uppercase fw-bold" href="/home">نظام المدرسة</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#topNavBar"
            aria-controls="topNavBar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="topNavBar">
            <form class="d-flex ms-auto my-3 my-lg-0" method="" action="/category/search/{{$classes->id}}">
                <div class="input-group">
                    <input class="form-control" type="search" name="search" id="search" placeholder="البحث .." aria-label="Search" />
                    <button class="btn btn-primary" type="submit">
                        <i class="bi bi-search"></i>
                    </button>
                </div>
            </form>
            <ul class="navbar-nav">
                <li class="nav-item dropdown">
                    <a class="nav-link dropdown-toggle ms-2" href="#" role="button" data-bs-toggle="dropdown"
                        aria-expanded="false">
                        <i class="bi bi-person-fill"></i>
                    </a>
                    <ul class="dropdown-menu dropdown-menu-end">
                        @auth
                            <li><a class="dropdown-item" href="/users/logout">تسجيل خروج</a></li>
                            <li><a class="dropdown-item" href="/users/profile">الملف الشخصي</a></li>
                        @endauth

                    </ul>
                </li>
            </ul>
        </div>
    </div>
</nav>
<!-- top navigation bar -->
<!-- offcanvas -->
<div class="offcanvas offcanvas-start sidebar-nav bg-dark" tabindex="-1" id="sidebar">
    <div class="offcanvas-body p-0">
        <nav class="navbar-dark">
            <ul class="navbar-nav">
                <li>
                    <br>

                </li>
                <li>
                    <a href="/home" class="nav-link px-3 ">
                        <span class="me-2"><i class="fas fa-home"></i></span>
                        <span>الرئيسية</span>
                    </a>
                </li>
                <li class="my-4">
                    <hr class="dropdown-divider bg-light" />
                </li>
                <li>
                    <div class="text-muted small fw-bold text-uppercase px-3 mb-3">
                        واجهة المستخدم
                    </div>
                </li>
                <li>
                    <a href="/schools/show" class="nav-link px-3 ">
                        <span class="me-2"><i class="fas fa-chalkboard"></i></span>
                        <span>الصفوف</span>
                        <span class="ms-auto">
                        </span>
                    </a>

                </li>
                <li>
                    <a href="/schools/create" class="nav-link px-3 ">
                        <span class="me-2"><i class="fas fa-school"></i></i></span>
                        <span>المدرسة</span>
                    </a>
                </li>
                <li class="my-4">
                    <hr class="dropdown-divider bg-light" />
                </li>
                <li>

                </li>
                <li>
                    <a href="/users/profile" class="nav-link px-3 ">
                        <span class="me-2"><i class="fas fa-user-cog"></i></span>
                        <span>الملف الشخصي</span>
                    </a>
                </li>
              
            </ul>
        </nav>
    </div>
</div>
<!-- offcanvas -->