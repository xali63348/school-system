<section class="main-content">
		<div class="container">
			<div class="row">
                <div class="col-sm-6 offset-sm-4 col-md-4 offset-md-4 col-lg-4 offset-lg-4 offset-xl-4 col-xl-4">
                    <div class="info-card info-card--success">
						<div class="info-card_icon">
							<img class="info-card_img-icon" src="/images/success.svg" alt="success">
						</div>
						<h2 class="info-card_label">Well Done</h2>
						<div class="info-card_message">You can now add a schedule of lectures</div>
						<a class="btn btn-primary" href="/tables/create/{{$category->id}}">Add Lessons</a>
					</div>
				</div>
			</div>
		</div>
	</section>