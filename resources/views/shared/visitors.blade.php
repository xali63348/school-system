<!-- top navigation bar -->
<nav class="navbar navbar-expand-lg navbar-dark bg-dark fixed-top">
    <div class="container-fluid">
        <button class="navbar-toggler" type="button" data-bs-toggle="offcanvas" data-bs-target="#sidebar"
            aria-controls="offcanvasExample">
            <span class="navbar-toggler-icon" data-bs-target="#sidebar"></span>
        </button>
        <a class="navbar-brand me-auto ms-lg-0 ms-3 text-uppercase fw-bold"
            href="/student/profile/{{$student->id}}">نظام المدرسة</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#topNavBar"
            aria-controls="topNavBar" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="topNavBar">
            <form class="d-flex ms-auto my-3 my-lg-0" method="get" action="/student/search">
                <div class="input-group">
                    <input class="form-control" type="search" name="search" id="search" placeholder=".. البحث"
                        aria-label="Search" />
                    <button class="btn btn-primary" type="submit">
                        <i class="bi bi-search"></i>
                    </button>
                </div>
            </form>
        </div>
    </div>
</nav>
<!-- top navigation bar -->
<!-- offcanvas -->
<div class="offcanvas offcanvas-start sidebar-nav bg-dark" tabindex="-1" id="sidebar">
    <div class="offcanvas-body p-0">
        <nav class="navbar-dark">
            <ul class="navbar-nav">
                <li>
                    <br>

                </li>
                <li>
                    <a href="/student/profile/{{$student->id}}" class="nav-link px-3 ">
                        <span class="me-2"><i class="fas fa-user-circle"></i></span>
                        <span>{{__('messages.Profile')}}</span>
                    </a>
                </li>
                <li class="my-4">
                    <hr class="dropdown-divider bg-light" />
                </li>
                <li>
                    <div class="text-muted small fw-bold text-uppercase px-3 mb-3">
                        {{__('messages.INTERFACE')}}
                    </div>
                </li>
                <li>
                    <a href="/student/school/{{$student->id}}" class="nav-link px-3 ">
                        <span class="me-2"><i class="fas fa-school"></i></span>
                        <span>المدرسة</span>
                        <span class="ms-auto">
                        </span>
                    </a>
                </li>

                <li>
                    <a href="/student/absences/{{$student->id}}" class="nav-link px-3 ">
                        <span class="me-2"><i class="fas fa-users-slash"></i></span>
                        <span>{{__('messages.Absences')}}</span>
                        <span class="ms-auto">
                        </span>
                    </a>

                </li>

                <li>
                    <a href="/student/notifications/{{$student->id}}" class="nav-link px-3">
                        <span class="me-2"><i class="fas fa-comment"></i></i></span>
                        <span>{{__('messages.notifications')}}</span>
                    </a>
                </li>
                <li class="my-4">
                    <hr class="dropdown-divider bg-light" />
                </li>
                <li>
                    <div class="text-muted small fw-bold text-uppercase px-3 mb-3">
                        {{__('messages.TABLES')}}
                    </div>
                </li>
                <li>
                    <a href="/student/table/{{$student->id}}" class="nav-link px-3 ">
                        <span class="me-2"><i class="bi bi-table"></i></span>
                        <span>{{__('messages.Table')}}</span>
                    </a>
                </li>
                <li>
                    <a href="/student/examschedules/{{$student->id}}" class="nav-link px-3 ">
                        <span class="me-2"><i class="far fa-calendar-alt"></i></span>
                        <span>{{__('messages.Exam Schedule')}}</span>
                    </a>
                </li>
                <li>
                    <a href="/student/examresults/{{$student->id}}" class="nav-link px-3">
                        <span class="me-2"><i class="fas fa-poll"></i></span>
                        <span>{{__('messages.Exam Results')}}</span>
                    </a>
                </li>
            </ul>
        </nav>
    </div>
</div>
<!-- offcanvas -->