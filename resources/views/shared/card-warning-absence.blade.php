@extends('layouts.app1')
@section('styles')

@endsection
@section('contant')
<section class="main-content">
		<div class="container">
			<div class="row">
                <div class="col-sm-6 offset-sm-4 col-md-4 offset-md-4 col-lg-4 offset-lg-4 offset-xl-4 col-xl-4">
					<div class="info-card info-card--warning">
						<div class="info-card_icon">
							<img class="info-card_img-icon" src="/images/warning.svg" alt="warning">
						</div>
						<h2 class="info-card_label">Notice</h2>
						<div class="info-card_message">You cannot add absences to students because you have not added any students. Please go to the Add Students page</div>
						<a class="btn btn-primary" href="/students/show/{{$student->category->id}}">Go to create Student</a>
                        
					</div>
				</div>
			</div>
		</div>
	</section>
@endsection