<!DOCTYPE html>
<html dir="rtl" lang="en">

<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>تحميل ملف بي دي إف</title>
    <style>
        @font-face {
            font-family: 'arabic-font';
            url('/pdf/fonts/Tajawal-Regular.ttf') format('truetype');
            ;
        }

        body {
            font-family: 'arabic-font';
        }

        .table {
            border-collapse: collapse;
            text-align: center;
        }

        .table th {

            border-top: 2px solid #009578;
            border-bottom: 2px solid #009578;
            padding: 0.9em 1em;
        }

        table td {
            padding: 0.9em 1em;
            border-top: 2px solid #009578;
            border-bottom: 2px solid #009578;
        }

        p {
            background-color: white;
            text-align: center;
        }

        .Copyright {
            text-decoration: none;
        }
    </style>
</head>

<body>
    <div class="container-fluid">
        <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 py-3">
                <table class="table">
                    <thead>
                        <tr>
                            <th scope="col">الايام</th>
                            <th scope="col">الدرس الاول</th>
                            <th scope="col">الدرس الثاني </th>
                            <th scope="col">الدرس الثالث </th>
                            <th scope="col">الدرس الرابع </th>
                            <th scope="col">الدرس الخامس </th>
                            <th scope="col">الدرس السادس </th>
                        </tr>
                    </thead>
                    <tbody>
                        <tr>
                            <th scope="row">{{$tables->days['day1']}}</th>
                            <td>
                                {{$tables->study_materials['Lesson1']}} <br>
                                {{$tables->teachers['teacher1']}}
                            </td>
                            <td>
                                {{$tables->study_materials['Lesson2']}} <br>
                                {{$tables->teachers['teacher2']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson3']}} <br>
                                {{$tables->teachers['teacher3']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson4']}} <br>
                                {{$tables->teachers['teacher4']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson5']}} <br>
                                {{$tables->teachers['teacher5']}}

                            </td>
                            <td>
                                {{$tables->study_materials['Lesson6']}} <br>
                                {{$tables->teachers['teacher6']}}

                            </td>
                        </tr>

                        <tr>
                            <th scope="row">{{$tables->days['day2']}}</th>
                            <td>
                                {{$tables->study_materials['Lesson7']}} <br>
                                {{$tables->teachers['teacher7']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson8']}} <br>
                                {{$tables->teachers['teacher8']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson9']}} <br>
                                {{$tables->teachers['teacher9']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson10']}} <br>
                                {{$tables->teachers['teacher10']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson11']}} <br>
                                {{$tables->teachers['teacher11']}}

                            </td>
                            <td>
                                {{$tables->study_materials['Lesson12']}} <br>
                                {{$tables->teachers['teacher12']}}

                            </td>
                        </tr>

                        <tr>
                            <th scope="row">{{$tables->days['day3']}}</th>
                            <td>
                                {{$tables->study_materials['Lesson13']}} <br>
                                {{$tables->teachers['teacher13']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson14']}} <br>
                                {{$tables->teachers['teacher14']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson15']}} <br>
                                {{$tables->teachers['teacher15']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson16']}} <br>
                                {{$tables->teachers['teacher16']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson17']}} <br>
                                {{$tables->teachers['teacher17']}}

                            </td>
                            <td>
                                {{$tables->study_materials['Lesson18']}} <br>
                                {{$tables->teachers['teacher18']}}

                            </td>
                        </tr>

                        <tr>
                            <th scope="row">{{$tables->days['day4']}}</th>
                            <td>
                                {{$tables->study_materials['Lesson19']}} <br>
                                {{$tables->teachers['teacher19']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson20']}} <br>
                                {{$tables->teachers['teacher20']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson21']}} <br>
                                {{$tables->teachers['teacher21']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson22']}} <br>
                                {{$tables->teachers['teacher22']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson23']}} <br>
                                {{$tables->teachers['teacher23']}}

                            </td>
                            <td>
                                {{$tables->study_materials['Lesson24']}} <br>
                                {{$tables->teachers['teacher24']}}

                            </td>
                        </tr>

                        <tr>
                            <th scope="row">{{$tables->days['day5']}}</th>
                            <td>
                                {{$tables->study_materials['Lesson25']}} <br>
                                {{$tables->teachers['teacher25']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson26']}} <br>
                                {{$tables->teachers['teacher26']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson27']}} <br>
                                {{$tables->teachers['teacher27']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson28']}} <br>
                                {{$tables->teachers['teacher28']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson29']}} <br>
                                {{$tables->teachers['teacher29']}}

                            </td>
                            <td>
                                {{$tables->study_materials['Lesson30']}} <br>
                                {{$tables->teachers['teacher30']}}

                            </td>
                        </tr>
                        @isset($tables->days['day6'])
                        <tr>
                            <th scope="row">{{$tables->days['day6']}}</th>
                            <td>
                                {{$tables->study_materials['Lesson31']}} <br>
                                {{$tables->teachers['teacher31']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson32']}} <br>
                                {{$tables->teachers['teacher32']}}

                            </td>
                            <td>

                                {{$tables->study_materials['Lesson33']}} <br>
                                {{$tables->teachers['teacher33']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson34']}} <br>
                                {{$tables->teachers['teacher34']}}
                            </td>
                            <td>

                                {{$tables->study_materials['Lesson35']}} <br>
                                {{$tables->teachers['teacher35']}}

                            </td>
                            <td>
                                {{$tables->study_materials['Lesson36']}} <br>
                                {{$tables->teachers['teacher36']}}

                            </td>
                        </tr>
                        @endisset
                    </tbody>

                </table>
            </div>
        </div>
    </div>
    <footer class="py-4 bg-dark text-white text-center position-relative">
        <div class="container">
            <p class="lead">
               <a href="https://www.instagram.com/700yz/" class="Copyright">علي حسين </a> 2022 &copy; جميع الحقوق محفوظة
            </p>
        </div>
    </footer>
</body>
</html>