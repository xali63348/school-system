<!DOCTYPE html>
<html dir="rtl" lang="en">
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>تحميل ملف بي دي إف</title>
    <style>
        @font-face {
            font-family: 'arabic-font';
        url('/pdf/fonts/Tajawal-Regular.ttf') format('truetype');;
        }
        body{ 
            
            font-family:'arabic-font';
        }
        
        .table {
            border-collapse: collapse;
            text-align: center;
        }

        .table th {
           
            border-top: 2px solid #FC5404;
            border-bottom: 2px solid #FC5404;
            padding: 0.9em 2em;
        }
        table td{
            padding: 0.9em 3em;
            border-top: 2px solid #FC5404;
            border-bottom: 2px solid #FC5404;
        }
        h3{
            text-align:center;
        }
        table{
            margin:auto;
        }
        p{
            background-color: white;
            text-align: center;
        }
        .Copyright{
            text-decoration: none;
        }
    </style>
</head>
<body>
<div class="container-fluid">
    <div class="row">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 py-3">
                <h3>{{$examschedule->month}}</h3>
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">الدرس</th>
                                    <th scope="col">التاريخ</th>
                                    <th scope="col">اليوم </th>
                                </tr>
                        </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson1']}}</th>
                                    <td>{{$examschedule->date['date1']}}</td>
                                    <td>{{$examschedule->days['day1']}}</td>
                                   
                                </tr>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson2']}}</th>
                                    <td>{{$examschedule->date['date2']}}</td>
                                    <td>{{$examschedule->days['day2']}}</td>
                                </tr>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson3']}}</th>
                                    <td>{{$examschedule->date['date3']}}</td>
                                    <td>{{$examschedule->days['day3']}}</td>
                                   
                                </tr>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson4']}}</th>
                                    <td>{{$examschedule->date['date4']}}</td>
                                    <td>{{$examschedule->days['day4']}}</td>
                                </tr>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson5']}}</th>
                                    <td>{{$examschedule->date['date5']}}</td>
                                    <td>{{$examschedule->days['day5']}}</td>
                                   
                                </tr>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson6']}}</th>
                                    <td>{{$examschedule->date['date6']}}</td>
                                    <td>{{$examschedule->days['day6']}}</td>
                                </tr>
                                @isset($examschedule->materials['lesson7'])
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson7']}}</th>
                                    <td>{{$examschedule->date['date7']}}</td>
                                    <td>{{$examschedule->days['day7']}}</td>
                                </tr>
                                @endisset
                                @isset($examschedule->materials['lesson8'])
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson8']}}</th>
                                    <td>{{$examschedule->date['date8']}}</td>
                                    <td>{{$examschedule->days['day8']}}</td>
                                </tr>
                                @endisset
                                @isset($examschedule->materials['lesson9'])
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson9']}}</th>
                                    <td>{{$examschedule->date['date9']}}</td>
                                    <td>{{$examschedule->days['day9']}}</td>
                                   
                                </tr>
                                @endisset
                                @isset($examschedule->materials['lesson10'])
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson10']}}</th>
                                    <td>{{$examschedule->date['date10']}}</td>
                                    <td>{{$examschedule->days['day10']}}</td>
                                </tr>
                                @endisset
                                @isset($examschedule->materials['lesson11'])
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson11']}}</th>
                                    <td>{{$examschedule->date['date11']}}</td>
                                    <td>{{$examschedule->days['day11']}}</td>
                                   
                                </tr>
                                @endisset
                                @isset($examschedule->materials['lesson12'])
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson12']}}</th>
                                    <td>{{$examschedule->date['date12']}}</td>
                                    <td>{{$examschedule->days['day12']}}</td>
                                </tr>
                                @endisset
                            </tbody>
                        </table>
                    </div>
                </div>
        </div>
        <footer class="py-4 bg-dark text-white text-center position-relative">
            <div class="container">
                <p class="lead">
                    <a href="https://www.instagram.com/700yz/" class="Copyright">علي حسين </a> 2022 &copy; جميع الحقوق محفوظة
                </p>
            </div>
        </footer>
  </body>
</html>