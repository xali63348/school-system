@extends('layouts.app1')
@section('styles')
<style>
    .title {
        margin-left: 15%;
    }

    a {
        list-style: none;
        text-decoration: none;
    }

    a:hover {
        color: black;
        text-decoration: underline;
    }

    #animContainer {
        width: 100%;
        height: 100%;
    }
   body{
       background-color: #fff !important;
   }

    .input-field {
        max-width: 400px;
        width: 100%;
        background-color: #f0f0f0;
        margin: 10px 0;
        margin-right:60px !important;
        height: 55px;
        border-radius: 55px;
        display: grid;
        grid-template-columns: 15% 85%;
        padding: 0 0.4rem;
        position: relative;
    }

    .input-field i {
        text-align: center;
        line-height: 55px;
        color: #acacac;
        transition: 0.5s;
        font-size: 1.1rem;
    }

    .input-field input {
        background: none;
        outline: none;
        border: none;
        line-height: 1;
        font-weight: 600;
        font-size: 1.1rem;
        color: #333;
    }

    .input-field input::placeholder {
        color: #aaa;
        font-weight: 500;
    }

    .btn1 {
        width: 150px;
        background-color: #5995fd;
        border: none;
        outline: none;
        height: 49px;
        border-radius: 49px;
        color: #fff;
        text-transform: uppercase;
        font-weight: 600;
        margin: 10px 0;
        cursor: pointer;
        transition: 0.5s;
        margin-left: 120px;
    }
</style>
@endsection
@section('contant')
<div class="container">
    <div class="d-sm-flex align-items-center justfiy-content-center">
        <div class="col-12 col-md-10 col-lg-8 col-xl-6">
            <div id="animContainer"></div>
        </div>
        <div class="col-sm-12 col-md-12 col-lg-12 col-xl-6 mt-5 ">
            <form method='post' action="/schools/create" class="ms-5">
                @csrf
                <div class="w-50 p-1"></div>
                <h2 class="title">انشاء مدرسة</h2>
                <div class="input-field">
                    <i class="fas fa-school"></i>
                    <input type="text" name="name" id="name" placeholder="اسم المدرسة" required />
                </div>
                <div class="input-field">
                    <i class="fas fa-user"></i>
                    <input type="text" name="manager" id="manager" placeholder="اسم المدير" required />
                </div>
                <div class="input-field">
                    <i class="fas fa-globe-americas"></i>
                    <input type="text" name="country" id="country" placeholder="البلد" required />
                </div>
                <div class="input-field">
                    <i class="fas fa-city"></i>
                    <input type="text" name="city" id="city" placeholder="المدينه" required />
                </div>
                <div class="input-field">
                    <i class="fas fa-street-view"></i>
                    <input type="text" name="area" id="area" placeholder="المنطقه" required />
                </div>
                <div class="input-field">
                    <i class="fas fa-align-right"></i>
                    <input type="text" name="extra" id="extra" placeholder="اضافي" />
                </div>
                <div>
                    <p></p>
                </div>

                <input type="submit" class="btn1" value="انشاء" />
            </form>
        </div>
    </div>
</div>
@endsection
