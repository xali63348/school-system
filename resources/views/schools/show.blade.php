@extends('layouts.app')
@section('styles')
<style>
    .categories {
        color: black;
        font-size: 22px;
    }

    .categories:hover {
        color: #4E98FF;
    }

    .category {
        color: black;
        font-size: 22px;
        text-decoration: none;
    }

    .category:hover {
        color: #4E98FF;
        text-decoration: underline;
    }


    .shadow {
        box-shadow: 0 5px 20px rgba(0, 0, 0, 0.06) !important;
    }

    .main-content {
        padding-top: 40px;
        padding-bottom: 100px;
    }

    .banner {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 125px;
        background-image: url("/images/background-10.jpg");
        background-position: center;
        background-size: cover;
    }

    .img-circle {
        height: 150px;
        width: 150px;
        border-radius: 150px;
        border: 3px solid #fff;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        z-index: 1;
    }

    .btn1 {
        width: 150px;
        background-color: #5995fd;
        border: none;
        outline: none;
        height: 49px;
        border-radius: 49px;
        color: #fff;
        text-transform: uppercase;
        font-weight: 600;
        margin: 10px 0;
        cursor: pointer;
        transition: 0.5s;
        margin-left: 150px !important;
    }

    .input-field {
        max-width: 380px;
        width: 100%;
        background-color: #f0f0f0;
        margin-left: 50px;
        height: 55px;
        border-radius: 55px;
        display: grid;
        grid-template-columns: 15% 85%;
        padding: 0 0.4rem;
        position: relative;
    }

    @media(max-width: 414px) {
        .input-field {
            margin-left: 5px;
        }

        .btn1 {
            margin-left: 120px !important;
        }

    }

    @media(max-width: 320px) {
        .input-field {
            max-width: 265px;
            margin-left: 10px;
        }

        .btn1 {
            height: 48px;
            margin-left: 70px !important;
        }

    }

    .input-field i {
        text-align: center;
        line-height: 55px;
        color: #acacac;
        transition: 0.5s;
        font-size: 1.1rem;
    }

    .input-field input {
        background: none;
        outline: none;
        border: none;
        line-height: 1;
        font-weight: 600;
        font-size: 1.4rem;
        color: #333;
    }

    .empty2 {
        color: #023e8a;
        text-decoration: none;
    }

    .empty2:hover {
        text-decoration: none;
    }

    .empty {
        text-align: center;
        margin-top: 15%;
    }

    #animContainer {
        width: 20%;
        height: 20%;
    }
</style>
@endsection

@section('contant')
<main class="mt-5 pt-3">
    <a href="" data-bs-toggle="modal" data-bs-target="#addClass" class="nav-link px-3 categories ">
        <span>اضافة صف <i class="bi bi-plus-square"></i></span>
    </a>
    {{-- add class --}}
    <div class="modal fade" id="addClass" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">انشاء صف</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="row g-3" method='post' action="/class/create/{{ $school->id }}">
                        @csrf
                        <div class="input-field">
                            <i class="fas fa-chalkboard"></i>
                            <input type="text" name="name" id="name" placeholder="اسم الصف" required />
                        </div>
                        <input type="submit" class="btn1 my-5" value="انشاء" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end --}}

    <div class="container-fluid">
        <div class="row">
            @isset(auth()->user()->school->classes)
            @foreach ($classes as $classes)
            <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">
                <div
                    class="profile-card card rounded-lg shadow p-4 p-xl-5 mb-4 text-center position-relative overflow-hidden">
                    <div class="banner"></div>
                    @isset($classes->image)
                    <img src="{{ asset($classes->image) }}" alt="error" class="img-circle mx-auto mb-3">
                    @endisset
                    @empty($classes->image)
                    <img src="/images/class2.png" alt="error" class="img-circle mx-auto mb-3">
                    @endempty
                    <h3 class="mb-4"><a href="/class/show/{{ $classes->id }}" class="category">{{ $classes->name
                            }}</a>
                    </h3>
                    <div class="social-links d-flex justify-content-center">
                        <a href="/class/delete/{{ $classes->id }}" class="mx-2 btn btn-danger">حذف</a>
                        <a href="" data-bs-toggle="modal" data-bs-target="#editClass{{$classes->id}}"
                            class="mx-2 btn btn-outline-primary">تعديل</a>

                        {{-- edit class --}}
                        <div class="modal fade" id="editClass{{$classes->id}}" data-bs-backdrop="static"
                            data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="staticBackdropLabel">تعديل الصف</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="row g-3" method='post' action="/class/edit/{{$classes->id}}">
                                            @csrf
                                            <div class="input-field">
                                                <i class="fas fa-chalkboard"></i>
                                                <input type="text" name="name" id="name" placeholder="اسم الصف"
                                                    value="{{$classes->name}}" required />
                                            </div>
                                            <input type="submit" class="btn1 my-5" value="تعديل" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- end --}}
                    </div>
                </div>
            </div>
            @endforeach
            @endisset

            @empty(auth()->user()->school->classes['0'])
            <div id="animContainer"></div>
            <h2 class="empty mt-3">أضف <a class="empty2" href="">صف</a></h2>
            @endempty

        </div>
    </div>
</main>
@endsection