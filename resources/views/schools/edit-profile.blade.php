@extends('layouts.app')
@section('styles')
<style>
    .mt-4 {
        padding: 5px;
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.10) !important;
    }

    .my-2 {
        margin-bottom: 2.2rem !important;
    }

    input[type="file"] {
        display: none;
    }

    .fa-images {
        margin-top: 10px;
        margin-right: 7px;
    }

    .fa-images {
        margin-top: 10px;
        margin-right: 7px;
    }

    .choose-photo {
        margin-top: 7px;
    }

    .lab {
        color: #fff;
        background-color: #4d84e2;
        position: absolute;
        height: 40px;
        width: 180px;
        justify-content: center;
        display: flex;
        border-radius: 4px;
        cursor: pointer;
    }

    #custom-button {
      color: #fff;
        background-color: #4d84e2;
        position: absolute;
        height: 40px;
        width: 180px;
        justify-content: center;
        display: flex;
        border-radius: 4px;
        cursor: pointer;
        
    }


</style>
@endsection
@section('contant')
<main class="mt-5 pt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12">
                @include('shared.profile')
                @component('shared.nav&tabs',['profile'=>'active'])
                @endcomponent
            </div>
            <div class="col-md-10 offset-md-1">

                <form class="row g-3  mt-4" method="post" action="/schools/edit/profile/{{$school->id}}"
                    enctype="multipart/form-data">
                    @csrf
                    <div class="col-6 col-sm-6 col-md-6">
                        <label for="inputEmail4" class="form-label">اسم المدرسة</label>
                        <input type="text" class="form-control" name="name" id="name" value="{{$school->name}}"
                            placeholder="اسم المدرسة" required>
                    </div>
                    <div class="col-6 col-sm-6 col-md-6">
                        <label for="inputEmail4" class="form-label">اسم المدير</label>
                        <input type="text" class="form-control" name="manager" id="manager" value="{{$school->manager}}"
                            placeholder="اسم المدير" required>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3 py-3">
                        <input type="text" class="form-control" name="country" id="country"
                            value="{{$school->address['country']}}" placeholder="البلد" required>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3 py-3">
                        <input type="text" class="form-control" name="city" id="city"
                            value="{{$school->address['city']}}" placeholder="المدينة" required>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3 py-3">
                        <input type="text" class="form-control" name="area" id="area"
                            value="{{$school->address['area']}}" placeholder="المنطقة" required>
                    </div>
                    <div class="col-6 col-sm-4 col-md-3 py-3">
                        <input type="text" class="form-control" name="extra" id="extra"
                            value="{{$school->address['extra']}}" placeholder="اضافي" >
                    </div>

                    <div class="mb-3 my-3">
                        <input class="form-control1" name="logo" type="file" id="file">
                        <label class="lab" for="file">
                            <i class="fas fa-images"></i>
                            <span class="choose-photo">اختر صورة</span>
                        </label>
                    </div>
                    <div class="mb-3 my-3 py-3">
                        <input type="file" name="logoo" id="real-file" hidden="hidden" />
                        <a type="button" id="custom-button"><i class="fas fa-images"></i><span class="choose-photo">اختر غلاف</span> </a>
                    </div>
                    <div class="d-grid gap-2">
                        <input type="submit" class="btn btn-outline-dark my-2 " value="تعديل" />
                    </div>
                </form>
            </div>
        </div>
    </div>
</main>
@endsection
@section('scrip')
<script>
    const realFileBtn = document.getElementById("real-file");
        const customBtn = document.getElementById("custom-button");
        const customTxt = document.getElementById("custom-text");
        
        customBtn.addEventListener("click", function() {
        realFileBtn.click();
        });
        
        realFileBtn.addEventListener("change", function() {
        if (realFileBtn.value) {
        customTxt.innerHTML = realFileBtn.value.match(
        /[\/\\]([\w\d\s\.\-\(\)]+)$/
        )[1];
        } else {
        customTxt.innerHTML = "No file chosen, yet.";
        }
        });
</script>
@endsection