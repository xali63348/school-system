@extends('layouts.app')
@section('contant')
<main class="mt-5 pt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12">
                @include('shared.profile')
                @component('shared.nav&tabs',['date'=>'active'])
                @endcomponent
                <div class="col-12 col-sm-12 col-md-6 offset-md-3 py-5 text-center">
                    <div class="table-responsive">
                        <table class="table">
                            <thead>
                                <tr>
                                    <th scope="col">فئه</th>
                                    <th scope="col">عدد البيانات</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    <th scope="row">الصفوف</th>
                                    <td>{{$count_classes}}</td>
                                </tr>  
                                <tr>
                                    <th scope="row">الشعب</th>
                                    <td>{{$count_category}}</td>
                                </tr>  
                                <tr>
                                    <th scope="row">المدرسين</th>
                                    <td>{{$count_teachers}}</td>
                                </tr>  
                                <tr>
                                    <th scope="row">الطلاب</th>
                                    <td>{{$count_students}}</td>
                                </tr>  
                                <tr>
                                    <th scope="row">الغيابات</th>
                                    <td>{{$count_absences}}</td>
                                </tr>  
                                <tr>
                                    <th scope="row">الجدول</th>
                                    <td>{{$count_tables}}</td>
                                </tr>  
                                <tr>
                                    <th scope="row">التبليغات</th>
                                    <td>{{$count_notifications}}</td>
                                </tr>  
                                <tr>
                                    <th scope="row">جداول الامتحانات</th>
                                    <td>{{$count_exam_schedule}}</td>
                                </tr>  
                                <tr>
                                    <th scope="row">نتائج الامتحانات</th>
                                    <td>{{$count_exam_results}}</td>
                                </tr>  
                                <tr>
                                    <th scope="row">{{auth()->user()->school->name}}</th>
                                    <td><a href="" class="btn btn-outline-danger" data-bs-toggle="modal" data-bs-target="#delete">حذف</a></td>
                                
                                </tr>  
                                
                                <!-- delete school -->
                                <div class="modal fade" id="delete" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                                    aria-labelledby="staticBackdropLabel" aria-hidden="true">
                                    <div class="modal-dialog">
                                        <div class="modal-content">
                                            <div class="modal-header">
                                                <h5 class="modal-title" id="staticBackdropLabel">حذف المدرسة</h5>
                                                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                                            </div>
                                            <div class="modal-body">
                                                هل متاكد من انك تريد حذف المدرسة 
                                                <br>
                                                ملاحظه: بعد حذفك لمدرسة لايمكنك ارجاع بيانات المدرسة
                                            </div>
                                            <div class="modal-footer">
                                                <button type="button" class="btn btn-secondary" data-bs-dismiss="modal">رجوع</button>
                                                <a href="/schools/delete" class="btn btn-primary">حذف</a>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>

@endsection