<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>نظام المدرسة</title>
    <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.5.0/font/bootstrap-icons.css">
    <link rel="stylesheet" href="/css/bootstrap.min.css">
    <link rel="stylesheet" href=" {{ URL::asset('home.css') }}">
    <link rel="stylesheet" href="/css/toastr.min.css">
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/CDNSFree2/Plyr/plyr.css" />
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/gh/CDNSFree2/Plyr/plyr.css" />
    <style>
        @import url('https://fonts.googleapis.com/css2?family=Almarai&family=Cairo:wght@200;400&family=Noto+Naskh+Arabic:wght@700&family=Readex+Pro&display=swap');
    </style>


</head>

<body>

    <div class="navbar navbar-expand-md bg-dark navbar-dark text-white fixed-top">
        <div class="container">
            <a href="/" class="navbar-brand">نظام المدرسة</a>

            <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#mainmenu">
                <span class="navbar-toggler-icon"></span>
            </button>
            <div class="collapse navbar-collapse" id="mainmenu">
                <ul class="navbar-nav ms-auto">
                    <li class="nav-item"><a href="" data-bs-toggle="modal" data-bs-target="#searchTeacher"
                            class="nav-link">الاستاذه</a></li>
                    <li class="nav-item"><a href="" data-bs-toggle="modal" data-bs-target="#search"
                            class="nav-link">الطلاب</a></li>
                    <li class="nav-item"><a href="#explore" class="nav-link">استكشاف النظام</a></li>
                    <li class="nav-item"><a href="/users/login" class="nav-link">تسجيل الدخول</a></li>
                </ul>
            </div>
        </div>
    </div>


    <section id="hero" class="bg-dark text-light text-center text-sm-start py-5">
        <div class="container">
            <div class="d-md-flex align-items-center justify-content-center text-center">
                <div>
                    <h1>نظام <span class="text-school">المدرسة</span></h1>
                    <p>نظام المدرسة هو نظام يمكن للطالب وولي الامر من الوصل الى جميع انشطة الطالب في المدرسه من غياب و
                        تبليغات و جداول
                        الاسبوعيه و جداول الشهريه و نتائج الامتحانات
                    </p>
                    <a href="" class="btn btn-primary" data-bs-toggle="modal" data-bs-target="#search">البحث</a>
                </div>
                <div id="animContainer" class="w-100 d-none d-md-block"></div>
            </div>
        </div>
    </section>

    {{-- serach Student --}}
    <div class="modal fade" id="search" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">البحث</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="d-flex ms-auto my-3 my-lg-0" action="/student/search" method="get">
                        @csrf
                        <div class="input-group">
                            <input class="form-control" type="search" name="search" placeholder="ادخل كود الطالب .."
                                aria-label="Search" name="search" />
                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end --}}

    {{-- serach Teachers --}}
    <div class="modal fade" id="searchTeacher" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
        <div class="modal-dialog modal-dialog-centered">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="exampleModalLabel">البحث</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form class="d-flex ms-auto my-3 my-lg-0" action="/teacher/search" method="get">
                        @csrf
                        <div class="input-group">
                            <input class="form-control" type="search" name="search" placeholder="ادخل كود الاستاذ .."
                                aria-label="Search" name="search" />
                            <button class="btn btn-primary" type="submit"><i class="fa fa-search"></i></button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end --}}

    <section id="student" class="py-5 mt-5 learn">
        <div class="container">
            <div class="row align-items-center justify-content-center">
                <div class="col-md">
                    <video width="300" height="300" control1 data-poster="/images/poster1.jpg" class="vid1">
                        <!-- Video files -->
                        <source src="/video/video_1.mp4" type="video/mp4" size="720">
                    </video>

                </div>
                <div class="col-md text-center py-3">
                    <h2>
                        الطلاب
                    </h2>
                    <p>يمكن لطالب الوصول الى جميع انشطته عن طريق البحث و استكشاف واجهة المستخدم الخاصه بطالب كما في
                        الفيديو</p>
                </div>

            </div>
        </div>
    </section>

    <section id="explore" class="py-5 mt-5 learn">
        <div class="container">
            <div class="row align-items-center justify-content-center">

                <div class="col-md text-center">
                    <h2>
                        استكشاف النظام
                    </h2>
                    <p>لكي تتمكن من الوصل الى جميع احصائيات المدرسه تحتاج الى لوحة قياده لترى جميع النشاطات الخاصه
                        بمدرستك</p>
                </div>
                <div class="col-md">
                    <video width="300" height="300" control1 data-poster="/images/poster_2.PNG" class="vid2">
                        <!-- Video files -->
                        <source src="/video/video_2.mp4" type="video/mp4" size="720">
                    </video>

                </div>
            </div>
        </div>
    </section>


    <section id="faq" class="py-5">
        <div class="container">
            <div class="row">
                <div class="col-md-10 offset-md-1">
                    <h2 class="text-center mb-3">
                        أسئلة مكررة
                    </h2>
                    <div class="accordion accordion-flush" id="accordionFlushExample">

                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingTwo">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#flush-collapseTwo" aria-expanded="false"
                                    aria-controls="flush-collapseTwo">
                                    ماهوه نظام المدرسة
                                </button>
                            </h2>
                            <div id="flush-collapseTwo" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingTwo" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">
                                    هو نظام يساعدك على الوصول الى جميع نشاطات الخاصه بالمدرسه <span
                                        class="text-school">بدينامكيه</span> من غيابات و تبليغات و جداول اسبوعيه و جداول
                                    امتحانات
                                    نتائج امتحانات
                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingOne">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#flush-collapseOne" aria-expanded="false"
                                    aria-controls="flush-collapseOne">
                                    كيف يمكن لي ان انشاء حساب في نظام المدرسة
                                </button>
                            </h2>
                            <div id="flush-collapseOne" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingOne" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">يمكنك مراسلة مالك النظام لكي يعطيك كود الدخول عند انشاء حساب

                                </div>
                            </div>
                        </div>
                        <div class="accordion-item">
                            <h2 class="accordion-header" id="flush-headingThree">
                                <button class="accordion-button collapsed" type="button" data-bs-toggle="collapse"
                                    data-bs-target="#flush-collapseThree" aria-expanded="true"
                                    aria-controls="flush-collapseThree">
                                    هل الموقع مجاني او مدفوع الثمن
                                </button>
                            </h2>
                            <div id="flush-collapseThree" class="accordion-collapse collapse"
                                aria-labelledby="flush-headingThree" data-bs-parent="#accordionFlushExample">
                                <div class="accordion-body">الموقع مدفوع الثمن وليس مكلف يمكنك تفعيل اشتراك سنوي
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    @include('shared.footer')


    <script src="/js/jquery-3.6.0.min.js"></script>
    {{-- <script src="/js/bootstrap.min.js"></script> --}}
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script src='https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.7.5/lottie.min.js'></script>
    <script src="https://cdnjs.cloudflare.com/ajax/libs/plyr/3.6.7/plyr.min.js"></script>

    <script src="/school.js"></script>
    <script src="/js/toastr.min.js"></script>
    {!! Toastr::message() !!}
    <script>
        toastr.options = {
            "closeButton": true,
            "debug": true,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          };
          @foreach($errors->all() as $error)
              toastr.error("{{$error}}")
          @endforeach
           
    </script>

    <script>
        var control1 =
          [
              'play-large', // The large play button in the center
             // 'restart', // Restart playback
             // 'rewind', // Rewind by the seek time (default 10 seconds)
              'play', // Play/pause playback
              'fast-forward', // Fast forward by the seek time (default 10 seconds)
              'progress', // The progress bar and scrubber for playback and buffering
              'current-time', // The current time of playback
              'duration', // The full duration of the media
              'mute', // Toggle mute
              'volume', // Volume control
              'captions', // Toggle captions
              'settings', // Settings menu
              'pip', // Picture-in-picture (currently Safari only)
              'airplay', // Airplay (currently Safari only)
              'download', // Show a download button with a link to either the current source or a custom URL you specify in your options
              'fullscreen' // Toggle fullscreen
          ];
            const player1 = new Plyr('.vid1',{control1});
            const player2 = new Plyr('.vid2',{control1});
    </script>

</body>

</html>