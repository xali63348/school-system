<!DOCTYPE html>
<html lang="en">

<head>
  <meta charset="UTF-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>نظام المدرسة</title>
  <link rel="stylesheet" href="/css/bootstrap.min.css">
  <link rel="stylesheet" href=" {{ URL::asset('create-category.css') }}">
  <link rel="stylesheet" href="/css/toastr.min.css">
  <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap-icons@1.7.1/font/bootstrap-icons.css">
  <script src="https://kit.fontawesome.com/64d58efce2.js" crossorigin="anonymous"></script>
  <style>
    @import url('https://fonts.googleapis.com/css2?family=Almarai&family=Cairo:wght@200;400&family=Noto+Naskh+Arabic:wght@700&family=Readex+Pro&display=swap');
  </style>

  <style>
    body {
      background: #f5f5f5;
      font-family: 'Almarai', sans-serif;
      font-family: 'Cairo', sans-serif;
      font-family: 'Noto Naskh Arabic', serif;
      font-family: 'Readex Pro', sans-serif;
    }
  </style>
  @section('styles')
  @show
</head>

<body>
  @yield('contant')




  <script src="/js/jquery-3.6.0.min.js"></script>
  <script src="/js/bootstrap.min.js"></script>
  <script src="/js/bootstrap.bundle.min.js"></script>
  <script src="/js/toastr.min.js"></script>
  <script src="/card.js"></script>
  {!! Toastr::message() !!}
  <script>
    toastr.options = {
      "closeButton": true,
      "debug": true,
      "newestOnTop": true,
      "progressBar": false,
      "positionClass": "toast-bottom-center",
      "preventDuplicates": false,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "7000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    };
    @foreach($errors->all() as $error)
        toastr.error("{{$error}}")
    @endforeach
     
  </script>
  <script src='https://cdnjs.cloudflare.com/ajax/libs/bodymovin/5.7.5/lottie.min.js'></script>
  <!-- path of bodymovin library-->
  <script src="/school.js"></script>
  <script src="/error.js"></script>
  @section('script')
  @show


</body>
</html>