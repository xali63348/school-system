@extends('layouts.app1')
@section('styles')
<style>
        table {
                background-color: #ced4da;
        }
</style>

@endsection
@section('contant')
@include('shared.navbar15')
<div class="container-fluid">
        <div class="row">
                @isset($category->students['0'])
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 py-3">
                        <div class="table-responsive">
                                <table class="table table-hover text-center">
                                        <thead>
                                                <tr>
                                                        <th scope="col">الاسم</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الشهر</th>
                                                        <th scope="col">اجرائات</th>
                                                </tr>
                                        </thead>
                                        @foreach ($students as $student)
                                        <form action="/examresults/create/{{$student->id}}" method="post">
                                                @csrf
                                                <tbody>
                                                        <tr>
                                                                <th scope="row">{{$student->name}}
                                                                </th>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson1" id="lesson1"
                                                                                placeholder="درس 1"
                                                                                value="{{old('lesson1')}}" required />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result1" id="result1"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result1')}}" required />
                                                                </td>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson2" id="lesson2"
                                                                                placeholder="درس 2"
                                                                                value="{{old('lesson2')}}" required />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result2" id="result2"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result2')}}" required />
                                                                </td>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson3" id="lesson3"
                                                                                placeholder="درس 3"
                                                                                value="{{old('lesson3')}}" required />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result3" id="result3"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result3')}}" required />
                                                                </td>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson4" id="lesson4"
                                                                                placeholder="درس 4"
                                                                                value="{{old('lesson4')}}" required />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result4" id="result4"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result4')}}" required />
                                                                </td>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson5" id="lesson5"
                                                                                placeholder="درس 5"
                                                                                value="{{old('lesson5')}}" required />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result5" id="result5"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result5')}}" required />
                                                                </td>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson6" id="lesson6"
                                                                                placeholder="درس 6"
                                                                                value="{{old('lesson6')}}" required />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result6" id="result6"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result6')}}" required />
                                                                </td>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson7" id="lesson7"
                                                                                placeholder="درس 7"
                                                                                value="{{old('lesson7')}}" />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result7" id="result7"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result7')}}" />
                                                                </td>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson8" id="lesson8"
                                                                                placeholder="درس 8"
                                                                                value="{{old('lesson8')}}" />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result8" id="result8"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result8')}}" />
                                                                </td>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson9" id="lesson9"
                                                                                placeholder="درس 9"
                                                                                value="{{old('lesson9')}}" />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result9" id="result9"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result9')}}" />
                                                                </td>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson10" id="lesson10"
                                                                                placeholder="درس 10"
                                                                                value="{{old('lesson10')}}" />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result10" id="result10"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result10')}}" />
                                                                </td>
                                                                <td>
                                                                        <input type="text" class="form-control"
                                                                                name="lesson11" id="lesson11"
                                                                                placeholder="درس 11"
                                                                                value="{{old('lesson11')}}" />
                                                                        <input type="number" class="form-control mt-2"
                                                                                name="result11" id="result11"
                                                                                placeholder="نتيجة"
                                                                                value="{{old('result11')}}" />
                                                                </td>
                                                                <td><input type="text" class="form-control" name="month"
                                                                                id="month" value="{{old('month')}}"
                                                                                placeholder="الشهر" required />
                                                                </td>
                                                                <td><input type="submit" class="btn btn-outline-dark"
                                                                                value="انشاء"></td>
                                                        </tr>
                                                </tbody>
                                        </form>
                                        @endforeach
                                </table>
                                @isset($search)
                                {{$students->appends(['search' => $search])->links()}}
                                @endisset
                                @empty($search)
                                {{$students->links()}}
                                @endempty
                        </div>
                </div>
                @endisset
                @empty($category->students['0'])
                <div class="py-4">
                        @include('shared.card-warining-exam-resulte')
                </div>

                @endempty
        </div>

</div>
@endsection