@extends('layouts.app1')
@section('styles')
<style>
        .categories {
                color: black;
                font-size: 19px;
                text-decoration: none;
        }

        .categories:hover {
                color: #4895ef;
        }

        .graduate {
                float: right;
        }

        .empty {
                text-align: center;
                margin-top: 15%;
        }

        .empty2 {
                color: #0077b6;
                text-decoration: none;
        }

        .empty2:hover {
                color: #023e8a;
                text-decoration: none;
        }

        
</style>

@endsection
@section('contant')
@include('shared.navbar17')
<div class="container-fluid">
        <div class="row">
                <span>
                        <a href="/examresults/create/{{$category->id}}" class="categories">
                                <span class="graduate">انشاء نتائج امتحانات <i class="fas fa-poll"></i></span>
                        </a>
                </span>
                @isset($category->ExamResults['0'])
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 py-3">
                        <div class="table-responsive">
                                <table class="table text-center table-primary table-hover">
                                        <thead>
                                                <tr>
                                                        <th scope="col">الاسم</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الدرس</th>
                                                        <th scope="col">الشهر</th>
                                                        <th scope="col">اجرائات</th>
                                                </tr>
                                        </thead>
                                
                                        @foreach ($ExamResults as $ExamResult)
                                        <tbody>
                                                <th scope="row">{{$ExamResult->students->name}}</th>
                                                <td>

                                                        {{$ExamResult->materials['lesson1']}}<br>
                                                        {{$ExamResult->grades['result1']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->materials['lesson2']}}<br>
                                                        {{$ExamResult->grades['result2']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->materials['lesson3']}}<br>
                                                        {{$ExamResult->grades['result3']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->materials['lesson4']}}<br>
                                                        {{$ExamResult->grades['result4']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->materials['lesson5']}}<br>
                                                        {{$ExamResult->grades['result5']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->materials['lesson6']}}<br>
                                                        {{$ExamResult->grades['result6']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->materials['lesson7']}}<br>
                                                        {{$ExamResult->grades['result7']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->materials['lesson8']}}<br>
                                                        {{$ExamResult->grades['result8']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->materials['lesson9']}}<br>
                                                        {{$ExamResult->grades['result9']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->materials['lesson10']}}<br>
                                                        {{$ExamResult->grades['result10']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->materials['lesson11']}}<br>
                                                        {{$ExamResult->grades['result11']}}
                                                </td>
                                                <td>
                                                        {{$ExamResult->month}}</td>

                                                <td>
                                                        <a href="/examresults/edit/{{$ExamResult->id}}"
                                                                class="btn btn-outline-dark">تعديل</a>
                                                        <a href="/examresults/delete/{{$ExamResult->id}}"
                                                                class="btn btn-danger">حذف</a>
                                                </td>
                                        </tbody>
                                        @endforeach
                                    

                                </table>
                                @isset($search)
                                        {{$ExamResults->appends(['search' => $search])->links()}}
                                @endisset
                                @empty($search)
                                        {{$ExamResults->links()}}
                                @endempty
                                
                        </div>
                </div>
                @endisset
                @empty($category->students['0'])
                        @include('shared.card-warining-exam-resulte');
                @endempty
                @isset($category->students['0'])
                @empty($category->ExamResults['0'])
                <h2 class="empty">أضف <a class="empty2"
                                href="/examresults/create/{{$category->id}}">نتائج امتحانات</a></h2>
                @endempty
                @endisset


        </div>
</div>
@endsection