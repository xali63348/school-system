@extends('layouts.app1')
@section('styles')
<style>
    .tables0 {
        color: black;
        font-size: 22px;
        text-decoration: none;
    }

    .tables0:hover {
        color: #28a745;
    }

    .tables1 {
        float: right;
    }

    table {
        background-color: #ced4da;
    }
</style>
@endsection
@section('contant')
@include('shared.navbar7')
<div class="container-fluid">
    <div class="row">
        <div class="table-responsive">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 py-3">
                <table class="table text-center table-hover">
                    <thead>
                        <tr>
                            <th scope="col">الايام</th>
                            <th scope="col">درس 1</th>
                            <th scope="col">درس 2</th>
                            <th scope="col">درس 3</th>
                            <th scope="col">درس 4</th>
                            <th scope="col">درس 5</th>
                            <th scope="col">درس 6</th>
                        </tr>
                    </thead>

                    <form action="/tables/edit/{{$table->id}}" method="post">
                        @csrf
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="day1" id="day1"
                                        value="{{$table->days['day1']}}" placeholder="اليوم" required>
                                </th>
                                <td>
                                    <input type="text" class="form-control" name="Lesson1" id="Lesson1"
                                        value="{{$table->study_materials['Lesson1']}}" value="{{old('Lesson1')}}"
                                        placeholder="درس 1" required>
                                    <select name="teacher1" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher1']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson2" id="Lesson2"
                                        value="{{$table->study_materials['Lesson2']}}" value="{{old('Lesson2')}}"
                                        placeholder="درس 2" required>
                                    <select name="teacher2" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher2']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson3" id="Lesson3"
                                        value="{{$table->study_materials['Lesson3']}}" value="{{old('Lesson3')}}"
                                        placeholder="درس 3" required>
                                    <select name="teacher3" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher3']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson4" id="Lesson4"
                                        value="{{$table->study_materials['Lesson4']}}" value="{{old('Lesson4')}}"
                                        placeholder="درس 4" required>
                                    <select name="teacher4" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher4']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson5" id="Lesson5"
                                        value="{{$table->study_materials['Lesson5']}}" value="{{old('Lesson5')}}"
                                        placeholder="درس 5" required>
                                    <select name="teacher5" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher5']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson6" id="Lesson6"
                                        value="{{$table->study_materials['Lesson6']}}" value="{{old('Lesson6')}}"
                                        placeholder="درس 6">
                                    <select name="teacher6" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher6']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>


                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="day2" id="day2"
                                        value="{{$table->days['day2']}}" value="{{old('day2')}}" placeholder="اليوم"
                                        required>
                                </th>
                                <td>
                                    <input type="text" class="form-control" name="Lesson7" id="Lesson7"
                                        value="{{$table->study_materials['Lesson7']}}" value="{{old('Lesson7')}}"
                                        placeholder="درس 1" required>
                                    <select name="teacher7" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher7']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson8" id="Lesson8"
                                        value="{{$table->study_materials['Lesson8']}}" value="{{old('Lesson8')}}"
                                        placeholder="درس 2" required>
                                    <select name="teacher8" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher8']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson9" id="Lesson9"
                                        value="{{$table->study_materials['Lesson9']}}" value="{{old('Lesson9')}}"
                                        placeholder="درس 3" required>
                                    <select name="teacher9" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher9']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson10" id="Lesson10"
                                        value="{{$table->study_materials['Lesson10']}}" value="{{old('Lesson10')}}"
                                        placeholder="درس 4" required>
                                    <select name="teacher10" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher10']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson11" id="Lesson11"
                                        value="{{$table->study_materials['Lesson11']}}" value="{{old('Lesson11')}}"
                                        placeholder="درس 5" required>
                                    <select name="teacher11" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher11']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson12" id="Lesson12"
                                        value="{{$table->study_materials['Lesson12']}}" value="{{old('Lesson12')}}"
                                        placeholder="درس 6">
                                    <select name="teacher12" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher12']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>


                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="day3" id="day3"
                                        value="{{$table->days['day3']}}" value="{{old('day3')}}" placeholder="اليوم"
                                        required>
                                </th>
                                <td>
                                    <input type="text" class="form-control" name="Lesson13" id="Lesson13"
                                        value="{{$table->study_materials['Lesson13']}}" value="{{old('Lesson13')}}"
                                        placeholder="درس 1" required>
                                    <select name="teacher13" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher13']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson14" id="Lesson14"
                                        value="{{$table->study_materials['Lesson14']}}" value="{{old('Lesson14')}}"
                                        placeholder="درس 2" required>
                                    <select name="teacher14" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher14']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson15" id="Lesson15"
                                        value="{{$table->study_materials['Lesson15']}}" value="{{old('Lesson15')}}"
                                        placeholder="درس 3" required>
                                    <select name="teacher15" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher15']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson16" id="Lesson16"
                                        value="{{$table->study_materials['Lesson16']}}" value="{{old('Lesson16')}}"
                                        placeholder="درس 4" required>
                                    <select name="teacher16" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher16']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson17" id="Lesson17"
                                        value="{{$table->study_materials['Lesson17']}}" value="{{old('Lesson17')}}"
                                        placeholder="درس 5" required>
                                    <select name="teacher17" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher17']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson18" id="Lesson18"
                                        value="{{$table->study_materials['Lesson18']}}" value="{{old('Lesson18')}}"
                                        placeholder="درس 6">
                                    <select name="teacher18" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher18']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>


                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="day4" id="day4"
                                        value="{{$table->days['day4']}}" value="{{old('day4')}}" placeholder="اليوم"
                                        required>
                                </th>
                                <td>
                                    <input type="text" class="form-control" name="Lesson19" id="Lesson19"
                                        value="{{$table->study_materials['Lesson19']}}" value="{{old('Lesson19')}}"
                                        placeholder="درس 1" required>
                                    <select name="teacher19" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher19']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson20" id="Lesson20"
                                        value="{{$table->study_materials['Lesson20']}}" value="{{old('Lesson20')}}"
                                        placeholder="درس 2" required>
                                    <select name="teacher20" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher20']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson21" id="Lesson21"
                                        value="{{$table->study_materials['Lesson21']}}" value="{{old('Lesson21')}}"
                                        placeholder="درس 3" required>
                                    <select name="teacher21" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher21']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson22" id="Lesson22"
                                        value="{{$table->study_materials['Lesson22']}}" value="{{old('Lesson22')}}"
                                        placeholder="درس 4" required>
                                    <select name="teacher22" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher22']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson23" id="Lesson23"
                                        value="{{$table->study_materials['Lesson23']}}" value="{{old('Lesson23')}}"
                                        placeholder="درس 5" required>
                                    <select name="teacher23" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher23']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson24" id="Lesson24"
                                        value="{{$table->study_materials['Lesson24']}}" value="{{old('Lesson24')}}"
                                        placeholder="درس 6">
                                    <select name="teacher24" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher24']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>


                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="day5" id="day5"
                                        value="{{$table->days['day5']}}" value="{{old('day5')}}" placeholder="اليوم"
                                        required>
                                </th>
                                <td>
                                    <input type="text" class="form-control" name="Lesson25" id="Lesson25"
                                        value="{{$table->study_materials['Lesson25']}}" value="{{old('Lesson25')}}"
                                        placeholder="درس 1" required>
                                    <select name="teacher25" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher25']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson26" id="Lesson26"
                                        value="{{$table->study_materials['Lesson26']}}" value="{{old('Lesson26')}}"
                                        placeholder="درس 2" required>
                                    <select name="teacher26" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher26']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson27" id="Lesson27"
                                        value="{{$table->study_materials['Lesson27']}}" value="{{old('Lesson27')}}"
                                        placeholder="درس 3" required>
                                    <select name="teacher27" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher27']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson28" id="Lesson28"
                                        value="{{$table->study_materials['Lesson28']}}" value="{{old('Lesson28')}}"
                                        placeholder="درس 4" required>
                                    <select name="teacher28" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher28']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson29" id="Lesson29"
                                        value="{{$table->study_materials['Lesson29']}}" value="{{old('Lesson29')}}"
                                        placeholder="درس 5" required>
                                    <select name="teacher29" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher29']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson30" id="Lesson30"
                                        value="{{$table->study_materials['Lesson30']}}" value="{{old('Lesson30')}}"
                                        placeholder="درس 6">
                                    <select name="teacher30" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher30']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>


                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="day6" id="day6"
                                        value="{{$table->days['day6']}}" value="{{old('day6')}}" placeholder="اليوم">
                                </th>
                                <td>
                                    <input type="text" class="form-control" name="Lesson31" id="Lesson31"
                                        value="{{$table->study_materials['Lesson31']}}" value="{{old('Lesson31')}}"
                                        placeholder="درس 1">
                                    <select name="teacher31" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher31']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson32" id="Lesson32"
                                        value="{{$table->study_materials['Lesson32']}}" value="{{old('Lesson32')}}"
                                        placeholder="درس 2">
                                    <select name="teacher32" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher32']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson33" id="Lesson33"
                                        value="{{$table->study_materials['Lesson33']}}" value="{{old('Lesson33')}}"
                                        placeholder="درس 3">
                                    <select name="teacher33" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher33']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson34" id="Lesson34"
                                        value="{{$table->study_materials['Lesson34']}}" value="{{old('Lesson34')}}"
                                        placeholder="درس 4">
                                    <select name="teacher34" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher34']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson35" id="Lesson35"
                                        value="{{$table->study_materials['Lesson35']}}" value="{{old('Lesson35')}}"
                                        placeholder="درس 5">
                                    <select name="teacher35" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher35']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="Lesson36" id="Lesson36"
                                        value="{{$table->study_materials['Lesson36']}}" value="{{old('Lesson36')}}"
                                        placeholder="درس 6">
                                    <select name="teacher36" id="inputState" class="form-select mt-1">
                                        <option selected>{{$table->teachers['teacher36']}}</option>
                                        @foreach($category->teachers as $teacher)
                                        <option value="{{$teacher->name}}">{{$teacher->name}}</option>
                                        @endforeach
                                    </select>
                                </td>
                            </tr>
                        </tbody>

                        <tbody>
                            <tr>
                                <td></td>
                                <td></td>
                                <td></td>

                                <td>
                                    <div class="d-grid gap-2 col-12 mx-auto">
                                        <input type="submit" class="btn btn-outline-dark" value="تحديث" />
                                    </div>
                                </td>
                                <td></td>
                                <td></td>
                                <td></td>
                            </tr>
                        </tbody>

                    </form>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection