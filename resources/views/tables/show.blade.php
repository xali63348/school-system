@extends('layouts.app1')
@section('styles')
<style>
    .tables0 {
        color: black;
        font-size: 19px;
        text-decoration: none;
    }

    .tables0:hover {
        color: #7ae582;
    }

    .tables1 {
        float: right;
    }

    .bd-callout-warning {
        border-left-color: #f0ad4e !important;
    }

    .bd-callout {
        padding: 1.25rem;
        margin-top: 35%;
        margin-bottom: 1.25rem;
        border: 1px solid #e9ecef;
        border-left-width: 0.25rem;
        border-radius: 0.25rem;
    }

    .main-content {
        padding-top: 2px;
        padding-bottom: 2px;
    }

    .info-card {
        background: #fff;
        text-align: center;
        padding: 50px 30px;
        margin-bottom: 30px;
        border-radius: 3px;
        box-shadow: 0 10px 30px rgba(0, 0, 0, 0.1);
    }

    .info-card .info-card_icon {
        height: 125px;
        width: 125px;
        margin: 0 auto 50px auto;
        border: 5px solid #4caf50;
        border-radius: 125px;
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;
    }

    .info-card .info-card_icon i {
        font-size: 50px;
        color: #4caf50;
    }

    .info-card .info-card_icon .info-card_img-icon {
        height: 60px;
        width: 60px;
        object-fit: contain;
    }

    .info-card .info-card_label {
        margin-bottom: 15px;
    }

    .info-card .info-card_message {
        margin-bottom: 15px;
    }

    .info-card .btn {
        background: #03a9f4;
        border-color: #03a9f4;
        box-shadow: 0 3px 10px rgba(0, 0, 0, 0.1);
    }

    .info-card--success .info-card_icon {
        border-color: #4caf50;
    }

    .info-card--success .info-card_icon i {
        color: #4caf50;
    }

    .info-card--danger .info-card_icon {
        border-color: #f44336;
    }

    .info-card--danger .info-card_icon i {
        color: #f44336;
    }

    .info-card--warning .info-card_icon {
        border-color: #ff9800;
    }

    .info-card--warning .info-card_icon i {
        color: #ff9800;
    }

    .btn .btn-outline-success {
        font-size: 19px;
    }

    .empty {
        text-align: center;
        margin-top: 15%;
    }

    .empty2 {
        color: #70e000;
        text-decoration: none;
    }

    .empty2:hover {
        color: #38b000;
        text-decoration: none;
    }
</style>
@endsection
@section('contant')
@include('shared.navbar6')
<div class="container-fluid">
    <div class="row">
        <span>
            @isset($category->tables['0'])
            <style>
                .tables0 {
                    pointer-events: none;
                    cursor: default;
                }
            </style>
            @endisset

            <a href="/tables/create/{{$category->id}}" class="tables0">
                <span class="tables1 ">انشاء جدول <i class="fas fa-table"></i></span>
            </a>
        </span>

        @isset($category->tables['0'])
        <div class="table-responsive">
            <a href="/pdf/download/table/{{$tables['0']->id}}" class="btn btn-outline-success">تنزيل الملف</a>
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 py-3">
                <table class="table table-info text-center table-hover">
                    <thead>
                        <tr>
                            <th scope="col">الايام</th>
                            <th scope="col">درس 1</th>
                            <th scope="col">درس 2</th>
                            <th scope="col">درس 3</th>
                            <th scope="col">درس 4</th>
                            <th scope="col">درس 5</th>
                            <th scope="col">درس 6</th>
                            <th scope="col">التاريخ</th>
                        </tr>
                    </thead>
                    @foreach($tables as $table)
                    <tbody>

                        <tr>
                            <th scope="row">{{$table->days['day1']}}</th>
                            <td>
                                {{$table->study_materials['Lesson1']}} <br>
                                {{$table->teachers['teacher1']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson2']}} <br>
                                {{$table->teachers['teacher2']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson3']}} <br>
                                {{$table->teachers['teacher3']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson4']}} <br>
                                {{$table->teachers['teacher4']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson5']}} <br>
                                {{$table->teachers['teacher5']}}

                            </td>
                            <td>
                                {{$table->study_materials['Lesson6']}} <br>
                                {{$table->teachers['teacher6']}}

                            </td>
                            <td>{{$table->created_at->format('m/d/Y')}}</td>
                        </tr>

                        <tr>
                            <th scope="row">{{$table->days['day2']}}</th>
                            <td>
                                {{$table->study_materials['Lesson7']}} <br>
                                {{$table->teachers['teacher7']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson8']}} <br>
                                {{$table->teachers['teacher8']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson9']}} <br>
                                {{$table->teachers['teacher9']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson10']}} <br>
                                {{$table->teachers['teacher10']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson11']}} <br>
                                {{$table->teachers['teacher11']}}

                            </td>
                            <td>
                                {{$table->study_materials['Lesson12']}} <br>
                                {{$table->teachers['teacher12']}}

                            </td>
                            <td>{{$table->created_at->format('m/d/Y')}}</td>
                        </tr>

                        <tr>
                            <th scope="row">{{$table->days['day3']}}</th>
                            <td>
                                {{$table->study_materials['Lesson13']}} <br>
                                {{$table->teachers['teacher13']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson14']}} <br>
                                {{$table->teachers['teacher14']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson15']}} <br>
                                {{$table->teachers['teacher15']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson16']}} <br>
                                {{$table->teachers['teacher16']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson17']}} <br>
                                {{$table->teachers['teacher17']}}

                            </td>
                            <td>
                                {{$table->study_materials['Lesson18']}} <br>
                                {{$table->teachers['teacher18']}}

                            </td>
                            <td>{{$table->created_at->format('m/d/Y')}}</td>
                        </tr>

                        <tr>
                            <th scope="row">{{$table->days['day4']}}</th>
                            <td>
                                {{$table->study_materials['Lesson19']}} <br>
                                {{$table->teachers['teacher19']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson20']}} <br>
                                {{$table->teachers['teacher20']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson21']}} <br>
                                {{$table->teachers['teacher21']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson22']}} <br>
                                {{$table->teachers['teacher22']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson23']}} <br>
                                {{$table->teachers['teacher23']}}

                            </td>
                            <td>
                                {{$table->study_materials['Lesson24']}} <br>
                                {{$table->teachers['teacher24']}}

                            </td>
                            <td>{{$table->created_at->format('m/d/Y')}}</td>
                        </tr>

                        <tr>
                            <th scope="row">{{$table->days['day5']}}</th>
                            <td>
                                {{$table->study_materials['Lesson25']}} <br>
                                {{$table->teachers['teacher25']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson26']}} <br>
                                {{$table->teachers['teacher26']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson27']}} <br>
                                {{$table->teachers['teacher27']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson28']}} <br>
                                {{$table->teachers['teacher28']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson29']}} <br>
                                {{$table->teachers['teacher29']}}

                            </td>
                            <td>
                                {{$table->study_materials['Lesson30']}} <br>
                                {{$table->teachers['teacher30']}}

                            </td>
                            <td>{{$table->created_at->format('m/d/Y')}}</td>
                        </tr>
                    @isset($table->days['day6'])
                        <tr>
                            <th scope="row">{{$table->days['day6']}}</th>
                            <td>
                                {{$table->study_materials['Lesson31']}} <br>
                                {{$table->teachers['teacher31']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson32']}} <br>
                                {{$table->teachers['teacher32']}}

                            </td>
                            <td>

                                {{$table->study_materials['Lesson33']}} <br>
                                {{$table->teachers['teacher33']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson34']}} <br>
                                {{$table->teachers['teacher34']}}
                            </td>
                            <td>

                                {{$table->study_materials['Lesson35']}} <br>
                                {{$table->teachers['teacher35']}}

                            </td>
                            <td>
                                {{$table->study_materials['Lesson36']}} <br>
                                {{$table->teachers['teacher36']}}

                            </td>
                            <td>{{$table->created_at->format('m/d/Y')}}</td>
                        </tr>
                    @endisset

                    </tbody>

                    <tbody>
                        <tr>
                            <td></td>
                            <td></td>
                            <td></td>
                            <td>
                                <div class="d-grid gap-2 col-12 mx-auto">
                                    <a href="/tables/edit/{{$table->id}}" class="btn btn-outline-dark">تعديل</a>
                                </div>
                            </td>

                            <td>
                                <div class="d-grid gap-2 col-12 mx-auto">
                                    <a href="/tables/delete/{{$table->id}}" class="btn btn-danger">حذف</a>
                                </div>
                            </td>
                            <td></td>
                            <td></td>
                            <td></td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
            </div>
        </div>
        @endisset
        @if(empty($category->tables['0']))
        <h2 class="empty">أضف <a class="empty2" href="/tables/create/{{$category->id}}">جدول</a> </h2>
        @endif

    </div>
</div>
@endsection