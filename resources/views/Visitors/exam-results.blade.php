@extends('layouts.app4')
@section('styles')
<style>
    .empty {
        text-align: center;
        margin-top: 23%;
    }

    .empty2 {
        color: #0077b6;
        text-decoration: none;
    }
</style>
@endsection
@section('contant')
<main class="mt-5 pt-3">
    <div class="container-fluid">
        <div class="row">
            @isset($ExamResults['0'])
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 py-3">
                <div class="table-responsive">
                    <table class="table text-center table-primary table-hover">
                        <thead>
                            <tr>
                                <th scope="col">{{__('messages.Name')}}</th>
                                <th scope="col">{{__('messages.Month')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Date')}}</th>
                            </tr>
                        </thead>

                        @foreach ($ExamResults as $ExamResult)
                        <tbody>
                            <th scope="row">{{$ExamResult->students->name}}</th>
                            <td>
                                {{$ExamResult->month}}
                            </td>

                            <td>

                                {{$ExamResult->materials['lesson1']}}<br>
                                {{$ExamResult->grades['result1']}}
                            </td>
                            <td>
                                {{$ExamResult->materials['lesson2']}}<br>
                                {{$ExamResult->grades['result2']}}
                            </td>
                            <td>
                                {{$ExamResult->materials['lesson3']}}<br>
                                {{$ExamResult->grades['result3']}}
                            </td>
                            <td>
                                {{$ExamResult->materials['lesson4']}}<br>
                                {{$ExamResult->grades['result4']}}
                            </td>
                            <td>
                                {{$ExamResult->materials['lesson5']}}<br>
                                {{$ExamResult->grades['result5']}}
                            </td>
                            <td>
                                {{$ExamResult->materials['lesson6']}}<br>
                                {{$ExamResult->grades['result6']}}
                            </td>
                            <td>
                                {{$ExamResult->materials['lesson7']}}<br>
                                {{$ExamResult->grades['result7']}}
                            </td>
                            <td>
                                {{$ExamResult->materials['lesson8']}}<br>
                                {{$ExamResult->grades['result8']}}
                            </td>
                            <td>
                                {{$ExamResult->materials['lesson9']}}<br>
                                {{$ExamResult->grades['result9']}}
                            </td>
                            <td>
                                {{$ExamResult->materials['lesson10']}}<br>
                                {{$ExamResult->grades['result10']}}
                            </td>
                            <td>
                                {{$ExamResult->materials['lesson11']}}<br>
                                {{$ExamResult->grades['result11']}}
                            </td>
                            <td>
                                {{$ExamResult->created_at->format('m/d/Y')}}
                            </td>
                        </tbody>
                        @endforeach
                    </table>
                    {{$ExamResults->links()}}
                </div>
            </div>
            @endisset

            @empty($ExamResults['0'])
            <h2 class="empty">لايوجد <span class="empty2">نتائج امتحانات </span> الى الان  </h2>
            @endempty

        </div>
    </div>
</main>
@endsection