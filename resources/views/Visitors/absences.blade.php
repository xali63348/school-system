@extends('layouts.app4')
@section('styles')
<style>
    .empty {
        text-align: center;
        margin-top: 23%;
    }

    .empty2 {
        color: #dc3545;
    }
</style>
@endsection

@section('contant')
<main class="mt-5 pt-3">
    <div class="container-fluid">
        <div class="row">

            <div class="col-12 py-1">
                <div class="table-responsive">
                    @isset($absences['0'])
                    <table class="table  table-danger table-hover text-center" style="width:100%">
                        <thead>
                            <tr>
                                <th scope="col">{{__('messages.Name')}}</th>
                                <th scope="col">{{__('messages.Lesson 1')}}</th>
                                <th scope="col">{{__('messages.Lesson 2')}}</th>
                                <th scope="col">{{__('messages.Lesson 3')}}</th>
                                <th scope="col">{{__('messages.Lesson 4')}}</th>
                                <th scope="col">{{__('messages.Lesson 5')}}</th>
                                <th scope="col">{{__('messages.Lesson 6')}}</th>
                                <th scope="col">{{__('messages.All Day')}}</th>
                                <th scope="col">{{__('messages.Date')}}</th>
                            </tr>
                        </thead>

                        @foreach($absences as $absence)
                        <tbody>
                            <tr>
                                <td>{{$absence->students->name}}</td>
                                <td>{{$absence->days_of_absence['lesson1']}}</td>
                                <td>{{$absence->days_of_absence['lesson2']}}</td>
                                <td>{{$absence->days_of_absence['lesson3']}}</td>
                                <td>{{$absence->days_of_absence['lesson4']}}</td>
                                <td>{{$absence->days_of_absence['lesson5']}}</td>
                                <td>{{$absence->days_of_absence['lesson6']}}</td>
                                <td>{{$absence->days_of_absence['allLasson']}}</td>

                                <td>{{$absence->date}}</td>

                            </tr>
                        </tbody>
                        @endforeach
                    </table>
                    {{$absences->links()}}
                    @endisset

                    @empty($absences['0'])
                    <h2 class="empty">لايوجد <span class="empty2">غيابات</span>
                        @endempty
                </div>
            </div>

        </div>
    </div>


</main>
@endsection