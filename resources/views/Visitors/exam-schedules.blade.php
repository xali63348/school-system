@extends('layouts.app4')
@section('styles')
<style>
    .empty {
        text-align: center;
        margin-top: 23%;
    }

    .empty2 {
        color: #ffc300;
        text-decoration: none;
    }

    .btn-outline {
        border: #ffc300 1px solid;
        border-radius: 0.25rem;
    }

    .btn-outline:hover {
        background-color: #ffc300;
    }
</style>

@endsection
@section('contant')
<main class="mt-5 pt-3">
    <div class="container-fluid">
        <div class="row">
            @isset($ExamSchedules['0'])
            <div class="table-responsive">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    @foreach($ExamSchedules as $examschedule)
                    <span>
                        <h3 class="text-center mt-3">{{$examschedule->month}}</h3>
                        <a href="/pdf/download/exam_schedules/{{$examschedule->id}}" class="btn btn-outline">{{__('messages.Download Pdf')}}</a>
                    </span>

                    <table class="table table-warning table-hover text-center mt-4">
                        <thead>
                            <tr>
                               <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Date')}}</th>
                                <th scope="col">{{__('messages.Day')}}</th>
                                <th scope="col">{{__('messages.Lesson')}}</th>
                                <th scope="col">{{__('messages.Date')}}</th>
                                <th scope="col">{{__('messages.Day')}}</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <th scope="row">{{$examschedule->materials['lesson1']}}</th>
                                <td>{{$examschedule->date['date1']}}</td>
                                <td>{{$examschedule->days['day1']}}</td>
                                <td>{{$examschedule->materials['lesson7']}}</td>
                                <td>{{$examschedule->date['date7']}}</td>
                                <td>{{$examschedule->days['day7']}}</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <th scope="row">{{$examschedule->materials['lesson2']}}</th>
                                <td>{{$examschedule->date['date2']}}</td>
                                <td>{{$examschedule->days['day2']}}</td>
                                <td>{{$examschedule->materials['lesson8']}}</td>
                                <td>{{$examschedule->date['date8']}}</td>
                                <td>{{$examschedule->days['day8']}}</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <th scope="row">{{$examschedule->materials['lesson3']}}</th>
                                <td>{{$examschedule->date['date3']}}</td>
                                <td>{{$examschedule->days['day3']}}</td>
                                <td>{{$examschedule->materials['lesson9']}}</td>
                                <td>{{$examschedule->date['date9']}}</td>
                                <td>{{$examschedule->days['day9']}}</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <th scope="row">{{$examschedule->materials['lesson4']}}</th>
                                <td>{{$examschedule->date['date4']}}</td>
                                <td>{{$examschedule->days['day4']}}</td>
                                <td>{{$examschedule->materials['lesson10']}}</td>
                                <td>{{$examschedule->date['date10']}}</td>
                                <td>{{$examschedule->days['day10']}}</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <th scope="row">{{$examschedule->materials['lesson5']}}</th>
                                <td>{{$examschedule->date['date5']}}</td>
                                <td>{{$examschedule->days['day5']}}</td>
                                <td>{{$examschedule->materials['lesson11']}}</td>
                                <td>{{$examschedule->date['date11']}}</td>
                                <td>{{$examschedule->days['day11']}}</td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <th scope="row">{{$examschedule->materials['lesson6']}}</th>
                                <td>{{$examschedule->date['date6']}}</td>
                                <td>{{$examschedule->days['day6']}}</td>
                                <td>{{$examschedule->materials['lesson12']}}</td>
                                <td>{{$examschedule->date['date12']}}</td>
                                <td>{{$examschedule->days['day12']}}</td>
                            </tr>
                        </tbody>

                    </table>

                    @endforeach
                    <div>
                        {{$ExamSchedules->links()}}
                    </div>
                </div>
            </div>
            @endisset
            @empty($ExamSchedules['0'])
            <h2 class="empty">لايوجد<span class="empty2">جدول امتحانات </span> الى الان</h2>
            @endempty
        </div>
    </div>
</main>
@endsection