@extends('layouts.app4')
@section('styles')
<style>
    .banner {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 175px;
        background-position: center;
        background-size: cover !important;
    }

    .img-circle {
        height: 180px;
        width: 180px;
        border-radius: 150px;
        border: 3px solid #fff;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        z-index: 1;
        margin-top: 40px;
    }

    .shadow {
        box-shadow: 0 5px 20px rgba(0, 0, 0, 0.06) !important;
    }
</style>
@endsection


@section('contant')
<main class="mt-5 pt-3">


    <div class="profile-card card rounded-lg shadow   mb-4 text-center position-relative overflow-hidden">
        <div class="banner">
            <img src="/images/banner1.jpg" alt="" class="banner"> 
        </div>
        @isset($student->images)
            <img src="{{asset($student->images)}}" alt="" class="img-circle mx-auto mb-3">
        @endisset
        @empty($student->images)
            <img src="/images/user.png" alt="" class="img-circle mx-auto mb-3">
        @endempty

        <h3 class="mb-1">{{$student->name}}</h3>
        <div class="text-left mb-4">
        <p class="mb-2"><i class="fas fa-user-graduate"></i>{{$student->age}}</p>
        <p class="mb-2"><i class="fas fa-chalkboard"></i> {{$student->category->classes->name}}-{{$student->category->name}}</p>
        <p class="mb-2"><i class="fa fa-phone mr-2"></i> {{$student->number_phone}}</p>
        <p class="mb-2"><i class="fas fa-code"></i> {{$student->number_code}}</p>
        <p class="mb-2"><i class="fa fa-map-marker-alt mr-2"></i> {{$student->address['country']}},
            {{$student->address['city']}},{{$student->address['area']}},{{$student->address['extra']}}</p>
        </div>
    </div>
</main>
@endsection