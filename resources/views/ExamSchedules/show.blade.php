@extends('layouts.app1')
@section('styles')
    <style>
        .ExamSchedules{
            color:black;
            font-size:19px;
            text-decoration: none;
        }
        .ExamSchedules:hover{
            color:#ffc300;
        }
        .ExamSchedules1{
            float: right;
        }
        .empty{
            text-align:center;
            margin-top:15%;
        }
        .empty2{
            color: #ffc300;
	        text-decoration: none;
        }
        .empty2:hover{
            color: #f8961e;
	        text-decoration: none;
        }
        .btn-outline{
            border: #ffc300 1px solid;
            border-radius: 0.25rem;
        }
        .btn-outline:hover{
            background-color:#ffc300;
        }
    </style>
@endsection
@section('contant')
    @include('shared.navbar12')
    <div class="container-fluid">
        <div class="row">
            <span>
                <a href="/examschedules/create/{{$category->id}}" class="ExamSchedules">
                    <span class="ExamSchedules1">انشاء جدول امتحانات<i class="far fa-calendar-alt"></i></span>
                </a>
            </span>
            @isset($ExamSchedules['0'])
            <div class="table-responsive">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                @foreach($ExamSchedules as $examschedule)
                <span>
                    <h3 class="text-center mt-3">{{$examschedule->month}}</h3>
                    <a href="/pdf/download/exam_schedules/{{$examschedule->id}}" class="btn btn-outline">تنزيل الملف</a>
                </span>
                   
                    <table class="table table-warning table-hover mt-4 text-center">
                        <thead>
                                <tr>
                                    <th scope="col">الدرس</th>
                                    <th scope="col">التاريح</th>
                                    <th scope="col">اليوم</th>
                                    <th scope="col">الدرس</th>
                                    <th scope="col">التاريح</th>
                                    <th scope="col">اليوم</th>
                                </tr>
                            </thead>
                        
                            <tbody>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson1']}}</th>
                                    <td>{{$examschedule->date['date1']}}</td>
                                    <td>{{$examschedule->days['day1']}}</td>
                                    <td >{{$examschedule->materials['lesson7']}}</td>
                                    <td>{{$examschedule->date['date7']}}</td>
                                    <td>{{$examschedule->days['day7']}}</td>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson2']}}</th>
                                    <td>{{$examschedule->date['date2']}}</td>
                                    <td>{{$examschedule->days['day2']}}</td>
                                    <td >{{$examschedule->materials['lesson8']}}</td>
                                    <td>{{$examschedule->date['date8']}}</td>
                                    <td>{{$examschedule->days['day8']}}</td>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson3']}}</th>
                                    <td>{{$examschedule->date['date3']}}</td>
                                    <td>{{$examschedule->days['day3']}}</td>
                                    <td >{{$examschedule->materials['lesson9']}}</td>
                                    <td>{{$examschedule->date['date9']}}</td>
                                    <td>{{$examschedule->days['day9']}}</td>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson4']}}</th>
                                    <td>{{$examschedule->date['date4']}}</td>
                                    <td>{{$examschedule->days['day4']}}</td>
                                    <td >{{$examschedule->materials['lesson10']}}</td>
                                    <td>{{$examschedule->date['date10']}}</td>
                                    <td>{{$examschedule->days['day10']}}</td>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson5']}}</th>
                                    <td>{{$examschedule->date['date5']}}</td>
                                    <td>{{$examschedule->days['day5']}}</td>
                                    <td >{{$examschedule->materials['lesson11']}}</td>
                                    <td>{{$examschedule->date['date11']}}</td>
                                    <td>{{$examschedule->days['day11']}}</td>
                                </tr>
                            </tbody>
                            <tbody>
                                <tr>
                                    <th scope="row">{{$examschedule->materials['lesson6']}}</th>
                                    <td>{{$examschedule->date['date6']}}</td>
                                    <td>{{$examschedule->days['day6']}}</td>
                                    <td >{{$examschedule->materials['lesson12']}}</td>
                                    <td>{{$examschedule->date['date12']}}</td>
                                    <td>{{$examschedule->days['day12']}}</td>
                                </tr>
                            </tbody>
                        
                    </table>
                    <div class="d-grid gap-2 d-md-flex justify-content-md-center py-3">
                        <a href="/examschedules/edit/{{$examschedule->id}}" class="btn btn-outline-dark ps-5 pe-5">تعديل</a>
                        <a href="/examschedules/delete/{{$examschedule->id}}" class="btn btn-danger ps-5 pe-5">حذف</a>
                    </div>
                        
                    @endforeach
                    <div>
                        @isset($search)
					        {{$ExamSchedules->appends(['search' => $search])->links()}}
                        @endisset
                        @empty($search)
                            {{$ExamSchedules->links()}}
                        @endempty
                        
                    </div>    
                </div>
            </div>
            @endisset
            @empty($ExamSchedules['0'])
                <h2 class="empty">أضف <a class="empty2" href="/examschedules/create/{{$category->id}}">جدول امتحانات</a></h2>
            @endempty
        </div>
    </div>
@endsection