@extends('layouts.app1')
@section('styles')
<style>
    table{
        background-color:#ced4da;
    }
</style>
@endsection
@section('contant')
@include('shared.navbar13')
<div class="container-fluid">
    <div class="row">
        <div class="table-responsive">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 py-3">
                <form action="/examschedules/create/{{$category->id}}" method="post">
                    @csrf
                    <div class="col-md-4">
                        <label for="inputmonth4" class="form-label"><strong>ادخل اسم شهر الامتحان</strong>
                        </label>
                        <input type="text" class="form-control" name="month" id="month" placeholder="الشهر" required />
                    </div>
                    <table class="table table-hover text-center mt-3">
                        <thead>
                            <tr>
                                <th scope="col">الدرس</th>
                                <th scope="col">التاريخ</th>
                                <th scope="col">اليوم</th>
                                <th scope="col">الدرس</th>
                                <th scope="col">التاريخ</th>
                                <th scope="col">اليوم</th>
                            </tr>
                        </thead>

                        <tbody>
                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="lesson1" id="lesson1"
                                        placeholder="درس 1" required />
                                </th>
                                <td>
                                    <input type="date" name="date1" id="date1" class="form-control" placeholder=""
                                        required>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day1" id="day1" placeholder="اليوم"
                                        required />
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="lesson7" id="lesson7"
                                        placeholder="درس 7" />
                                </td>
                                <td>
                                    <input type="date" name="date7" id="date7" class="form-control" placeholder="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day7" id="day7" placeholder="اليوم" />
                                </td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="lesson2" id="lesson2"
                                        placeholder="درس 2" required />
                                </th>
                                <td>
                                    <input type="date" name="date2" id="date2" class="form-control" placeholder=""
                                        required>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day2" id="day2" placeholder="اليوم"
                                        required />
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="lesson8" id="lesson8"
                                        placeholder="درس 8" />
                                </td>
                                <td>
                                    <input type="date" name="date8" id="date8" class="form-control" placeholder="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day8" id="day8" placeholder="اليوم" />
                                </td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="lesson3" id="lesson3"
                                        placeholder="درس 3" required />
                                </th>
                                <td>
                                    <input type="date" name="date3" id="date3" class="form-control" placeholder=""
                                        required>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day3" id="day3" placeholder="اليوم"
                                        required />
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="lesson9" id="lesson9"
                                        placeholder="درس 9" />
                                </td>
                                <td>
                                    <input type="date" name="date9" id="date9" class="form-control" placeholder="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day9" id="day9" placeholder="اليوم" />
                                </td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="lesson4" id="lesson4"
                                        placeholder="درس 4" required />
                                </th>
                                <td>
                                    <input type="date" name="date4" id="date4" class="form-control" placeholder=""
                                        required>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day4" id="day4" placeholder="اليوم"
                                        required />
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="lesson10" id="lesson10"
                                        placeholder="درس 10" />
                                </td>
                                <td>
                                    <input type="date" name="date10" id="date10" class="form-control" placeholder="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day10" id="day10" placeholder="اليوم" />
                                </td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="lesson5" id="lesson5"
                                        placeholder="درس 5" required />
                                </th>
                                <td>
                                    <input type="date" name="date5" id="date5" class="form-control" placeholder=""
                                        required>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day5" id="day5" placeholder="اليوم"
                                        required />
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="lesson11" id="lesson11"
                                        placeholder="درس 11" />
                                </td>
                                <td>
                                    <input type="date" name="date11" id="date11" class="form-control" placeholder="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day11" id="day11" placeholder="اليوم" />
                                </td>
                            </tr>
                        </tbody>
                        <tbody>
                            <tr>
                                <th scope="row">
                                    <input type="text" class="form-control" name="lesson6" id="lesson6"
                                        placeholder="درس 6" required />
                                </th>
                                <td>
                                    <input type="date" name="date6" id="date6" class="form-control" placeholder=""
                                        required>
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day6" id="day6" placeholder="اليوم"
                                        required />
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="lesson12" id="lesson12"
                                        placeholder="درس 12" />
                                </td>
                                <td>
                                    <input type="date" name="date12" id="date12" class="form-control" placeholder="">
                                </td>
                                <td>
                                    <input type="text" class="form-control" name="day12" id="day12" placeholder="اليوم" />
                                </td>
                            </tr>
                        </tbody>
                    </table>
                    <div class="d-grid gap-2 col-4 mx-auto">
                        <input type="submit" class="btn btn-outline-dark" value="انشاء">
                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
@endsection