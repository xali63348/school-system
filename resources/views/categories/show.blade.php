@extends('layouts.app')
@section('styles')
<style>


    .main-content {
        padding-top: 10px;
        padding-bottom: 50px;
    }

    .stat-card {
        background: #fff;
        padding: 25px;
        margin-bottom: 25px;
        border-radius: 5px;
        overflow: hidden;
        display: flex;
        justify-content: space-between;
        box-shadow: 0 2px 10px rgba(0, 0, 0, 0.1);
        transition: all 0.2s;
    }

    .stat-card:hover {
        transform: translateY(-2px);
        box-shadow: 0 5px 20px rgba(0, 0, 0, 0.1);
    }

    .stat-card__icon-circle {
        height: 60px;
        width: 60px;
        border-radius: 60px;
        background: rgba(0, 123, 255, 0.2);
        display: flex;
        align-items: center;
        justify-content: center;
    }

    .stat-card__icon-circle i {
        font-size: 30px;
        color: #007bff;
    }

    .stat-card__icon.stat-card__icon--success .stat-card__icon-circle {
        background: rgba(40, 167, 69, 0.2);
    }

    .stat-card__icon.stat-card__icon--success .stat-card__icon-circle i {
        color: #28a745;
    }

    .stat-card__icon.stat-card__icon--danger .stat-card__icon-circle {
        background: rgba(220, 53, 69, 0.2);
    }

    .stat-card__icon.stat-card__icon--danger .stat-card__icon-circle i {
        color: #dc3545;
    }

    .stat-card__icon.stat-card__icon--warning .stat-card__icon-circle {
        background: rgba(255, 193, 7, 0.2);
    }

    .stat-card__icon.stat-card__icon--warning .stat-card__icon-circle i {
        color: #ffc107;
    }

    .stat-card__icon.stat-card__icon--primary .stat-card__icon-circle {
        background: rgba(0, 123, 255, 0.2);
    }

    .stat-card__icon.stat-card__icon--primary .stat-card__icon-circle i {
        color: #007bff;
    }

    .stat-card__icon.stat-card__icon--purple .stat-card__icon-circle {
        background: #d9a9e4;
    }

    .stat-card__icon.stat-card__icon--purple .stat-card__icon-circle i {
        color: #4C0070;
    }

    .stat-card__icon.stat-card__icon--pink .stat-card__icon-circle {
        background: #FFC75F;
    }

    .stat-card__icon.stat-card__icon--pink .stat-card__icon-circle i {
        color: #FC5404;
    }

    .stat-card__icon.stat-card__icon--violate .stat-card__icon-circle {
        background: #6998AB;
    }

    .stat-card__icon.stat-card__icon--violate .stat-card__icon-circle i {
        color: #1A374D;
    }

    .stat-card__icon.stat-card__icon--grey .stat-card__icon-circle {
        background: #B4C6A6;
    }

    .stat-card__icon.stat-card__icon--grey .stat-card__icon-circle i {
        color: #66806A;
    }

    .blue {
        text-decoration: none;
        color: black;
    }

    .blue:hover {
        color: #007bff;
    }

    .red {
        text-decoration: none;
        color: black;
    }

    .red:hover {
        color: #dc3545;
    }

    .yellow {
        text-decoration: none;
        color: black;
    }

    .yellow:hover {
        color: #ffc107;
    }

    .green {
        text-decoration: none;
        color: black;
    }

    .green:hover {
        color: #28a745;
    }

    .purple {
        text-decoration: none;
        color: black;
    }

    .purple:hover {
        color: #4C0070;
    }

    .pink {
        text-decoration: none;
        color: black;
    }

    .pink:hover {
        color: #FC5404;
    }

    .violate {
        text-decoration: none;
        color: black;
    }

    .violate:hover {
        color: #0f3c5e;
    }

    .grey {
        text-decoration: none;
        color: black;
    }

    .grey:hover {
        color: #66806A;
    }

    .Vector {
        width: 50px;
        margin-left: 15px;
    }

    
</style>
@endsection
@section('contant')
<main class="mt-5 pt-3">
    <section class="main-content">
        <div class="container">
            <div class="row">
                <a href="/class/show/{{$category->classes->id}}"><img src="/images/Vector.png" class="Vector"
                        alt=""></a>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3">
                    <a href="/absences/show/{{$category->id}}" class="red">
                        <div class="stat-card">
                            <div class="stat-card__content">
                                <p class="text-uppercase mb-1 text-muted">الغيابات</p>
                                <h2 class="count"> {{$count_absences}}</h2>
                            </div>
                            <div class="stat-card__icon stat-card__icon--danger">
                                <div class="stat-card__icon-circle">
                                    <i class="fas fa-users-slash"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3">
                    <a href="/teachers/show/{{$category->id}}" class="yellow">
                        <div class="stat-card">
                            <div class="stat-card__content">
                                <p class="text-uppercase mb-1 text-muted">المدرسين</p>
                                <h2 class="count"> {{$count_teachers}}</h2>
                            </div>
                            <div class="stat-card__icon stat-card__icon--warning">
                                <div class="stat-card__icon-circle">
                                    <i class="fas fa-user-tie"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-3">
                    <a href="/students/show/{{$category->id}}" class="blue">
                        <div class="stat-card">
                            <div class="stat-card__content">
                                <p class="text-uppercase mb-1 text-muted">الطلاب</p>
                                <h2 class="count">{{$count_students}}</h2>
                            </div>
                            <div class="stat-card__icon stat-card__icon--primary">
                                <div class="stat-card__icon-circle">
                                    <i class="fa fa-users"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-1">
                    <a href="/tables/show/{{$category->id}}" class="green">
                        <div class="stat-card">
                            <div class="stat-card__content">
                                <p class="text-uppercase mb-1 text-muted">الجدول</p>
                                <h2 class="count"> {{$count_tables}}</h2>
                            </div>
                            <div class="stat-card__icon stat-card__icon--success">
                                <div class="stat-card__icon-circle">
                                    <i class="fas fa-table"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-1">
                    <a href="/notifications/show/{{$category->id}}" class="purple">
                        <div class="stat-card">
                            <div class="stat-card__content">
                                <p class="text-uppercase mb-1 text-muted">التبليغات</p>
                                <h2 class="count">{{$count_notifications}}</h2>
                            </div>
                            <div class="stat-card__icon stat-card__icon--purple">
                                <div class="stat-card__icon-circle">
                                    <i class="far fa-comment"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-1">
                    <a href="/examschedules/show/{{$category->id}}" class="pink">
                        <div class="stat-card">
                            <div class="stat-card__content">
                                <p class="text-uppercase mb-1 text-muted">جدول الامتحانات</p>
                                <h2 class="count">{{$count_ExamSchedule}}</h2>
                            </div>
                            <div class="stat-card__icon stat-card__icon--pink">
                                <div class="stat-card__icon-circle">
                                    <i class="far fa-calendar-alt"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 py-1">
                    <a href="/examresults/show/{{$category->id}}" class="violate">
                        <div class="stat-card">
                            <div class="stat-card__content">
                                <p class="text-uppercase mb-1 text-muted">جدول النتائج</p>
                                <h2 class="count">{{$count_ExamResult}}</h2>
                            </div>
                            <div class="stat-card__icon stat-card__icon--violate">
                                <div class="stat-card__icon-circle">
                                    <i class="fas fa-poll"></i>
                                </div>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
        </div>
    </section>
</main>
@endsection