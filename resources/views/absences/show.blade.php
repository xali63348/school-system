@section('styles')
<style>


    .slash {
        color: black;
        font-size: 22px;
        text-decoration: none;
    }

    .slash:hover {
        color: #dc3545;
    }

    .absences {
        float: right;
    }

    .Vector {
        width: 50px;
        margin-left: 15px;
    }
    .empty2 {
        color: #f44336;
        text-decoration: none;
    }

    .empty2:hover {
        color: #dc3545;
        text-decoration: none;
    }

    .empty {
        text-align: center;
        margin-top: 15%;
    }

    input[type=text] {
        float: left;
        padding: 5px;
        font-size: 17px;
        border: 1px solid #ced4da;
        border-radius: 0.25rem;
    }

    button {
        float: left;
        padding: 6px 10px;
        margin-right: 14px;
        background: #ddd;
        font-size: 17px;
        cursor: pointer;
    }
</style>
@endsection
@extends('layouts.app1')
@section('contant')

@include('shared.navbar3')
<div class="container-fluid">
    <div class="row">
        <span>
            <a href="/absences/create/{{$category->id}}" class="slash">
                <span class="absences">انشاء غيابات<i class="fas fa-user-slash"></i></span>
            </a>
        </span>

        <div class="col-12 py-1">
            <div class="table-responsive">
                @isset($category->absences['0'])
                <table class="table  table-danger text-center table-hover" style="width:100%">
                    <thead>
                        <tr>
                            <th scope="col">الاسم</th>
                            <th scope="col">الدرس 1</th>
                            <th scope="col">الدرس 2</th>
                            <th scope="col">الدرس 3</th>
                            <th scope="col">الدرس 4</th>
                            <th scope="col">الدرس 5</th>
                            <th scope="col">الدرس 6</th>
                            <th scope="col">يوم كامل</th>
                            <th scope="col">التاريخ</th>
                            <th scope="col">تحديث المعلومات</th>
                            <th scope="col">اجرائات</th>
                        </tr>
                    </thead>

                    @foreach($absences as $absence)
                    <tbody>
                        <tr>
                            <td>{{$absence->students->name}}</td>
                            <td>{{$absence->days_of_absence['lesson1']}}</td>
                            <td>{{$absence->days_of_absence['lesson2']}}</td>
                            <td>{{$absence->days_of_absence['lesson3']}}</td>
                            <td>{{$absence->days_of_absence['lesson4']}}</td>
                            <td>{{$absence->days_of_absence['lesson5']}}</td>
                            <td>{{$absence->days_of_absence['lesson6']}}</td>
                            <td>{{$absence->days_of_absence['allLasson']}}</td>
                            <td>{{$absence->date}}</td>
                            <td>{{$absence->update_data}}</td>

                            <td><a href="/absences/update/{{$absence->id}}" class="btn btn-danger">تحديث</a></td>
                        </tr>
                    </tbody>
                    @endforeach
                </table>
                @isset($search)
                    {{$absences->appends(['search' => $search])->links()}}
                @endisset
                @empty($search)
                    {{$absences->links()}}
                @endempty
                @endisset

                @empty($category->students['0'])
                    @include('shared.card-warining-absences')
                @endempty

                @if(isset($category->students['0']))
                @empty($category->absences['0'])
                <h2 class="empty">أضف <a class="empty2" href="/absences/create/{{$category->id}}">غيابات الطلاب</a></h2>
                @endempty
                @endif
            </div>
        </div>

    </div>
</div>
@endsection