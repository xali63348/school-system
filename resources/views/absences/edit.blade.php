@extends('layouts.app1')
@section('styles')
<style>
    .main-content {
        padding-top: 2px;
        padding-bottom: 2px;
    }

    .info-card {
        background: #fff;
        text-align: center;
        padding: 50px 30px;
        margin-bottom: 30px;
        border-radius: 3px;
        box-shadow: 0 10px 30px rgba(0, 0, 0, 0.1);
    }

    .info-card .info-card_icon {
        height: 125px;
        width: 125px;
        margin: 0 auto 50px auto;
        border: 5px solid #4caf50;
        border-radius: 125px;
        display: flex;
        justify-content: center;
        align-items: center;
        position: relative;
    }

    .info-card .info-card_icon i {
        font-size: 50px;
        color: #4caf50;
    }

    .info-card .info-card_icon .info-card_img-icon {
        height: 60px;
        width: 60px;
        object-fit: contain;
    }

    .info-card .info-card_label {
        margin-bottom: 15px;
    }

    .info-card .info-card_message {
        margin-bottom: 15px;
    }

    .info-card .btn {
        background: #03a9f4;
        border-color: #03a9f4;
        box-shadow: 0 3px 10px rgba(0, 0, 0, 0.1);
    }

    .info-card--success .info-card_icon {
        border-color: #4caf50;
    }

    .info-card--success .info-card_icon i {
        color: #4caf50;
    }

    .info-card--danger .info-card_icon {
        border-color: #f44336;
    }

    .info-card--danger .info-card_icon i {
        color: #f44336;
    }

    .info-card--warning .info-card_icon {
        border-color: #ff9800;
    }

    .info-card--warning .info-card_icon i {
        color: #ff9800;
    }

    table {
        background-color: #ced4da;
    }
</style>
@endsection
@section('contant')
@include('shared.navbar19')
<div class="container-fluid">
    <div class="row">
        <div class="table-responsive">
            <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 py-3">
                <table class="table table-hover text-center table-border">
                    <thead>
                        <tr>
                            <th scope="col">الاسم</th>
                            <th scope="col">الدرس 1</th>
                            <th scope="col">الدرس 2</th>
                            <th scope="col">الدرس 3</th>
                            <th scope="col">الدرس 4</th>
                            <th scope="col">الدرس 5</th>
                            <th scope="col">الدرس 6</th>
                            <th scope="col">يوم كامل</th>
                            <th scope="col">التاريخ</th>
                            <th scope="col">اجرائات</th>
                        </tr>
                    </thead>
                    <form method='post' action="/absences/update/{{$absence->id}}">
                        @csrf
                        <tbody>
                            <tr>
                                <th scope="row">{{$absence->students->name}}</th>
                                <td>
                                    <select name="quantity0" id="inputState" class="form-select ">
                                        <option selected>{{$absence->days_of_absence['lesson1']}}</option>
                                        <option value="--">--</option>
                                        <option value="حاضر">حاضر</option>
                                        <option value="غياب">غياب</option>
                                    </select>

                                </td>
                                <td>
                                    <select name="quantity1" id="inputState" class="form-select">
                                        <option selected>{{$absence->days_of_absence['lesson2']}}</option>
                                        <option value="--">--</option>
                                        <option value="حاضر">حاضر</option>
                                        <option value="غياب">غياب</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="quantity2" id="inputState" class="form-select">
                                        <option selected>{{$absence->days_of_absence['lesson3']}}</option>
                                        <option value="--">--</option>
                                        <option value="حاضر">حاضر</option>
                                        <option value="غياب">غياب</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="quantity3" id="inputState" class="form-select">
                                        <option selected>{{$absence->days_of_absence['lesson4']}}</option>
                                        <option value="--">--</option>
                                        <option value="حاضر">حاضر</option>
                                        <option value="غياب">غياب</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="quantity4" id="inputState" class="form-select">
                                        <option selected>{{$absence->days_of_absence['lesson5']}}</option>
                                        <option value="--">--</option>
                                        <option value="حاضر">حاضر</option>
                                        <option value="غياب">غياب</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="quantity5" id="inputState" class="form-select">
                                        <option selected>{{$absence->days_of_absence['lesson6']}}</option>
                                        <option value="--">--</option>
                                        <option value="حاضر">حاضر</option>
                                        <option value="غياب">غياب</option>
                                    </select>
                                </td>
                                <td>
                                    <select name="quantity6" id="inputState" class="form-select">
                                        <option selected>{{$absence->days_of_absence['allLasson']}}</option>
                                        <option value="--">--</option>
                                        <option value="غياب">غياب</option>
                                    </select>
                                </td>
                                <td>
                                    <input type="date" class="form-control" name="update_data"
                                        value="{{date('Y-m-d', time())}}">
                                </td>


                                <td>
                                    <input type="submit" class="btn btn-outline-dark" value="تحديث" />
                                </td>

                            </tr>
                        </tbody>
                    </form>
                </table>
            </div>
        </div>
    </div>
</div>
@endsection