@extends('layouts.app1')
@section('styles')
    <style>
        .Vector{
            margin-top:10px;
            width: 50px;
            margin-left:15px;
        }
        
        table{
            background-color:#ced4da;
        }
    
    </style>
@endsection
@section('contant')
@include('shared.navbar5')
    <div class="container-fluid">
        <div class="row">
            <div class="table-responsive">
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12 py-3">
                    @if(isset($category->students['0']))
                            <table class="table table-hover text-center table-border"  >
                            <thead>
                                <tr>
                                    <th scope="col">الاسم</th>
                                    <th scope="col">الدرس 1</th>
                                    <th scope="col">الدرس 2</th>
                                    <th scope="col">الدرس 3</th>
                                    <th scope="col">الدرس 4</th>
                                    <th scope="col">الدرس 5</th>
                                    <th scope="col">الدرس 6</th>
                                    <th scope="col">يوم كامل</th>
                                    <th scope="col">التاريخ</th>
                                    <th scope="col">اجرائات</th>
                                </tr>
                            </thead>
                            @foreach($students as $student)
                                <form method='post' action="/absences/create/{{$student->id}}">
                                    @csrf
                            <tbody>
                                <tr>
                                    <th scope="row">{{$student->name}}</th>
                                        <td>
                                            <select name="quantity0" id="inputState" class="form-select">
                                            <option selected>--</option>
                                            <option value="حاضر">حاضر</option>
                                            <option value="غياب">غياب</option>
                                            </select>
                                                
                                        </td>
                                        <td>
                                            <select name="quantity1" id="inputState" class="form-select">
                                            <option selected>--</option>
                                            <option value="حاضر">حاضر</option>
                                            <option value="غياب">غياب</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="quantity2" id="inputState" class="form-select">
                                            <option selected>--</option>
                                            <option value="حاضر">حاضر</option>
                                            <option value="غياب">غياب</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="quantity3" id="inputState" class="form-select">
                                            <option selected>--</option>
                                            <option value="حاضر">حاضر</option>
                                            <option value="غياب">غياب</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="quantity4" id="inputState" class="form-select">
                                            <option selected>--</option>
                                            <option value="حاضر">حاضر</option>
                                            <option value="غياب">غياب</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select name="quantity5" id="inputState" class="form-select">
                                            <option selected>--</option>
                                            <option value="حاضر">حاضر</option>
                                            <option value="غياب">غياب</option>
                                            </select>
                                        </td>
                                        <td>
                                            <select  name="quantity6" id="inputState" class="form-select">
                                            <option selected>--</option>
                                            <option value="غياب">غياب</option>
                                            </select>
                                        </td>
                                        <td>
                                            <input type="date" class="form-control" name="date" value="{{date('Y-m-d', time())}}">
                                        </td>
                                        
                                    
                                        <td>
                                            <input type="submit" class="btn btn-outline-dark" value="انشاء" />
                                        </td>
                                        
                                    </tr>
                                </tbody>
                                </form>
                            @endforeach
                        </table>
                        @isset($search)
                        {{$students->appends(['search' => $search])->links()}}
                        @endisset
                        @empty($search)
                            {{$students->links()}}
                        @endempty
                    </div>
                
            @endisset
            @empty($category->students['0'])
                <div class="py-3">
                    @include('shared.card-warining-absences')
                </div>
            @endempty
            </div>
        </div>
    </div>
@endsection