@extends('layouts.app3')
@section('styles')
<style>
    * {
        margin: 0;
        padding: 0;
        box-sizing: border-box;
    }

    *,
    *:before,
    *:after {
        box-sizing: border-box;
    }

    .categories {
        color: black;
        font-size: 22px;
    }

    .categories:hover {
        color: #4E98FF;
    }

    .category {
        color: black;
        font-size: 22px;
        text-decoration: none;
    }

    .category:hover {
        color: #4E98FF;
        text-decoration: underline;
    }

    .shadow {
        box-shadow: 0 5px 20px rgba(0, 0, 0, 0.06) !important;
    }

    .main-content {
        padding-top: 5px;
        padding-bottom: 90px;
    }

    .banner {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 125px;
        background-image: url("/images/banner.jpg");
        background-position: center;
        background-size: cover;
    }

    .img-circle {
        height: 150px;
        width: 150px;
        border-radius: 150px;
        border: 3px solid #fff;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        z-index: 1;
    }

    
 

    .input-field {
        max-width: 380px;
        width: 100%;
        background-color: #f0f0f0;
        margin-left: 50px;
        height: 55px;
        border-radius: 55px;
        display: grid;
        grid-template-columns: 15% 85%;
        padding: 0 0.4rem;
        position: relative;
    }

    .input-field i {
        text-align: center;
        line-height: 55px;
        color: #acacac;
        transition: 0.5s;
        font-size: 1.1rem;
    }

    .input-field input {
        background: none;
        outline: none;
        border: none;
        line-height: 1;
        font-weight: 600;
        font-size: 1.4rem;
        color: #333;
    }

    .btn1 {
        width: 150px;
        background-color: #5995fd;
        border: none;
        outline: none;
        height: 49px;
        border-radius: 49px;
        color: #fff;
        text-transform: uppercase;
        font-weight: 600;
        margin: 10px 0;
        cursor: pointer;
        transition: 0.5s;
        margin-left: 150px !important;
    }

    .input-field {
        max-width: 380px;
        width: 100%;
        background-color: #f0f0f0;
        margin-left: 50px;
        height: 55px;
        border-radius: 55px;
        display: grid;
        grid-template-columns: 15% 85%;
        padding: 0 0.4rem;
        position: relative;
    }


    @media(max-width: 414px) {
        .input-field {
            margin-left: 5px;
        }

        .btn1 {
            margin-left: 130px !important;
        }

    }

    @media(max-width: 320px) {
        .input-field {
            max-width: 265px;
            margin-left: 10px;
        }

        .btn1 {
            height: 48px;
            margin-left: 70px !important;
        }

    }

 

    .input-field i {
        text-align: center;
        line-height: 55px;
        color: #acacac;
        transition: 0.5s;
        font-size: 1.1rem;
    }

    .input-field input {
        background: none;
        outline: none;
        border: none;
        line-height: 1;
        font-weight: 600;
        font-size: 1.4rem;
        color: #333;
    }

    .input-field input::placeholder {
        color: #aaa;
        font-weight: 500;
    }

    .form-control1[type="file"] {
        display: none;
    }

    .label1 {
        color: #fff;
        background-color: #4d84e2;
        position: absolute;
        height: 40px;
        width: 160px;
        justify-content: center;
        display: flex;
        border-radius: 4px;
        cursor: pointer;
        margin-left: 53px;
    }


    .fa-images {
        margin-top: 10px;
        margin-right: 7px;
    }

    .choose-photo {
        margin-top: 5px;
    }

    .empty2 {
        color: #023e8a;
        text-decoration: none;
    }

    .empty2:hover {
        text-decoration: none;
    }

    .empty {
        text-align: center;
        margin-top: 15%;
    }

    #animContainer {
        width: 20%;
        height: 20%;
    }

    #custom-button {
        color: #fff;
        background-color: #4d84e2;
        position: absolute;
        height: 40px;
        width: 180px;
        justify-content: center;
        display: flex;
        border-radius: 4px;
        cursor: pointer;
        margin-left: 50px;
    }

    .form-control[type=file] {
        overflow: hidden;
    }
</style>
@endsection
@section('contant')
@include('shared.navbar0')

<main class="mt-5 pt-3">
    <a href="" data-bs-toggle="modal" data-bs-target="#addCategory" class="nav-link px-3 categories ">
        <span>أضف شعبة <i class="far fa-address-card"></i></span>
    </a>

    {{-- add category --}}
    <div class="modal fade" id="addCategory" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">انشاء شعبة</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method='post' action="/category/create/{{$classes->id}}" class="row g-3"
                        enctype="multipart/form-data">
                        @csrf
                        <div class="input-field">
                            <i class="fas fa-chalkboard-teacher"></i>
                            <input type="text" name="name" id="name" placeholder="أضف الشعبة" required />
                        </div>
                        <div class="mb-3 my-3 ">
                            <input type="file" name="logo" id="real-file" hidden="hidden" />
                            <a type="button" id="custom-button"><i class="fas fa-images"></i><span
                                    class="choose-photo">اختر صوره</span></a>
                        </div>
                        <input type="submit" class="btn1 my-5" value="انشاء" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end --}}

    <section class="main-content">
        <div class="container">
            <div class="row">
                @isset($classes->category['0'])
                @foreach($category as $category)
                <div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-4">

                    <div
                        class="profile-card card rounded-lg shadow p-4 p-xl-5 mb-4 text-center position-relative overflow-hidden">
                        <div class="banner"></div>
                        @isset($category->image)
                        <img src="{{ asset($category->image) }}" alt="error" class="img-circle mx-auto mb-3">
                        @endisset
                        @empty($category->image)
                        <img src="/images/class2.png" alt="error" class="img-circle mx-auto mb-3">
                        @endempty
                        <h3 class="mb-4"><a href="/category/show/{{ $category->id }}" class="category">{{
                                $category->name }}</a></h3>
                        <div class="social-links d-flex justify-content-center">
                            <a href="/category/delete/{{ $category->id }}" class="mx-2 btn btn-danger">حذف</a>
                            <a href="" data-bs-toggle="modal" data-bs-target="#editCategory{{$category->id}}"
                                class="mx-2 btn btn-outline-primary">تعديل</a>
                        </div>

                        {{-- edit category --}}
                        <div class="modal fade" id="editCategory{{$category->id}}" data-bs-backdrop="static"
                            data-bs-keyboard="false" tabindex="-1" aria-labelledby="staticBackdropLabel"
                            aria-hidden="true">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <h5 class="modal-title" id="staticBackdropLabel">تعديل الشعبه</h5>
                                        <button type="button" class="btn-close" data-bs-dismiss="modal"
                                            aria-label="Close"></button>
                                    </div>
                                    <div class="modal-body">
                                        <form class="row g-3" method='post' action="/category/edit/{{$category->id}}"
                                            enctype="multipart/form-data">
                                            @csrf
                                            <div class="input-field">
                                                <i class="fas fa-chalkboard-teacher"></i>
                                                <input type="text" name="name" id="name" placeholder="اسم الشعبه"
                                                    value="{{$category->name}}" required />
                                            </div>
                                            <div class="mb-3 my-3">
                                                <input class="form-control1" name="logoo" type="file" id="file">
                                                <label class="label1" for="file">
                                                    <i class="fas fa-images"></i>
                                                    <span class="choose-photo">اختر صوره</span>
                                                </label>
                                            </div>
                                            <input type="submit" class="btn1 my-5" value="تعديل" />
                                        </form>
                                    </div>
                                </div>
                            </div>
                        </div>
                        {{-- end --}}

                    </div>
                </div>
                @endforeach
                @endisset

                @empty($classes->category['0'])

                <div id="animContainer"></div>
                <h2 class="empty mt-3">اضف <a class="empty2" href="">شعبه
                    </a></h2>
                @endempty

            </div>
        </div>
    </section>
</main>

@endsection
@section('script')

<script>
    const realFileBtn = document.getElementById("real-file");
                const customBtn = document.getElementById("custom-button");
                const customTxt = document.getElementById("custom-text");
                
                customBtn.addEventListener("click", function() {
                realFileBtn.click();
                });
                
                realFileBtn.addEventListener("change", function() {
                if (realFileBtn.value) {
                customTxt.innerHTML = realFileBtn.value.match(
                /[\/\\]([\w\d\s\.\-\(\)]+)$/
                )[1];
                } else {
                customTxt.innerHTML = "No file chosen, yet.";
                }
                });
</script>

@endsection