@extends('layouts.app1')
@section('styles')
<style>
    input[type="file"] {
        display: none;
    }

    .lab {
        color: #fff;
        background-color: #4d84e2;
        position: absolute;
        height: 40px;
        width: 160px;
        justify-content: center;
        display: flex;
        border-radius: 4px;
        cursor: pointer;
    }

    .choose-photo {
        margin-top: 5px;
    }

    .fa-images {
        margin-top: 7px;
        margin-right: 7px;
    }

    .mt-1 {
        padding: 5px;
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.10) !important;
    }

    body {
        background: #f5f5f5;
    }

    .bd-callout-warning {
        border-left-color: #f0ad4e !important;
    }

    .bd-callout {
        padding: 1.25rem;
        margin-top: 1.25rem;
        margin-bottom: 1.25rem;
        border: 1px solid #e9ecef;
        border-left-width: 0.25rem;
        border-radius: 0.25rem;
    }
</style>
@endsection
@section('contant')
@include('shared.navbar20')
<div class="contianer">
    <div class="col-12 col-sm-12  col-md-10 offset-md-1 col-lg-8 offset-lg-2 mt-1">
        <h3 class="text-center">تعديل الطالب</h3>
        <form class="row g-3 py-3" method='post' action="/students/edit/{{$student->id}}" enctype="multipart/form-data">
            @csrf
            <div class="col-12 col-sm-12   col-md-12">
                <label for="inputEmail4" class="form-label">الاسم الكامل</label>
                <input type="text" class="form-control" name="name" id="name" value="{{$student->name}}"
                    value="{{old('name')}}" placeholder="الاسم الكامل" required>
            </div>
            <div class="col-6 col-sm-3 col-md-3">
                <label for="inputAddress" class="form-label"></label>
                <input type="text" class="form-control" name="country" id="country"
                    value="{{$student->address['country']}}" value="{{old('country')}}" placeholder="البلد" required>
            </div>
            <div class="col-6 col-sm-3 col-md-3">
                <label for="inputAddress" class="form-label"> </label>
                <input type="text" class="form-control" name="city" id="city" value="{{$student->address['city']}}"
                    value="{{old('city')}}" placeholder="المدينة" required>
            </div>
            <div class="col-6 col-sm-3 col-md-3">
                <label for="inputAddress" class="form-label"> </label>
                <input type="text" class="form-control" name="area" id="area" value="{{$student->address['area']}}"
                    value="{{old('arae')}}" placeholder="المنطقة" required>
            </div>
            <div class="col-6 col-sm-3 col-md-3">
                <label for="inputAddress" class="form-label"> </label>
                <input type="text" class="form-control" name="extra" id="extra" value="{{$student->address['extra']}}"
                    value="{{old('extra')}}" placeholder="اضافي">
            </div>
            <div class="col-6 col-sm-5 col-md-5">
                <label for="inputAddress2" class="form-label"> رقم الهاتف</label>
                <input type="number" class="form-control" name="number_phone" id="number_phone"
                    value="{{$student->number_phone}}" placeholder="رقم الهاتف">
            </div>
            <div class="col-6 col-sm-5 col-md-5">
                <label for="inputAddress2" class="form-label"> رقم الكود</label>
                <input type="text" class="form-control" name="number_code" id="number_code"
                    value="{{$student->number_code}}" placeholder="اضف كود" required>
            </div>
            <div class="col-4 col-sm-2 col-md-2">
                <label for="inputZip" class="form-label">التولد</label>
                <input type="number" class="form-control" name="age" placeholder="التولد" value="{{$student->age}}"
                    value="{{old('age')}}" id="age" required>
            </div>
            <div class="mb-3 my-3">
                <input class="form-control" name="logo" type="file" id="file">
                <label class="lab" for="file">
                    <i class="fas fa-images"></i>
                   <span class="choose-photo">اختر صورة</span>
                </label>
            </div>
            <div class="d-grid gap-2">
                <input type="submit" class="btn btn-outline-dark my-5" value="تعديل" />
            </div>
        </form>
    </div>
    <div class="col-12 col-sm-12  col-md-10 offset-md-1 col-lg-8 offset-lg-2">
        <div class="bd-callout bd-callout-warning">
            <code>2MB</code> يرجى ملاحظة أن الحد الأقصى لحجم الصورة المطلوب هو
        </div>
    </div>
</div>
</div>
@endsection