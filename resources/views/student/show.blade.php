@extends('layouts.app1')
@section('styles')
    <style>
.shadow {
	box-shadow: 0 5px 20px rgba(0, 0, 0, 0.06) !important;
}



.banner {
	position: absolute;
	top: 0;
	left: 0;
	width: 100%;
	height: 125px;
	background-image: url("/images/banner1.jpg");
	background-position: center;
	background-size: cover;
}

.img-circle {
	height: 150px;
	width: 150px;
	border-radius: 150px;
	border: 3px solid #fff;
	box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
	z-index: 1;
}
.categories{
  color:black;
  font-size:19px;
  text-decoration: none;
}
.categories:hover{
  color:#4E98FF;
}
.graduate{
	float: right;
}
.Vector{
    width: 50px;
    margin-left:15px;
}
.empty{
	text-align:center;
	margin-top:15%;
}
.empty2{
	text-decoration: none;
}
input[type=text]{
            float: left;
            padding: 5px;
            font-size: 17px;
            border: 1px solid #ced4da;
            border-radius: 0.25rem;
        }
        button {
            float: left;
            padding: 6px 10px;
            margin-right: 14px;
            background: #ddd;
            font-size: 17px;
            cursor: pointer;  
        }
    </style>
@endsection
@section('contant')  
	@include('shared.navbar2')
		<div class="container-fluid">
			<div class="row">
				<span>
					<a href="/students/create/{{$category->id}}" class="categories">
						<span class="graduate">انشاء طالب <i class="fas fa-user-graduate"></i></span>
					</a>
				</span>
			@isset($students['0'])
                @foreach ($students as $student)
				<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xxl-3 my-3">
					<div class="profile-card card rounded-lg shadow p-4 p-xl-5 mb-4 text-center position-relative overflow-hidden">
						<div class="banner"></div>
						@isset($student->images)
							<img src="{{asset($student->images)}}" alt="" class="img-circle mx-auto mb-3">
						@endisset
						@empty($student->images)
							<img src="/images/user.png" alt="" class="img-circle mx-auto mb-3">
						@endempty
						
						<h3 class="mb-4">{{$student->name}}</h3>
						<div class="text-left mb-4">
							<p class="mb-2"><i class="fas fa-user-graduate"></i></i> {{$student->age}}</p>
							<p class="mb-2"><i class="fa fa-phone mr-2"></i> {{$student->number_phone}}</p>
							<p class="mb-2"><i class="fas fa-code"></i> {{$student->number_code}}</p>
							<p class="mb-2"><i class="fa fa-map-marker-alt mr-2"></i> {{$student->address['country']}}, {{$student->address['city']}},{{$student->address['area']}},{{$student->address['extra']}}</p>
							
							<a href="/students/delete/{{$student->id}}" class="mx-2 btn btn-danger my-2">حذف</a>
                    		<a href="/students/edit/{{$student->id}}" class="mx-2 btn btn-outline-primary">تعديل</a>
						</div>
					</div>
				</div>
                @endforeach
				
				@isset($search)
					{{$students->appends(['search' => $search])->links()}}
				@endisset
				@empty($search)
					{{$students->links()}}
				@endempty
				@endisset

				@empty($students['0'])
					<h2 class="empty">أضف <a class="empty2" href="/students/create/{{$category->id}}">طلاب</a></h2>
				@endempty
			</div>
		</div>

@endsection