<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>نظام المدرسة - تسجيل الدخول</title>
    <script
      src="https://kit.fontawesome.com/64d58efce2.js"
      crossorigin="anonymous"
    ></script>
    <link rel="stylesheet" href="/css/toastr.min.css">
    <link rel="stylesheet" href=" {{ URL::asset('register.css') }}">
    <style>
      @import url('https://fonts.googleapis.com/css2?family=Almarai&family=Cairo:wght@200;400&family=Noto+Naskh+Arabic:wght@700&family=Readex+Pro&display=swap');
    </style>
    
    <style>
      body {
        font-family: 'Almarai', sans-serif;
        font-family: 'Cairo', sans-serif;
        font-family: 'Noto Naskh Arabic', serif;
        font-family: 'Readex Pro', sans-serif;
      }
    </style>
</head>
<body>
    
<div class="container">
      <div class="forms-container">
        <div class="signin-signup">
          <form method='post' action="/users/store" class="sign-in-form">
            @csrf
            <h2 class="title">انشاء حساب جديد</h2>
            <div class="input-field">
              <i class="fas fa-user"></i>
              <input type="text" name="name" id="name"  placeholder="الاسم" required/>
            </div>

            <div class="input-field">
              <i class="fas fa-envelope"></i>
              <input type="email" name="email" id="email" placeholder="البريد الالكتروني" required />
            </div>

            <div class="input-field">
            <i class="fas fa-mobile-alt"></i>
              <input type="Number" name="number_phone" id="number_phone" placeholder="رقم الهاتف" required/>
            </div>

            <div class="input-field">
            <i class="fas fa-code"></i>
              <input type="text" name="code" id="code" placeholder="ادخل كود" required/>
            </div>

            <div class="input-field">
              <i class="fas fa-lock"></i>
              <input type="password" name="password" id="password" placeholder="كلمة السر" required/>
            </div>
            <div class="input-field">
              <i class="fas fa-lock"></i>
              <input type="password" name="confirm" id="confirm" placeholder="تاكيد كلمة السر" required />
            </div>
            <input type="submit" class="btn" value="انشاء" />
          </form>
        </div>
      </div>

      <div class="panels-container">
        <div class="panel left-panel">
          
          <img src="{{asset('images/register.svg')}}" class="image" alt="" />
        </div>
    </div>

    


    <script src="login.js"></script>
    <script src="/js/jquery-3.6.0.min.js"></script>
    <script src="/js/bootstrap.min.js"></script>
    <script src="/js/bootstrap.bundle.min.js"></script>
    <script src="/js/toastr.min.js"></script>
    {!! Toastr::message() !!}
    <script>
      toastr.options = {
            "closeButton": true,
            "debug": true,
            "newestOnTop": true,
            "progressBar": false,
            "positionClass": "toast-bottom-center",
            "preventDuplicates": false,
            "showDuration": "300",
            "hideDuration": "1000",
            "timeOut": "7000",
            "extendedTimeOut": "1000",
            "showEasing": "swing",
            "hideEasing": "linear",
            "showMethod": "fadeIn",
            "hideMethod": "fadeOut"
          };
          @foreach($errors->all() as $error)
              toastr.error("{{$error}}")
          @endforeach
           
    </script>
</body>
</html>


















