@extends('layouts.app')
@section('styles')
<style>
    .edit {
        text-decoration: none;
    }

    .edit:hover {
        text-decoration: underline;
        color: black;
    }

    .tr:hover {
        background: #E5E5E5;
    }

    .edit_name {
        width: 100%;
        height: 100%;
        position: fixed;
        left: 0;
        top: 0;
        background-color: transparent;
        z-index: 2;
        transform: translateY(-44.5%) scale(0);
        transition: .4s ease-in-out;
    }

    .edit_name.active {
        transform: translateY(0%) scale(100%);
        background-color: rgba(0, 0, 0, .8);
    }

    .edit_number_phone {
        width: 100%;
        height: 100%;
        position: fixed;
        left: 0;
        top: 0;
        background-color: transparent;
        z-index: 2;
        transform: translateY(-44.5%) scale(0);
        transition: .4s ease-in-out;
    }

    .edit_number_phone.active {
        transform: translateY(0%) scale(100%);
        background-color: rgba(0, 0, 0, .8);
    }

    .pop_up_container {
        display: flex;
        width: 100%;
        height: 100%;
    }

    .pop_up_body {
        margin: auto;
        width: 500px;
        background-color: #fff;
        border-radius: 10px;
        text-align: center;
        padding: 100px 15px 50px 10px;
        position: relative;
    }

    .pop_up_body p {
        font-size: 28px;
        font-weight: 600;
        color: #22262D;
        margin-bottom: 40px;
    }

    .pop_up_close {
        position: absolute;
        top: 15px;
        right: 20px;
        font-size: 21px;
        cursor: pointer;
    }


    .btn1 {
        width: 150px;
        background-color: #5995fd;
        border: none;
        outline: none;
        height: 49px;
        border-radius: 49px;
        color: #fff;
        text-transform: uppercase;
        font-weight: 600;
        margin: 10px 0;
        cursor: pointer;
        transition: 0.5s;
        margin-left: 150px !important;
    }

    .input-field {
        max-width: 380px;
        width: 100%;
        background-color: #f0f0f0;
        margin-left: 50px;
        height: 55px;
        border-radius: 55px;
        display: grid;
        grid-template-columns: 15% 85%;
        padding: 0 0.4rem;
        position: relative;
    }

    .input-field i {
        text-align: center;
        line-height: 55px;
        color: #acacac;
        transition: 0.5s;
        font-size: 1.1rem;
    }

    @media (max-width: 375px) {

        .input-field {
            max-width: 280px;
        }

        .btn1 {
            margin-left: 101px !important;
        }
    }

    @media (max-width: 414px) {

        .input-field {
            max-width: 310px;
        }

        .btn1 {
            margin-left: 125px !important;
        }
    }


    .input-field input {
        background: none;
        outline: none;
        border: none;
        line-height: 1;
        font-weight: 600;
        font-size: 1.4rem;
        color: #333;
    }

    .input-field input::placeholder {
        color: #aaa;
        font-weight: 500;
    }
</style>
@endsection

@section('contant')
<main class="mt-5 pt-3">

    <!-- edit name -->
    <div class="modal fade" id="editName" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">تعديل الاسم</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method='post' action="/users/edit/name/{{auth()->user()->id}}" class="row g-3">
                        @csrf
                        <div class="input-field">
                            <i class="fas fa-user"></i>
                            <input type="text" name="name" id="name" value="{{auth()->user()->name}}"
                                placeholder="الاسم" required />
                        </div>

                        <div class="input-field">
                            <i class="fas fa-unlock-alt"></i>
                            <input type="password" name="password" id="password" placeholder="كلمة المرور" required />
                        </div>

                        <input type="submit" class="btn1 my-5" value="تعديل" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end --}}

    <!-- edit number phone -->
    <div class="modal fade" id="editNumber" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">تعديل رقم الهاتف</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method='post' action="/users/edit/numberphone/{{auth()->user()->id}}" class="row g-3">
                        @csrf
                        <div class="input-field">
                            <i class="fas fa-mobile-alt"></i>
                            <input type="number" name="number" id="number" value="{{auth()->user()->number_phone}}"
                                placeholder="رقم الهاتف" required />
                        </div>

                        <div class="input-field">
                            <i class="fas fa-unlock-alt"></i>
                            <input type="password" name="password" id="password" placeholder="كلمة المرور" required />
                        </div>

                        <input type="submit" class="btn1 my-5" value="تعديل" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end --}}

    <!-- edit email -->
    <div class="modal fade" id="editEmail" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">تعديل البريد الالكتروني</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method='post' action="/users/edit/email/{{auth()->user()->id}}" class="row g-3">
                        @csrf
                        <div class="input-field">
                            <i class="fas fa-envelope"></i>
                            <input type="text" name="email" id="email" value="{{auth()->user()->email}}"
                                placeholder="البريد الالكتروني" required />
                        </div>

                        <div class="input-field">
                            <i class="fas fa-unlock-alt"></i>
                            <input type="password" name="password" id="password" placeholder="كلمة المرور" required />
                        </div>

                        <input type="submit" class="btn1 my-5" value="تعديل" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end --}}

    <!-- edit password -->
    <div class="modal fade" id="editPassword" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
        aria-labelledby="staticBackdropLabel" aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <h5 class="modal-title" id="staticBackdropLabel">Edit Password</h5>
                    <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
                </div>
                <div class="modal-body">
                    <form method='post' action="/users/edit/password/{{auth()->user()->id}}" class="row g-3">
                        @csrf
                        <div class="input-field">
                            <i class="fas fa-unlock-alt"></i>
                            <input type="password" name="password" id="password" placeholder="كلمة المرور القديمه" required />
                        </div>
                        <div class="input-field">
                            <i class="fas fa-lock"></i>
                            <input type="password" name="newpassword" id="newpassword" placeholder="كلمة السر الجديده"
                                required />
                        </div>

                        <div class="input-field">
                            <i class="fas fa-lock"></i>
                            <input type="password" name="confirm" id="confirm" placeholder="تاكيد كلمة المرور"
                                required />
                        </div>

                        <input type="submit" class="btn1 my-5" value="تعديل" />
                    </form>
                </div>
            </div>
        </div>
    </div>
    {{-- end --}}

    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12">
                @include('shared.profile')
                @component('shared.nav&tabs',['information'=>'active'])
                @endcomponent
                <div class="table-responsive col-lg-12 py-3">
                    <table class="table">
                        <thead>
                            <tr>
                                <th scope="col">المعلومات</th>
                                <th scope="col"></th>
                                <th scope="col"></th>
                            </tr>
                        </thead>
                        <tbody>
                            <tr class="tr">
                                <th class="w-25" scope="row">الاسم</th>
                                <td class="w-25">{{auth()->user()->name}}</td>

                                <td class="w-25"><a class="edit" href="" data-bs-toggle="modal"
                                        data-bs-target="#editName">تعديل</a></td>
                            </tr>
                            <tr class="tr">
                                <th class="w-25" scope="row">رقم الهاتف</th>
                                <td class="w-25">{{auth()->user()->number_phone}}</td>

                                <td class="w-25"><a class="edit" href="" data-bs-toggle="modal"
                                        data-bs-target="#editNumber">تعديل</a></td>
                            </tr>
                            <tr class="tr">
                                <th class="w-25" scope="row">البريد الألكتروني</th>
                                <td class="w-25">{{auth()->user()->email}}</td>

                                <td class="w-25"><a class="edit" href="" data-bs-toggle="modal"
                                        data-bs-target="#editEmail">تعديل</a></td>
                            </tr>
                            <tr class="tr">
                                <th class="w-25" scope="row">كلمة المرور</th>
                                <td class="w-25">**********</td>
                                <td class="w-25"><a class="edit" href="" data-bs-toggle="modal"
                                        data-bs-target="#editPassword">تعديل</a></td>
                            </tr>
                        </tbody>
                    </table>
                </div>
            </div>

        </div>
    </div>
</main>
@endsection