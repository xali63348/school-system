@extends('layouts.app')
@section('styles')

@endsection
@section('contant')
<main class="mt-5 pt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="col-12 col-sm-12 col-md-12">
                @include('shared.profile')
                @component('shared.nav&tabs',['status'=>'active'])
                @endcomponent
                <div class="col-12 col-sm-12 col-md-6 offset-md-3 py-5">
                    <div class="table-responsive">
                        <table class="table text-center">
                            <thead>
                                <tr>
                                    <th scope="col">الحالة</th>
                                    <th scope="col">متبقي</th>
                                </tr>
                            </thead>
                            <tbody>
                                <tr>
                                    @if(auth()->user()->status != 0)
                                        <th scope="row">نشط</th>
                                    @else
                                        <th scope="row">منتهي</th>
                                    @endif
                                        <td>{{auth()->user()->status}} يوم</td>
                                </tr>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</main>
@endsection