@extends('layouts.app5')
@section('styles')
<style>
    .banner {
        position: absolute;
        top: 0;
        left: 0;
        width: 100%;
        height: 175px;
        background-position: center;
        background-size: cover !important;
    }

    .img-circle {
        height: 180px;
        width: 180px;
        border-radius: 150px;
        border: 3px solid #fff;
        box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
        z-index: 1;
        margin-top: 40px;
    }

    .shadow {
        box-shadow: 0 5px 20px rgba(0, 0, 0, 0.06) !important;
    }
</style>
@endsection


@section('contant')
<main class="mt-5 pt-3">


    <div class="profile-card card rounded-lg shadow   mb-4 text-center position-relative overflow-hidden">
        <div class="banner">
            @isset($teacher->school->background)
                <img src="{{asset($teacher->school->background)}}" alt="" class="banner">
            @endisset
            @empty($teacher->school->background)
                <img src="/images/school-background.jpg" alt="" class="banner">
            @endempty
        </div>
        @isset($teacher->school->image)
            <img src="{{asset($teacher->school->image)}}" alt="" class="img-circle mx-auto mb-3">
        @endisset
        @empty($teacher->school->image)
            <img src="/images/user.png" alt="" class="img-circle mx-auto mb-3">
        @endempty

        <h3 class="mb-1">{{$teacher->school->name}}</h3>
        <div class="text-left mb-4">
            <p class="mb-2">المدير: {{$teacher->school->manager}}</p>
            <p class="mb-2">
                {{$teacher->school->user['0']->number_phone}}</p>
            <p class="mb-2"> {{$teacher->school->address['country']}}, {{$teacher->school->address['city']}},
                {{$teacher->school->address['area']}}, {{$teacher->school->address['extra']}}</p>

        </div>
    </div>
</main>
@endsection