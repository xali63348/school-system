@extends('layouts.app5')
@section('styles')
<style>
    .empty {
        text-align: center;
        margin-top: 23%;
    }

    .empty2 {
        color: #38b000;
    }
    .btn-outline-success{
        margin-bottom: 15px;
    }
</style>
@endsection
@section('contant')
<main class="mt-5 pt-3">
    <div class="container-fluid">
        <div class="row">
            <div class="table-responsive">
            @foreach($teacher->category as $category)
                @foreach($category->tables as $table)
                
                <div class="col-sm-12 col-md-12 col-lg-12 col-xl-12">
                    <span>
                        <h3 class="text-center mt-3">{{$category->classes->name}}-{{$category->name}}</h3>
                        <a href="/pdf/download/table/{{$table->id}}" class="btn btn-outline-success">{{__('messages.Download Pdf')}}</a>
                    </span>
                    
                    <table class="table table-info table-hover text-center">
                        <thead>
                            <tr>
                                <th scope="col">{{__('messages.Days')}}</th>
                                <th scope="col">{{__('messages.Lesson 1')}}</th>
                                <th scope="col">{{__('messages.Lesson 2')}}</th>
                                <th scope="col">{{__('messages.Lesson 3')}}</th>
                                <th scope="col">{{__('messages.Lesson 4')}}</th>
                                <th scope="col">{{__('messages.Lesson 5')}}</th>
                                <th scope="col">{{__('messages.Lesson 6')}}</th>
                                <th scope="col">{{__('messages.Date')}}</th>

                            </tr>
                        </thead>
                       
                        <tbody>

                            <tr>
                                <th scope="row">{{$table->days['day1']}}</th>
                                <td>
                                    {{$table->study_materials['Lesson1']}} <br>
                                    {{$table->teachers['teacher1']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson2']}} <br>
                                    {{$table->teachers['teacher2']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson3']}} <br>
                                    {{$table->teachers['teacher3']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson4']}} <br>
                                    {{$table->teachers['teacher4']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson5']}} <br>
                                    {{$table->teachers['teacher5']}}

                                </td>
                                <td>
                                    {{$table->study_materials['Lesson6']}} <br>
                                    {{$table->teachers['teacher6']}}

                                </td>
                                <td>{{$table->created_at->format('m/d/Y')}}</td>
                            </tr>

                            <tr>
                                <th scope="row">{{$table->days['day2']}}</th>
                                <td>
                                    {{$table->study_materials['Lesson7']}} <br>
                                    {{$table->teachers['teacher7']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson8']}} <br>
                                    {{$table->teachers['teacher8']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson9']}} <br>
                                    {{$table->teachers['teacher9']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson10']}} <br>
                                    {{$table->teachers['teacher10']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson11']}} <br>
                                    {{$table->teachers['teacher11']}}

                                </td>
                                <td>
                                    {{$table->study_materials['Lesson12']}} <br>
                                    {{$table->teachers['teacher12']}}

                                </td>
                                <td>{{$table->created_at->format('m/d/Y')}}</td>
                            </tr>

                            <tr>
                                <th scope="row">{{$table->days['day3']}}</th>
                                <td>
                                    {{$table->study_materials['Lesson13']}} <br>
                                    {{$table->teachers['teacher13']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson14']}} <br>
                                    {{$table->teachers['teacher14']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson15']}} <br>
                                    {{$table->teachers['teacher15']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson16']}} <br>
                                    {{$table->teachers['teacher16']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson17']}} <br>
                                    {{$table->teachers['teacher17']}}

                                </td>
                                <td>
                                    {{$table->study_materials['Lesson18']}} <br>
                                    {{$table->teachers['teacher18']}}

                                </td>
                                <td>{{$table->created_at->format('m/d/Y')}}</td>
                            </tr>

                            <tr>
                                <th scope="row">{{$table->days['day4']}}</th>
                                <td>
                                    {{$table->study_materials['Lesson19']}} <br>
                                    {{$table->teachers['teacher19']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson20']}} <br>
                                    {{$table->teachers['teacher20']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson21']}} <br>
                                    {{$table->teachers['teacher21']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson22']}} <br>
                                    {{$table->teachers['teacher22']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson23']}} <br>
                                    {{$table->teachers['teacher23']}}

                                </td>
                                <td>
                                    {{$table->study_materials['Lesson24']}} <br>
                                    {{$table->teachers['teacher24']}}

                                </td>
                                <td>{{$table->created_at->format('m/d/Y')}}</td>
                            </tr>

                            <tr>
                                <th scope="row">{{$table->days['day5']}}</th>
                                <td>
                                    {{$table->study_materials['Lesson25']}} <br>
                                    {{$table->teachers['teacher25']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson26']}} <br>
                                    {{$table->teachers['teacher26']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson27']}} <br>
                                    {{$table->teachers['teacher27']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson28']}} <br>
                                    {{$table->teachers['teacher28']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson29']}} <br>
                                    {{$table->teachers['teacher29']}}

                                </td>
                                <td>
                                    {{$table->study_materials['Lesson30']}} <br>
                                    {{$table->teachers['teacher30']}}

                                </td>
                                <td>{{$table->created_at->format('m/d/Y')}}</td>
                            </tr>
                            @isset($table->days['day6'])
                            <tr>
                                <th scope="row">{{$table->days['day6']}}</th>
                                <td>
                                    {{$table->study_materials['Lesson31']}} <br>
                                    {{$table->teachers['teacher31']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson32']}} <br>
                                    {{$table->teachers['teacher32']}}

                                </td>
                                <td>

                                    {{$table->study_materials['Lesson33']}} <br>
                                    {{$table->teachers['teacher33']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson34']}} <br>
                                    {{$table->teachers['teacher34']}}
                                </td>
                                <td>

                                    {{$table->study_materials['Lesson35']}} <br>
                                    {{$table->teachers['teacher35']}}

                                </td>
                                <td>
                                    {{$table->study_materials['Lesson36']}} <br>
                                    {{$table->teachers['teacher36']}}

                                </td>
                                <td>{{$table->created_at->format('m/d/Y')}}</td>
                            </tr>
                            @endisset
                        </tbody>
                       
                    </table>
                </div>
            </div>
            @endforeach
            @endforeach
        </div>
    </div>
</main>

@endsection