@extends('layouts.app5')
@section('styles')
<style>
    .main-row {
        background-color: white;
        box-shadow: 0 0 10px 10px rgba(0, 0, 0, .05);
        border-radius: 0.5rem;
    }

    .blog-img>img {
        width: 100%;
        height: 100%;
        transform: translateY(0px);
        object-fit: cover;
        border-radius: 0.5rem;
        box-shadow: 0 0 8px 3px rgba(0, 0, 0, .3);
        cursor: pointer;
    }

    .pop_up_close {
        position: absolute;
        top: 50px;
        right: 10px;
        font-size: 21px;
        cursor: pointer;
    }

    .blog-desc>p {
        font-style: normal;
        line-height: 2;
    }

    .full-image {
        width: 100%;
        height: 100vh;
        background: rgba(0, 0, 0, 0.9);
        position: fixed;
        top: 0;
        left: 0;
        display: none;
        align-items: center;
        justify-content: center;
        z-index: 100;
    }

    .full-image img {
        width: 50%;
        max-width: 60%;
    }

    .full-image span {
        position: absolute;
        right: 5%;
        font-size: 30px;
        color: #fff;
        cursor: pointer;
    }

    .empty {
        text-align: center;
        margin-top: 23%;
    }

    .empty2 {
        color: #4C0070;
    }
    .blog-date{
        color:#65676B;
    }
</style>

@endsection
@section('contant')
<main class="mt-5 pt-3">
    <div class="container-fluid">
        @foreach($teacher->category as $category)
        @foreach($category->notifications as $notific)
                <div class="row main-row mt-4">
                    @isset($notific->images)
                    <div class="col-sm-12 col-md-12 col-lg-4">
                        <div class="full-image" id="fullImgBox">
                            <img src="{{asset($notific->images)}}" class="img-fluid" alt="error" id="fullImg">
                            <span class="pop_up_close" onclick="closeFullImg()">&#10006</span>
                        </div>

                        <div class="blog-img">
                            <img src="{{asset($notific->images)}}" class="img-fluid" alt="error"
                                onclick="openFullImg(this.src)">
                        </div>
                    </div>

                    <div class="col-md-12 col-sm-12 col-lg-8 py-4 text-end">
                        <div class="blog-title mb-3">
                            <h3>{{$notific->title}}</h3>
                        </div>

                        <div class="blog-desc mb-2">
                            <p>
                                {{$notific->notifications}}
                            </p>
                        </div>
                        <div class="blog-date mb-2">
                            <p>{{$notific->created_at->format('m/d/Y')}}</p>
                        </div>
                        <div class="blog-date mb-2">
                            <p>{{$notific->category->classes->name}}-{{$notific->category->name}}</p>
                        </div>
                    </div>
                    @endisset
                    @empty($notific->images)
                    <div class="col-md-12 col-sm-12 col-lg-12 py-4 text-end">
                        <div class="blog-title mb-3 ">
                            <h3>{{$notific->title}}</h3>
                        </div>

                        <div class="blog-desc mb-2 text-end">
                            <p>
                                {{$notific->notifications}}
                            </p>
                        </div>
                        <div class="blog-date mb-2">
                            <p>{{$notific->created_at->format('m/d/Y')}}</p>
                        </div>
                        <div class="blog-date mb-2">
                            <p>{{$notific->category->classes->name}}-{{$notific->category->name}}</p>
                        </div>
                    </div>
                    @endempty
                </div>
               
        @endforeach
        @endforeach
      
    </div>
</main>
@endsection
@section('script')
<script src="/notifications.js"></script>
@endsection