@extends('layouts.app1')
@section('styles')
<style>

    input[type="file"] {
        display: none;
    }

    .lab {
        color: #fff;
        background-color: #4d84e2;
        position: absolute;
        height: 40px;
        width: 160px;
        justify-content: center;
        display: flex;
        border-radius: 4px;
        cursor: pointer;
    }

    .fa-images {
        margin-top: 10px;
        margin-right: 7px;
    }
    

    .mt-1 {
        padding: 5px;
        box-shadow: 0 10px 20px rgba(0, 0, 0, 0.10) !important;
    }
    .bd-callout-warning {
        border-left-color: #f0ad4e !important;
    }

    .bd-callout {
        padding: 1.25rem;
        margin-top: 1.25rem;
        margin-bottom: 1.25rem;
        border: 1px solid #e9ecef;
        border-left-width: 0.25rem;
        border-radius: 0.25rem;
    }

    .choose-photo {
        margin-top: 5px;
    }

</style>
@endsection
@section('contant')
@include('shared.navbar9')
<div class="contianer">
    <div class="col-12 col-sm-12  col-md-10 offset-md-1 col-lg-8 offset-lg-2 mt-1">
        <h3 class="text-center">اضافة استاذ</h3>
        <form class="row g-3 py-3" method='post' action="/teachers/create/{{$category->id}}"
            enctype="multipart/form-data">
            @csrf
            <div class="col-4 col-sm-4 col-md-4">
                <label for="inputEmail4" class="form-label">الاسم الاول</label>
                <input type="text" class="form-control" name="name" id="name" value="{{old('name')}}"
                    placeholder="الاسم الاول" required>
            </div>
            <div class="col-4 col-sm-4 col-md-4">
                <label for="inputPassword4" class="form-label">الاسم الوسط</label>
                <input type="text" class="form-control" name="middle_name" id="middle_name"
                    value="{{old('middle_name')}}" placeholder="الاسم الوسط" required>
            </div>
            <div class="col-4 col-sm-4 col-md-4">
                <label for="inputPassword4" class="form-label">الاسم الاخير</label>
                <input type="text" class="form-control" name="last_name" id="last_name" value="{{old('last_name')}}"
                    placeholder="الاسم الاخير" required>
            </div>
            <div class="col-6 col-sm-3 col-md-3">
                <label for="inputAddress" class="form-label"></label>
                <input type="text" class="form-control" name="country" id="country" value="{{old('country')}}"
                    placeholder="البلد">
            </div>
            <div class="col-6 col-sm-3 col-md-3">
                <label for="inputAddress" class="form-label"> </label>
                <input type="text" class="form-control" name="city" id="city" value="{{old('city')}}"
                    placeholder="المدينة">
            </div>
            <div class="col-6 col-sm-3 col-md-3">
                <label for="inputAddress" class="form-label"> </label>
                <input type="text" class="form-control" name="area" id="area" value="{{old('area')}}"
                    placeholder="المنطقة">
            </div>
            <div class="col-6 col-sm-3 col-md-3">
                <label for="inputAddress" class="form-label"> </label>
                <input type="text" class="form-control" name="extra" id="extra" value="{{old('extra')}}"
                    placeholder="اضافي">
            </div>
            <div class="col-6 col-sm-5 col-md-5">
                <label for="inputAddress2" class="form-label">رقم الهاتف</label>
                <input type="number" class="form-control" name="phone_number" id="phone_number"
                    placeholder="رقم الهاتف">
            </div>
            <div class="col-6 col-sm-5 col-md-5">
                <label for="inputAddress2" class="form-label"> رقم الكود</label>
                <input type="text" class="form-control" name="number_code" id="number_code" placeholder="اضف كود"
                    required>
            </div>
            <div class="col-4 col-sm-2 col-md-2">
                <label for="inputZip" class="form-label">التولد</label>
                <input type="number" class="form-control" name="age" placeholder="التولد" value="{{old('age')}}" id="age">
            </div>
            <div class="mb-3 my-3">
                <input class="form-control" name="logo" type="file" id="file">
                <label class="lab" for="file">
                    <i class="fas fa-images"></i>
                    <span class="choose-photo">اختر صورة</span>
                </label>
            </div>
            <div class="d-grid gap-2">
                <input type="submit" class="btn btn-outline-dark my-5" value="انشاء" />
            </div>
        </form>
    </div>
    <div class="col-12 col-sm-12  col-md-10 offset-md-1 col-lg-8 offset-lg-2">
        <div class="bd-callout bd-callout-warning">
           <code>2MB</code> يرجى ملاحظة أن الحد الأقصى لحجم الصورة المطلوب هو 
        </div>
    </div>
    <div class="col-12 col-sm-12  col-md-10 offset-md-1 col-lg-8 offset-lg-2 ">
        <form action="/teachers/add/{{$category->id}}" method="post" class="form1">
            @csrf
            <label for="inputState" class="form-label">اضف اساتذه</label>
            <select id="inputState" name="teachers" class="form-select">
                <option selected>اختر...</option>
                @foreach ($teachers as $teacher)
                <option value="{{$teacher->id}}">{{$teacher->name}}</option>
                @endforeach
            </select>
            <input type="submit" class="btn btn-outline-dark mt-2" value="انشاء" />
        </form>
    </div>
</div>
</div>
<br>
<br>

@endsection