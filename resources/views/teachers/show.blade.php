@extends('layouts.app1')
@section('styles')
<style>
	.shadow {
		box-shadow: 0 5px 20px rgba(0, 0, 0, 0.06) !important;

	}



	.banner {
		position: absolute;
		top: 0;
		left: 0;
		width: 100%;
		height: 125px;
		background-image: url("/images/teachers.jpg");
		background-position: center;
		background-size: cover;
	}

	.img-circle {
		height: 150px;
		width: 150px;
		border-radius: 150px;
		border: 3px solid #fff;
		box-shadow: 0 2px 5px rgba(0, 0, 0, 0.1);
		z-index: 1;
	}

	.categories {
		color: black;
		font-size: 19px;
		text-decoration: none;
	}

	.categories:hover {
		color: #ffc107;
	}

	.graduate {
		float: right;
	}

	.Vector {
		width: 50px;
		margin-left: 15px;
	}

	.empty {
		text-align: center;
		margin-top: 15%;
	}

	.empty2 {
		color: #ffc107;
		text-decoration: none;
	}

	.empty2:hover {
		color: #FABB51;
		text-decoration: none;
	}

	input[type=text] {
		float: left;
		padding: 5px;
		font-size: 17px;
		border: 1px solid #ced4da;
		border-radius: 0.25rem;
	}

	button {
		float: left;
		padding: 6px 10px;
		margin-right: 14px;
		background: #ddd;
		font-size: 17px;
		cursor: pointer;
	}
</style>
@endsection
@section('contant')

@include('shared.navbar4')
<div class="container-fluid">
	<div class="row">
		<span>
			<a href="/teachers/create/{{$category->id}}" class="categories">
				<span class="graduate">انشاء استاذ<i class="fas fa-chalkboard-teacher"></i></span>
			</a>
		</span>
		@isset($teachers['0'])
		@foreach($teachers as $teacher)
		<div class="col-12 col-sm-6 col-md-4 col-lg-4 col-xl-3 col-xxl-3 my-3">
			<div
				class="profile-card card rounded-lg shadow p-4 p-xl-5 mb-4 text-center position-relative overflow-hidden">
				<div class="banner"></div>
				@isset($teacher->images)
				<img src="{{asset($teacher->images)}}" alt="" class="img-circle mx-auto mb-3">
				@endisset
				@empty($teacher->images)
				<img src="/images/user.png" alt="" class="img-circle mx-auto mb-3">
				@endempty

				<h3 class="mb-4">{{$teacher->name}}</h3>
				<div class="text-left mb-4">
					<p class="mb-2"><i class="fas fa-chalkboard-teacher"></i>{{$teacher->age}}</p>
					<p class="mb-2"><i class="fa fa-phone mr-2"></i>{{$teacher->phone_number}}</p>
					<p class="mb-2"><i class="fas fa-code"></i>{{$teacher->number_code}}</p>
					<p class="mb-2"><i class="fa fa-map-marker-alt mr-2"></i> {{$teacher->address['country']}},
						{{$teacher->address['city']}},{{$teacher->address['area']}},{{$teacher->address['extra']}}</p>

					<a href=""
						class="mx-2 btn btn-danger my-2" data-bs-toggle="modal" data-bs-target="#delete{{$teacher->id}}">حذف</a>
					<a href="/teachers/edit/{{$teacher->id}}/{{$category->id}}"
						class="mx-2 btn btn-outline-primary">تعديل</a>

						<!-- delete teacher -->
						<div class="modal fade" id="delete{{$teacher->id}}" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
							aria-labelledby="staticBackdropLabel" aria-hidden="true">
							<div class="modal-dialog">
								<div class="modal-content">
									<div class="modal-header">
										<h5 class="modal-title" id="staticBackdropLabel">حذف</h5>
										<button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
									</div>
									<div class="modal-body">
										يرجى اختيار طريقة الحذف
									</div>
									<div class="modal-footer">
										<a href="/teachers/delete/{{$teacher->id}}/{{$category->id}}" class="btn btn-outline-danger">حذف من الشعبه</a>
										<a href="/teachers/permanently/delete/{{$teacher->id}}/{{$category->id}}" class="btn btn-outline-danger">حذف بشكل نهائي</a>
									</div>
								</div>
							</div>
						</div>
						{{-- end --}}
				</div>
			</div>
		</div>
		@endforeach

		@isset($search)
		{{$teachers->appends(['search' => $search])->links()}}
		@endisset
		@empty($search)
		{{$teachers->links()}}
		@endempty

		@endisset
		@empty($category->teachers['0'])
		<h2 class="empty">أضف <a class="empty2" href="/teachers/create/{{$category->id}}">استاذ</a></h2>
		@endempty
	</div>
</div>

@endsection