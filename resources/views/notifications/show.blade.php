@extends('layouts.app1')
@section('styles')
<style>
    .main-row {
        margin-top: 8%;
        background-color: white;
        box-shadow: 0 0 10px 10px rgba(0, 0, 0, .05);
        border-radius: 0.5rem;
    }

    .blog-img>img {
        width: 100%;
        height: 100%;
        transform: translateY(0px);
        object-fit: cover;
        border-radius: 0.5rem;
        box-shadow: 0 0 8px 3px rgba(0, 0, 0, .3);
        cursor: pointer;
    }

    .pop_up_close {
        position: absolute;
        top: 15px;
        right: 15px;
        font-size: 21px;
        cursor: pointer;
    }

    .blog-desc>p {
        font-style: normal;
        line-height: 2;
    }

    .full-image {
        width: 100%;
        height: 100vh;
        background: rgba(0, 0, 0, 0.9);
        position: fixed;
        top: 0;
        left: 0;
        display: none;
        align-items: center;
        justify-content: center;
        z-index: 100;
    }

    .full-image img {
        width: 50%;
        max-width: 60%;
    }

    .full-image span {
        position: absolute;
        top: 3%;
        right: 5%;
        font-size: 30px;
        color: #fff;
        cursor: pointer;
    }

    .lab {
        color: #fff;
        background-color: #4d84e2;
        position: absolute;
        height: 40px;
        width: 160px;
        justify-content: center;
        display: flex;
        border-radius: 4px;
        cursor: pointer;
    }

    input[type="file"] {
        display: none;
    }

    .fa-images {
        margin-top: 10px;
        margin-right: 7px;
    }

    .choose-photo {
        margin-top: 7px;
    }

    .far .fa-trash-alt {
        font-size: 32px;
    }

    .blog-edit {
        float: left !important;
    }

    .categories {
        color: black;
        font-size: 19px;
        text-decoration: none;
    }

    .categories:hover {
        color: #4C0070;
    }

    .graduate {
        float: right;
    }

    .empty {
        text-align: center;
        margin-top: 15%;
    }

    .empty2 {
        color: #4C0070;
        text-decoration: none;
    }

    .empty2:hover {
        color: #5a189a;
        text-decoration: none;
    }
</style>
@endsection
@section('contant')
@include('shared.navbar11')
{{-- create notification --}}

<div class="modal fade" id="exampleModal" tabindex="-1" aria-labelledby="exampleModalLabel" aria-hidden="true">
    <div class="modal-dialog">
        <div class="modal-content">
            <div class="modal-header">
                <h5 class="modal-title" id="exampleModalLabel">انشاء تبليغ</h5>
                <button type="button" class="btn-close" data-bs-dismiss="modal" aria-label="Close"></button>
            </div>
            <div class="modal-body">
                <form action="/notifications/create/{{$category->id}}" method="post" enctype="multipart/form-data">
                    @csrf
                    <div class="mb-3">
                        <label for="recipient-name" class="col-form-label">العنوان:</label>
                        <input type="text" class="form-control" name="title" id="title" value="{{old('title')}}"
                            placeholder="العنوان" required>
                    </div>
                    <div class="mb-3">
                        <label for="message-text" class="col-form-label">اكتب التبليغ:</label>
                        <textarea class="form-control" name="text" id="text" value="{{old('text')}}"
                            placeholder="اكتب ..."></textarea>
                    </div>
                    <div class="mb-3 my-3">
                        <input class="form-control" name="logo" type="file" id="file">
                        <label class="lab" for="file">
                            <i class="fas fa-images"></i>
                            <span class="choose-photo">اختر صورة</span>
                        </label>
                    </div>
            </div>
            <br>
            <br>
            <div class="modal-footer">
                <input type="submit" class="btn btn-dark" value="انشاء" />
            </div>
            </form>
        </div>
    </div>
</div>
{{-- end --}}
<div class="container-fluid">
    <div class="row">
        <span>
            <a href="" data-bs-toggle="modal" data-bs-target="#exampleModal" class="categories">
                <span class="graduate">انشاء تبليغ <i class="far fa-comment"></i></span>
            </a>
        </span>

    </div>
</div>

@isset($category->notifications)
@foreach($category->notifications as $notific)
<div class="container">
    <div class="row main-row">
        @isset($notific->images)
        <div class="col-sm-12 col-md-12 col-lg-4">
            <div class="full-image" id="fullImgBox">
                <img src="{{asset($notific->images)}}" class="img-fluid" alt="error" id="fullImg">
                <span class="pop_up_close" onclick="closeFullImg()">&#10006</span>
            </div>

            <div class="blog-img">
                <img src="{{asset($notific->images)}}" class="img-fluid" alt="error" onclick="openFullImg(this.src)">
            </div>
        </div>

        <div class="col-md-12 col-sm-12 col-lg-8 py-4 text-end">
            <div class="blog-title mb-3">
                <h3>{{$notific->title}}</h3>
            </div>

            <div class="blog-desc mb-2">
                <p>
                    {{$notific->notifications}}
                </p>
            </div>
            <div class="blog-date mb-2">
                <p>{{$notific->created_at}}</p>
            </div>
            <div class="blog-edit">
                <a href="/notifications/delete/{{$notific->id}}" class="btn btn-danger"><i
                        class="far fa-trash-alt"></i></a>
            </div>
        </div>
        @endisset
        @empty($notific->images)
        <div class="col-md-12 col-sm-12 col-lg-12 py-4 text-end">
            <div class="blog-title mb-3 ">
                <h3>{{$notific->title}}</h3>
            </div>

            <div class="blog-desc mb-2 text-end">
                <p>
                    {{$notific->notifications}}
                </p>
            </div>
            <div class="blog-date mb-2">
                <p>{{$notific->created_at}}</p>
            </div>
            <div class="blog-edit ">
                <a href="/notifications/delete/{{$notific->id}}" class="btn btn-danger"><i
                        class="far fa-trash-alt"></i></a>
            </div>
        </div>
        @endempty
    </div>
    @endforeach
    @endisset
    @empty($category->notifications['0'])
    <h2 class="empty">أضف <a class="empty2" href="" data-bs-toggle="modal"
            data-bs-target="#exampleModal">تبليغ</a></h2>
    @endempty
    @endsection

    @section('script')
    <script src="/notifications.js"></script>
    @endsection