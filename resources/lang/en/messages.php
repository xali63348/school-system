<?php

return [
    'Profile'=>'Profile',
    'INTERFACE'=>'INTERFACE',
    'Absences'=>'Absences',
    'notifications'=>'notifications',
    'TABLES'=>'TABLES',
    'Table'=>'Table',
    'Exam Schedule'=>'Exam Schedule',
    'Exam Results'=>'Exam Results',
    'Serach'=>'Serach',
    'Lesson'=>'Lesson',
    'Lesson 1'=>'Lesson 1',
    'Lesson 2'=>'Lesson 2',
    'Lesson 3'=>'Lesson 3',
    'Lesson 4'=>'Lesson 4',
    'Lesson 5'=>'Lesson 5',
    'Lesson 6'=>'Lesson 6',
    'Date'=>'Date',
    'Days'=>'Days',
    'Download Pdf'=>'Download Pdf',
    'Month'=>'Month',
    'Name'=>'Name',
    'All Day'=>'All Day',
    'Day'=>'Day',
]


?>