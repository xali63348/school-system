<?php

return[
    'Profile'=>'الملف الشخصي',
    'INTERFACE'=>'واجهة المستخدم',
    'Absences'=>'الغيابات',
    'notifications'=>'تبليغات',
    'TABLES'=>'الجداول',
    'Table'=>'جدول',
    'Exam Schedule'=>'جدول الامتحانات',
    'Exam Results'=>'نتائج الامتحانات',
    'Serach'=>'البحث',
    'Lesson'=>'درس',
    'Lesson 1'=>'درس 1',
    'Lesson 2'=>'درس 2',
    'Lesson 3'=>'درس 3',
    'Lesson 4'=>'درس 4',
    'Lesson 5'=>'درس 5',
    'Lesson 6'=>'درس 6',
    'Date'=>'تاريخ',
    'Days'=>'الايام',
    'Download Pdf'=>'تنزيل الملف',
    'Month'=>'الشهر',
    'Name'=>'الاسم',
    'All Day'=>'يوم كامل',
    'Day'=>'يوم',
]
?>