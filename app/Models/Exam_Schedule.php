<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam_Schedule extends Model
{
    use HasFactory;

    protected $guarded = [
        
    ];

    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
    public function school(){
        return $this->belongsTo(School::class,'school_id');
    }
    protected $casts = [
        'days'=>'array',
        'materials'=>'array',
        'date'=>'array',
    ];
}
