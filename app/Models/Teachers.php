<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Teachers extends Model
{
    use HasFactory;

    protected $guarded = [
        
    ];
    
    public function category(){
        return $this->belongsToMany(Category::class,'teachers_categories','teachers_id','category_id');
    }
    public function school(){
        return $this->belongsTo(School::class,'school_id');
    }
    protected $casts = [
        'address'=>'array'
    ];
    
}
