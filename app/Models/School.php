<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class School extends Model
{

    use HasFactory;

    protected $guarded = [
        
    ];
    // protected $with= [
    //     'classes',
    // ];
    
    public function user(){
        return $this->hasMany(User::class,'school_id');
    }
    
    public function classes(){
        return $this->hasMany(Classes::class,'school_id');
    }
    public function category(){
        return $this->hasMany(Category::class,'school_id');
    }
    public function teachers(){
        return $this->hasMany(Teachers::class,'school_id');
    }
    public function tables(){
        return $this->hasMany(Table::class,'school_id');
    }
    public function absences(){
        return $this->hasMany(Absence::class,'school_id');
    }
    public function students(){
        return $this->hasMany(Student::class,'school_id');
    }
    public function notifications(){
        return $this->hasMany(Notifications::class,'school_id');
    }
    public function ExamSchedules(){
        return $this->hasMany(Exam_Schedule::class,'school_id');
    }
    public function ExamResults(){
        return $this->hasMany(Exam_Results::class,'school_id');
    }
    protected $casts = [
        'address'=>'array'
    ];
}
