<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Exam_Results extends Model
{
    use HasFactory;

     protected $guarded = [

     ];
     public function category(){
        return $this->belongsTo(Category::class,'category_id');
     }
    public function school(){
        return $this->belongsTo(School::class,'school_id');
     }
    public function students(){
        return $this->belongsTo(Student::class,'student_id');
    }
   
    protected $casts = [
        'grades'=>'array',
        'materials'=>'array',

    ];

}
