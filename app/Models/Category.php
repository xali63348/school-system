<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    use HasFactory;
    
    protected $guarded = [
        
    ];
    // protected $with= [
    //     'students',
    // ];

    public function classes(){
        return $this->belongsTo(Classes::class,'classes_id');
    }
    public function students(){
        return $this->hasMany(Student::class,'category_id')->orderBy('id','desc');
    }
    public function notifications(){
        return $this->hasMany(Notifications::class,'category_id')->orderBy('id','desc');
    }
    public function absences(){
        return $this->hasMany(Absence::class,'category_id')->orderBy('id','desc');
    }
    public function tables(){
        return $this->hasMany(Table::class,'category_id');
    }
    public function ExamSchedules(){
        return $this->hasMany(Exam_Schedule::class,'category_id')->orderBy('id','desc');
    }
    public function ExamResults(){
        return $this->hasMany(Exam_Results::class,'category_id')->orderBy('id','desc');
    }
    public function teachers(){
        return $this->belongsToMany(Teachers::class,'teachers_categories','category_id','teachers_id')->orderBy('id','desc');
    }
    public function school(){
        return $this->belongsTo(School::class,'school_id');
    }
    
}
