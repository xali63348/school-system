<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absence extends Model
{
    use HasFactory;

    protected $guarded = [
        
    ];
    // protected $with= [
    //     'create',
    // ];
    public function students(){
        return $this->belongsTo(Student::class,'student_id');
    }
    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
    public function school(){
        return $this->belongsTo(School::class,'school_id');
    }
    protected $casts = [
        'days_of_absence'=>'array'
    ];
}
