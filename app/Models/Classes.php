<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Classes extends Model
{
    use HasFactory;
    
    protected $guarded = [
        
    ];
    public function school(){
        return $this->belongsTo(School::class,'school_id');
    }
    public function category(){
        return $this->hasMany(Category::class,'classes_id');
    }
    public function teachers(){
        return $this->belongsToMany(Teachers::class,'teachers_classes','classes_id','teachers_id');
    }
}
