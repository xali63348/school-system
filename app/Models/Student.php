<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Student extends Model
{
    use HasFactory;

    protected $guarded = [
        
    ];
    public function category(){
        return $this->belongsTo(Category::class,'category_id');
    }
    public function absences(){
        return $this->hasMany(Absence::class,'student_id')->orderBy('id','desc');
    }
    public function ExamResults(){
        return $this->hasMany(Exam_Results::class,'student_id');
    }
    public function school(){
        return $this->belongsTo(School::class,'school_id');
    }
    protected $casts = [
        'address'=>'array'
    ];
    
    
}
