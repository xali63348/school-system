<?php

namespace App\Http\Controllers;

use App\Models\Notifications;
use App\Models\Category;
use Brian2694\Toastr\Facades\Toastr;

use App\Http\Requests\StoreNotificationsRequest;
use App\Http\Requests\UpdateNotificationsRequest;

class NotificationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreNotificationsRequest  $request
     * @return \Illuminate\Http\Response
     */

    
    public function store(StoreNotificationsRequest $request,Category $category)
    {
        $validateData=request()->validate([
            'title'=>'required|min:2',
            'logo'=>'mimes:png,jpg,jpeg,bmp|max:2000'
        ]);
        $path=null;
        if($request->hasFile('logo')){
            $path='/storage/'.$request->file('logo')->store('logos',['disk'=>'public']);
        }
        $school_id=auth()->user()->school->id;
        $new_notification= Notifications::create([
            'notifications'=>$request->text,
            'title'=>$request->title,
            'images'=>$path,
            'school_id'=>$school_id,
            'category_id'=>$category->id,
        ]);
        Toastr::success('تم انشاء التبليغ بنجاح');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Notifications  $notifications
     * @return \Illuminate\Http\Response
     */
    public function show(Notifications $notifications ,Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            return view('notifications.show',compact('category'));
        }
        return view('shared.error');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Notifications  $notifications
     * @return \Illuminate\Http\Response
     */
    public function edit(Notifications $notifications)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateNotificationsRequest  $request
     * @param  \App\Models\Notifications  $notifications
     * @return \Illuminate\Http\Response
     */
    public function update(UpdateNotificationsRequest $request, Notifications $notifications)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Notifications  $notifications
     * @return \Illuminate\Http\Response
     */
    public function destroy(Notifications $notific)
    {
        if($notific->school_id == auth()->user()->school->id){

            $notific->delete();
            Toastr::success('تم الحذف بنجاح');
            return redirect()->back();
        }
        return view('shared.error');
    }
}
