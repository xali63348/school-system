<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use PDF;
use App\Models\Table;
use App\Models\Exam_Schedule;

class PDFController extends Controller
{
   
    public function generatePDF(Table $tables){
        $pdf = PDF::loadView('pdf.table', compact('tables'));
        return $pdf->download('table.pdf');
        // return view('pdf.table', compact('tables'));
    }
    public function generatePDF2(Exam_Schedule $examschedule){
        $pdf = PDF::loadView('pdf.exam-schedule', compact('examschedule'));
        return $pdf->download('exam-schedule.pdf');
        // return view('pdf.exam-schedule', compact('examschedule'));
    }
}
