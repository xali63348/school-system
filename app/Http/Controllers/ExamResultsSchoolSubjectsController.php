<?php

namespace App\Http\Controllers;

use App\Models\exam__results___school_subjects;
use App\Http\Requests\Storeexam__results___school_subjectsRequest;
use App\Http\Requests\Updateexam__results___school_subjectsRequest;

class ExamResultsSchoolSubjectsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\Storeexam__results___school_subjectsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Storeexam__results___school_subjectsRequest $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\exam__results___school_subjects  $exam__results___school_subjects
     * @return \Illuminate\Http\Response
     */
    public function show(exam__results___school_subjects $exam__results___school_subjects)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\exam__results___school_subjects  $exam__results___school_subjects
     * @return \Illuminate\Http\Response
     */
    public function edit(exam__results___school_subjects $exam__results___school_subjects)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\Updateexam__results___school_subjectsRequest  $request
     * @param  \App\Models\exam__results___school_subjects  $exam__results___school_subjects
     * @return \Illuminate\Http\Response
     */
    public function update(Updateexam__results___school_subjectsRequest $request, exam__results___school_subjects $exam__results___school_subjects)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\exam__results___school_subjects  $exam__results___school_subjects
     * @return \Illuminate\Http\Response
     */
    public function destroy(exam__results___school_subjects $exam__results___school_subjects)
    {
        //
    }
}
