<?php

namespace App\Http\Controllers;

use App\Models\Table;
use App\Models\Category;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use App\Http\Controllers\PDF;

class TableController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
           $classes=$category->classes;
           if(isset($category->tables['0'])){
               return redirect('tables/show/'.$category->id);
           }
            return view('tables.create',compact('category','classes'));
        }
        return view('shared.error');
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $validateDate=request()->validate([
            'day1'=>'required|min:5',
            'day2'=>'required|min:5',
            'day3'=>'required|min:5',
            'day4'=>'required|min:5',
            'day5'=>'required|min:5',
 
             'Lesson1'=>'required',
             
             'Lesson2'=>'required',
            
             'Lesson3'=>'required',
             
             'Lesson4'=>'required',
             
             'Lesson5'=>'required',
             
 
             'Lesson7'=>'required',
             
             'Lesson8'=>'required',
             
             'Lesson9'=>'required',
             
             'Lesson10'=>'required',
            
             'Lesson11'=>'required',
             
 
             'Lesson13'=>'required',
             
             'Lesson14'=>'required',
             
             'Lesson15'=>'required',
            
             'Lesson16'=>'required',
             
             'Lesson17'=>'required',
             
 
             'Lesson19'=>'required',
            
             'Lesson20'=>'required',
            
             'Lesson21'=>'required',
             
             'Lesson22'=>'required',
            
             'Lesson23'=>'required',
            
 
             'Lesson25'=>'required',
            
             'Lesson26'=>'required',
             
             'Lesson27'=>'required',
            
             'Lesson28'=>'required',
           
             'Lesson29'=>'required',
            
 
        ]);
       
        $new_days=[
            'day1'=>request()->day1,
            'day2'=>request()->day2,
            'day3'=>request()->day3,
            'day4'=>request()->day4,
            'day5'=>request()->day5,
            'day6'=>request()->day6,
        ];
        $new_study_materials=[
            'Lesson1'=>request()->Lesson1,
            'Lesson2'=>request()->Lesson2,
            'Lesson3'=>request()->Lesson3,
            'Lesson4'=>request()->Lesson4,
            'Lesson5'=>request()->Lesson5,
            'Lesson6'=>request()->Lesson6,

            'Lesson7'=>request()->Lesson7,
            'Lesson8'=>request()->Lesson8,
            'Lesson9'=>request()->Lesson9,
            'Lesson10'=>request()->Lesson10,
            'Lesson11'=>request()->Lesson11,
            'Lesson12'=>request()->Lesson12,

            'Lesson13'=>request()->Lesson13,
            'Lesson14'=>request()->Lesson14,
            'Lesson15'=>request()->Lesson15,
            'Lesson16'=>request()->Lesson16,
            'Lesson17'=>request()->Lesson17,
            'Lesson18'=>request()->Lesson18,

            'Lesson19'=>request()->Lesson19,
            'Lesson20'=>request()->Lesson20,
            'Lesson21'=>request()->Lesson21,
            'Lesson22'=>request()->Lesson22,
            'Lesson23'=>request()->Lesson23,
            'Lesson24'=>request()->Lesson24,

            'Lesson25'=>request()->Lesson25,
            'Lesson26'=>request()->Lesson26,
            'Lesson27'=>request()->Lesson27,
            'Lesson28'=>request()->Lesson28,
            'Lesson29'=>request()->Lesson29,
            'Lesson30'=>request()->Lesson30,

            'Lesson31'=>request()->Lesson31,
            'Lesson32'=>request()->Lesson32,
            'Lesson33'=>request()->Lesson33,
            'Lesson34'=>request()->Lesson34,
            'Lesson35'=>request()->Lesson35,
            'Lesson36'=>request()->Lesson36,
        ];
        $new_teachers=[
            'teacher1'=>request()->teacher1,
            'teacher2'=>request()->teacher2,
            'teacher3'=>request()->teacher3,
            'teacher4'=>request()->teacher4,
            'teacher5'=>request()->teacher5,
            'teacher6'=>request()->teacher6,

            'teacher7'=>request()->teacher7,
            'teacher8'=>request()->teacher8,
            'teacher9'=>request()->teacher9,
            'teacher10'=>request()->teacher10,
            'teacher11'=>request()->teacher11,
            'teacher12'=>request()->teacher12,

            'teacher13'=>request()->teacher13,
            'teacher14'=>request()->teacher14,
            'teacher15'=>request()->teacher15,
            'teacher16'=>request()->teacher16,
            'teacher17'=>request()->teacher17,
            'teacher18'=>request()->teacher18,

            'teacher19'=>request()->teacher19,
            'teacher20'=>request()->teacher20,
            'teacher21'=>request()->teacher21,
            'teacher22'=>request()->teacher22,
            'teacher23'=>request()->teacher23,
            'teacher24'=>request()->teacher24,

            'teacher25'=>request()->teacher25,
            'teacher26'=>request()->teacher26,
            'teacher27'=>request()->teacher27,
            'teacher28'=>request()->teacher28,
            'teacher29'=>request()->teacher29,
            'teacher30'=>request()->teacher30,

            'teacher31'=>request()->teacher31,
            'teacher32'=>request()->teacher32,
            'teacher33'=>request()->teacher33,
            'teacher34'=>request()->teacher34,
            'teacher35'=>request()->teacher35,
            'teacher36'=>request()->teacher36,
        ];
        
        $school_id=auth()->user()->school->id;
        $new_table=Table::create([
            'days'=>$new_days,
            'teachers'=>$new_teachers,
            'study_materials'=>$new_study_materials,
            'category_id'=>$category->id,
            'school_id'=>$school_id,
        ]);
        Toastr::success('تم اضافة الجدول بنجاح');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            $tables=$category->tables;
            return view('tables.show',compact('category','tables'));
        }
        return view('shared.error');
    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function edit(Table $table)
    {
        if($table->school_id== auth()->user()->school->id){
            $category=$table->category;
            return view('tables.edit',compact('category','table'));
        }
        return view('shared.error');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Table $table)
    {
        $validateDate=request()->validate([
            'day1'=>'required|min:5',
            'day2'=>'required|min:5',
            'day3'=>'required|min:5',
            'day4'=>'required|min:5',
            'day5'=>'required|min:5',
 
             'Lesson1'=>'required',
             
             'Lesson2'=>'required',
            
             'Lesson3'=>'required',
             
             'Lesson4'=>'required',
             
             'Lesson5'=>'required',
             
 
             'Lesson7'=>'required',
             
             'Lesson8'=>'required',
             
             'Lesson9'=>'required',
             
             'Lesson10'=>'required',
            
             'Lesson11'=>'required',
             
 
             'Lesson13'=>'required',
             
             'Lesson14'=>'required',
             
             'Lesson15'=>'required',
            
             'Lesson16'=>'required',
             
             'Lesson17'=>'required',
             
 
             'Lesson19'=>'required',
            
             'Lesson20'=>'required',
            
             'Lesson21'=>'required',
             
             'Lesson22'=>'required',
            
             'Lesson23'=>'required',
            
 
             'Lesson25'=>'required',
            
             'Lesson26'=>'required',
             
             'Lesson27'=>'required',
            
             'Lesson28'=>'required',
           
             'Lesson29'=>'required',
            
 
        ]);
        
         $edit_days=[
             'day1'=>request()->day1,
             'day2'=>request()->day2,
             'day3'=>request()->day3,
             'day4'=>request()->day4,
             'day5'=>request()->day5,
             'day6'=>request()->day6,
         ];
         $edit_study_materials=[
             'Lesson1'=>request()->Lesson1,
             'Lesson2'=>request()->Lesson2,
             'Lesson3'=>request()->Lesson3,
             'Lesson4'=>request()->Lesson4,
             'Lesson5'=>request()->Lesson5,
             'Lesson6'=>request()->Lesson6,
 
             'Lesson7'=>request()->Lesson7,
             'Lesson8'=>request()->Lesson8,
             'Lesson9'=>request()->Lesson9,
             'Lesson10'=>request()->Lesson10,
             'Lesson11'=>request()->Lesson11,
             'Lesson12'=>request()->Lesson12,
 
             'Lesson13'=>request()->Lesson13,
             'Lesson14'=>request()->Lesson14,
             'Lesson15'=>request()->Lesson15,
             'Lesson16'=>request()->Lesson16,
             'Lesson17'=>request()->Lesson17,
             'Lesson18'=>request()->Lesson18,
 
             'Lesson19'=>request()->Lesson19,
             'Lesson20'=>request()->Lesson20,
             'Lesson21'=>request()->Lesson21,
             'Lesson22'=>request()->Lesson22,
             'Lesson23'=>request()->Lesson23,
             'Lesson24'=>request()->Lesson24,
 
             'Lesson25'=>request()->Lesson25,
             'Lesson26'=>request()->Lesson26,
             'Lesson27'=>request()->Lesson27,
             'Lesson28'=>request()->Lesson28,
             'Lesson29'=>request()->Lesson29,
             'Lesson30'=>request()->Lesson30,
 
             'Lesson31'=>request()->Lesson31,
             'Lesson32'=>request()->Lesson32,
             'Lesson33'=>request()->Lesson33,
             'Lesson34'=>request()->Lesson34,
             'Lesson35'=>request()->Lesson35,
             'Lesson36'=>request()->Lesson36,
         ];
         $edit_teachers=[
             'teacher1'=>request()->teacher1,
             'teacher2'=>request()->teacher2,
             'teacher3'=>request()->teacher3,
             'teacher4'=>request()->teacher4,
             'teacher5'=>request()->teacher5,
             'teacher6'=>request()->teacher6,
 
             'teacher7'=>request()->teacher7,
             'teacher8'=>request()->teacher8,
             'teacher9'=>request()->teacher9,
             'teacher10'=>request()->teacher10,
             'teacher11'=>request()->teacher11,
             'teacher12'=>request()->teacher12,
 
             'teacher13'=>request()->teacher13,
             'teacher14'=>request()->teacher14,
             'teacher15'=>request()->teacher15,
             'teacher16'=>request()->teacher16,
             'teacher17'=>request()->teacher17,
             'teacher18'=>request()->teacher18,
 
             'teacher19'=>request()->teacher19,
             'teacher20'=>request()->teacher20,
             'teacher21'=>request()->teacher21,
             'teacher22'=>request()->teacher22,
             'teacher23'=>request()->teacher23,
             'teacher24'=>request()->teacher24,
 
             'teacher25'=>request()->teacher25,
             'teacher26'=>request()->teacher26,
             'teacher27'=>request()->teacher27,
             'teacher28'=>request()->teacher28,
             'teacher29'=>request()->teacher29,
             'teacher30'=>request()->teacher30,
 
             'teacher31'=>request()->teacher31,
             'teacher32'=>request()->teacher32,
             'teacher33'=>request()->teacher33,
             'teacher34'=>request()->teacher34,
             'teacher35'=>request()->teacher35,
             'teacher36'=>request()->teacher36,
         ];
         $edit_table=Table::find($table->id)->update([
             'days'=>$edit_days,
             'teachers'=>$edit_teachers,
             'study_materials'=>$edit_study_materials,
         ]);
        Toastr::success('تم تعديل الجدول بنجاح');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Table  $table
     * @return \Illuminate\Http\Response
     */
    public function destroy(Table $table)
    {
        if($table->school_id == auth()->user()->school->id){
            $table->delete();
            Toastr::success('تم حذف الجدول بنجاح');
            return redirect()->back();
        }
        return view('shared.error');
    }
}
