<?php

namespace App\Http\Controllers;

use App\Models\Student;
use App\Models\Absence;
use App\Models\Category;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class StudentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            return view('student.create',compact('category'));
        }
        return view('shared.error');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request, Category $category)
    {
        $validateData=request()->validate([
            'name'=>'required|min:2',
            'middle_name'=>'required|min:2',
            'last_name'=>'required|min:2',
            'country'=>'required|min:2',
            'city'=>'required|min:2',
            'area'=>'required|min:2',
            'number_phone'=>'required|min:8|unique:students',
            'number_code'=>'required|min:6|unique:students',
            'age'=>'required|min:1',
            'logo'=>'mimes:png,jpg,jpeg,bmp|max:2000'
        ]);
        $path=null;
        if($request->hasFile('logo')){
            $path='/storage/'.$request->file('logo')->store('logos',['disk'=>'public']);
        }
        $address_student=[
            'country'=>request()->country,
            'city'=>request()->city,
            'area'=>request()->area,
            'extra'=>request()->extra,
        ];
        $school_id=auth()->user()->school->id;
        $newStudent=Student::create([
            'name'=>$request->name .' '.$request->middle_name.' '.$request->last_name,
            'number_phone'=>$request->number_phone,
            'number_code'=>$request->number_code,
            'age'=>$request->age,
            'address'=>$address_student,
            'images'=>$path,
            'category_id'=>$category->id,
            'school_id'=>$school_id,
        ]);
        Toastr::success('تم اضافة الطالب بنجاح');
        return redirect()->back();
       
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        
        if($category->school_id == auth()->user()->school->id){
            $students= $category->students()->paginate(8);
            return view('student.show',compact('category','students'));
        }
        return view('shared.error');
        
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function edit(Student $student)
    {
        if($student->school_id == auth()->user()->school->id){
            return view('student.edit',compact('student'));
        }
        return view('shared.error');
    }
    
    public function search(Request $request ,Category $category){
        if($category->school_id == auth()->user()->school->id){
            $students=$category->students()->where('name','like','%'.$request->search.'%')
            ->paginate(8);
            $search=$request->search;
            return view('student.show',compact('students','category','search'));
        }
        return view('shared.error');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Student $student)
    {
        $validateData=request()->validate([
            'name'=>'required|min:2',
            'country'=>'required|min:2',
            'city'=>'required|min:2',
            'area'=>'required|min:2',
            'number_phone'=>'min:8|unique:students,number_phone,'.$student->id,
            'number_code'=>'min:6|unique:students,number_code,'.$student->id,
            'age'=>'required|min:1',
            'logo'=>'mimes:png,jpg,jpeg,bmp|max:2000'
        ]);
        
        $path=$student->images;
        if($request->hasFile('logo')){
            $path='/storage/'.$request->file('logo')->store('logos',['disk'=>'public']);
        }
        $address_student=[
            'country'=>request()->country,
            'city'=>request()->city,
            'area'=>request()->area,
            'extra'=>request()->extra,
        ];
        $editStudent=Student::find($student->id)->update([
            'name'=>$request->name,
            'number_phone'=>$request->number_phone,
            'number_code'=>$request->number_code,
            'age'=>$request->age,
            'address'=>$address_student,
            'images'=>$path,
        ]);
        Toastr::success('تم تعديل على الطالب بنجاح');
        return redirect()->back();
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Student  $student
     * @return \Illuminate\Http\Response
     */
    public function destroy(Student $student)
    {
        
        if($student->school_id == auth()->user()->school->id){
            $absences=Absence::where('student_id','=',$student->id)->delete();
            $student->delete();
            Toastr::success('تم الحذف بنجاح');
            return redirect()->back();
        }
        return view('shared.error');
    }
}
