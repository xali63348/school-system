<?php

namespace App\Http\Controllers;

use App\Models\Exam_Results;
use App\Models\Category;
use App\Models\Student;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Requests\StoreExam_ResultsRequest;
use App\Http\Requests\UpdateExam_ResultsRequest;
use Illuminate\Http\Request;

class ExamResultsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            $students=$category->students()->paginate(7);
            return view('ExamResults.create',compact('category','students'));
        }
        return view('shared.error');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreExam_ResultsRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request ,Student $student)
    {
        $validateDate=request()->validate([
            'lesson1'=>'required',
            'lesson2'=>'required',
            'lesson3'=>'required',
            'lesson4'=>'required',
            'lesson5'=>'required',
            'lesson6'=>'required',
            'result1'=>'required',
            'result2'=>'required',
            'result3'=>'required',
            'result4'=>'required',
            'result5'=>'required',
            'result6'=>'required',
            'month'=>'required',
        ]);
        
        foreach($student->ExamResults as $ExamResult){
            if($request->month == $ExamResult->month){ 
                Toastr::Warning('هذا الطالب حاصل على درجة هذا الشهر');
                return redirect()->back();
            }
        }
        $new_materials=[
            'lesson1'=>request()->lesson1,
            'lesson2'=>request()->lesson2,
            'lesson3'=>request()->lesson3,
            'lesson4'=>request()->lesson4,
            'lesson5'=>request()->lesson5,
            'lesson6'=>request()->lesson6,
            'lesson7'=>request()->lesson7,
            'lesson8'=>request()->lesson8,
            'lesson9'=>request()->lesson9,
            'lesson10'=>request()->lesson10,
            'lesson11'=>request()->lesson11,
        ];
        $new_grades=[
            'result1'=>request()->result1,
            'result2'=>request()->result2,
            'result3'=>request()->result3,
            'result4'=>request()->result4,
            'result5'=>request()->result5,
            'result6'=>request()->result6,
            'result7'=>request()->result7,
            'result8'=>request()->result8,
            'result9'=>request()->result9,
            'result10'=>request()->result10,
            'result11'=>request()->result11,
        ];
        $school_id=auth()->user()->school->id;
        $Exam_Results=Exam_Results::create([
            'grades'=>$new_grades,
            'materials'=>$new_materials,
            'month'=>$request->month,
            'school_id'=>$school_id,
            'category_id'=>$student->category->id,
            'student_id'=>$student->id
        ]);

        Toastr::success('تم إضافة درجة الطلاب بنجاح');
        return redirect()->back();
    }

    public function search(Request $request, Category $category){
        if($category->school_id == auth()->user()->school->id){
            $students=$category->students()->where('name','like','%'.$request->search.'%')
            ->get();
            $ExamResults=$students['0']->ExamResults()->paginate(10);
            $search=$request->search;
            return view('ExamResults.show',compact('ExamResults','category','search'));
        }
        return view('shared.error');
    }

    public function search1(Request $request, Category $category){
        if($category->school_id == auth()->user()->school->id){
            $students=$category->students()->where('name','like','%'.$request->search.'%')
            ->paginate(10);
            $search=$request->search;
            return view('ExamResults.create',compact('students','category','search'));
        }
        return view('shared.error');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Exam_Results  $exam_Results
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $ExamResults=$category->ExamResults()->paginate(10);
        if($category->school_id == auth()->user()->school->id){
            return view('ExamResults.show',compact('category','ExamResults'));
        }
        return view('shared.error');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Exam_Results  $exam_Results
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam_Results $examresults)
    {
        if($examresults->school_id == auth()->user()->school->id){
            $category=$examresults->category;
            return view('examresults.edit',compact('examresults','category'));
        }
        return view('shared.error');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateExam_ResultsRequest  $request
     * @param  \App\Models\Exam_Results  $exam_Results
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam_Results $examresults)
    {
        $validateDate=request()->validate([
            'lesson1'=>'required',
            'lesson2'=>'required',
            'lesson3'=>'required',
            'lesson4'=>'required',
            'lesson5'=>'required',
            'lesson6'=>'required',
            'result1'=>'required',
            'result2'=>'required',
            'result3'=>'required',
            'result4'=>'required',
            'result5'=>'required',
            'result6'=>'required',
            'month'=>'required',
        ]);   
           
        $edit_materials=[
            'lesson1'=>request()->lesson1,
            'lesson2'=>request()->lesson2,
            'lesson3'=>request()->lesson3,
            'lesson4'=>request()->lesson4,
            'lesson5'=>request()->lesson5,
            'lesson6'=>request()->lesson6,
            'lesson7'=>request()->lesson7,
            'lesson8'=>request()->lesson8,
            'lesson9'=>request()->lesson9,
            'lesson10'=>request()->lesson10,
            'lesson11'=>request()->lesson11,
        ];
        $edit_grades=[
            'result1'=>request()->result1,
            'result2'=>request()->result2,
            'result3'=>request()->result3,
            'result4'=>request()->result4,
            'result5'=>request()->result5,
            'result6'=>request()->result6,
            'result7'=>request()->result7,
            'result8'=>request()->result8,
            'result9'=>request()->result9,
            'result10'=>request()->result10,
            'result11'=>request()->result11,
        ];
        $Exam_Results=Exam_Results::find($examresults->id)->update([
            'grades'=>$edit_grades,
            'materials'=>$edit_materials,
            'month'=>$request->month,
        ]);
        Toastr::success('تم تعديل درجة الطالب بنجاح');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Exam_Results  $exam_Results
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam_Results $examresults)
    {
        if($examresults->school_id == auth()->user()->school->id){
            $examresults->delete();
            Toastr::success('تم الحذف بنجاح');
            return redirect()->back();
        }
        return view('shared.error');
    }
}
