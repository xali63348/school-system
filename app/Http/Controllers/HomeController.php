<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Classes;
use App\Models\Student;
use App\Models\Teachers;
use App\Models\User;
use Brian2694\Toastr\Facades\Toastr;

class HomeController extends Controller
{
    public function home(){
        return view('home');
    }
    public function dashboard(){
        $count_classes=0;
        $count_category=0;
        $count_teachers=0;
        $count_tables=0;
        $count_notifications=0;
        $count_absences=0;
        $count_students=0;
        $count_exam_schedule=0;
        $count_exam_results=0;

        if(isset(auth()->user()->school->classes)){
            $classes=auth()->user()->school->classes;
            foreach($classes as $classes){ 
                $count= count(array($classes->id));
                $count_classes=$count + $count_classes;
            }
        }

        if(isset(auth()->user()->school->category)){
            $category=auth()->user()->school->category;
            foreach($category as $category){
                $count= count(array($category->id));
                $count_category=$count + $count_category;
            }
        }
        
        if(isset(auth()->user()->school->teachers)){
            $teachers=auth()->user()->school->teachers;
            foreach($teachers as $teachers){
                $count= count(array($teachers->id));
                $count_teachers=$count + $count_teachers;
            }
        }
        if(isset(auth()->user()->school->tables)){
            $tables=auth()->user()->school->tables;
            foreach($tables as $tables){
                $count= count(array($tables->id));
                $count_tables=$count + $count_tables;
            }
        }
        if(isset(auth()->user()->school->notifications)){
            $notifications=auth()->user()->school->notifications;
            foreach($notifications as $notific){
                $count= count(array($notific->id));
                $count_notifications=$count + $count_notifications;
            }
        }
        if(isset(auth()->user()->school->absences)){
            $absences=auth()->user()->school->absences;
            foreach($absences as $absence){
                $count= count(array($absence->id));
                $count_absences=$count + $count_absences;
            }
        }

        if(isset(auth()->user()->school->students)){
            $students=auth()->user()->school->students;
            foreach($students as $student){
                $count= count(array($student->id));
                $count_students=$count + $count_students;
            }
        }
        if(isset(auth()->user()->school->ExamSchedules)){
            $exam_schedules=auth()->user()->school->ExamSchedules;
            foreach($exam_schedules as $exam_schedule){
                $count= count(array($exam_schedule->id));
                $count_exam_schedule=$count + $count_exam_schedule;
            }
        }
        if(isset(auth()->user()->school->ExamResults)){
            $exam_results=auth()->user()->school->ExamResults;
            foreach($exam_results as $exam_result){
                $count= count(array($exam_result->id));
                $count_exam_results=$count + $count_exam_results;
            }
        }
        return view('dashboard',compact('count_classes','count_category','count_teachers','count_tables','count_notifications','count_absences','count_students','count_exam_schedule','count_exam_results'));
    }
    
    //serach student vistor
    public function search(Request $request){
        $validatedData=request()->validate([
            'search'=>'required|min:6',
        ]);
       
        $student=Student::where('number_code','=',$request->search)->first();
        if(isset($student->name)){
            $inf_student=session()->has('inf_student') ? session('inf_student'):'';
            if(!empty($inf_student)){
                $inf_student='';
            }
            $inf_student=$student;
            session(['inf_student'=>$inf_student]);

            return view('Visitors.show',compact('student'));
        }else{
            Toastr::Warning('عذرا ادخلت كود غير صالح');
            return redirect()->back();
        }
    }
    public function profile(Student $student){
        if(isset(session('inf_student')->id)){
            if($student->id == session('inf_student')->id){
                return view('Visitors.show',compact('student'));
            }
            $id=session('inf_student')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/student/profile/'.$id);
        }
        return redirect("/");
    }
    public function school(Student $student){
        if(isset(session('inf_student')->id)){
            if($student->id == session('inf_student')->id){
                return view('Visitors.profile-school',compact('student'));
            }
            $id=session('inf_student')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/student/school/'.$id);
        }
        return redirect("/");
    }

    public function absences(Student $student){
        if(isset(session('inf_student')->id)){ 
            if($student->id == session('inf_student')->id){
                $absences=$student->absences()->paginate(11);
                return view('Visitors.absences',compact('absences','student'));
            }
            $id=session('inf_student')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/student/absences/'.$id);
        }
        return redirect("/");
    }
    public function notifications(Student $student){
        if(isset(session('inf_student')->id)){
            if($student->id == session('inf_student')->id){
                $notifications=$student->category->notifications()->paginate(3);
                return view('Visitors.notifications',compact('notifications','student'));
            }
            $id=session('inf_student')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/student/notifications/'.$id);
        }
        return redirect("/");
    }

    public function table(Student $student){
        if(isset(session('inf_student')->id)){
            if($student->id == session('inf_student')->id){
                $tables=$student->category->tables;
                return view('Visitors.table',compact('tables','student'));
            }
            $id=session('inf_student')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/student/table/'.$id);
        }
        return redirect("/");
    }

    public function examschedules(Student $student){
        if(isset(session('inf_student')->id)){
            if($student->id == session('inf_student')->id){
                $ExamSchedules=$student->category->ExamSchedules()->paginate(2);
                return view('Visitors.exam-schedules',compact('ExamSchedules','student'));
            }
            $id=session('inf_student')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/student/examschedules/'.$id);
        }
        return redirect("/");
    }

    public function examresults(Student $student){
        if(isset(session('inf_student')->id)){
            if($student->id == session('inf_student')->id){
                $ExamResults=$student->ExamResults()->paginate(8);
                return view('Visitors.exam-results',compact('ExamResults','student'));
            }
            $id=session('inf_student')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/student/examresults/'.$id);
        }
        return redirect("/");
    }
    // end

    //serach teacher vistor
    public function Tsearch(Request $request){
        $validatedData=request()->validate([
            'search'=>'required|min:6',
        ]);
        $teacher=Teachers::where('number_code','=',$request->search)->first();

        if(isset($teacher->name)){
            $inf_teacher=session()->has('inf_teacher') ? session('inf_teacher'):'';
            if(!empty($inf_teacher)){
                $inf_teacher='';
            }
                $inf_teacher=$teacher;
                session(['inf_teacher'=>$inf_teacher]);
                return view('VisitorsTeacher.show',compact('teacher'));
            }else{
            Toastr::Warning('عذرا ادخلت كود غير صالح');
            return redirect()->back();
        }
    }
    public function Tprofile(Teachers $teacher){
        if(isset(session('inf_teacher')->id)){
            if($teacher->id == session('inf_teacher')->id){
                return view('VisitorsTeacher.show',compact('teacher'));
            }
            $id=session('inf_teacher')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/teacher/profile/'.$id);
        }
        return redirect("/");
    }

    public function Tschool(Teachers $teacher){
        if(isset(session('inf_teacher')->id)){
            if($teacher->id == session('inf_teacher')->id){
                return view('VisitorsTeacher.profile-school',compact('teacher'));
            }
            $id=session('inf_teacher')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/teacher/school/'.$id);
        }
        return redirect("/");
    }

    public function Tnotifications(Teachers $teacher){
        if(isset(session('inf_teacher')->id)){
            if($teacher->id == session('inf_teacher')->id){
                return view('VisitorsTeacher.notifications',compact('teacher'));
            }
            $id=session('inf_teacher')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/teacher/notifications/'.$id);
        }
        return redirect("/");
    } 

    public function Ttable(Teachers $teacher){
        if(isset(session('inf_teacher')->id)){
            if($teacher->id == session('inf_teacher')->id){
                return view('VisitorsTeacher.table',compact('teacher'));
            }
            $id=session('inf_teacher')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/teacher/table/'.$id);
        }
        return redirect("/");
    }
    
    public function Texamschedules(Teachers $teacher){
        if(isset(session('inf_teacher')->id)){
            if($teacher->id == session('inf_teacher')->id){
                return view('VisitorsTeacher.exam-schedules',compact('teacher'));
            }
            $id=session('inf_teacher')->id;
            Toastr::error('عذرا ليس لديك هذه الصلاحيات');
            return redirect('/teacher/examschedules/'.$id);
        }
        return redirect("/");
    }
    // end
}
