<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Absence;
use App\Models\Classes;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Classes $classes )
    {
       //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request , Classes $classes)
    {
        
        $validatedDate=request()->validate([
            'name'=>'required|min:1',
            'logo'=>'mimes:png,jpg|max:2000'
        ]);
    
        foreach($classes->Category as $Category){
            if($request->name == $Category->name){
                Toastr::Warning('الشعبه موجوده بالفعل');
                return redirect()->back();
            }
        }
        $path=null;
        if($request->hasFile('logo')){
            $path='/storage/'.$request->file('logo')->store('logos',['disk'=>'public']);
        }
        $school_id=auth()->user()->school->id;
        $category=Category::create([
            'name'=>$request->name,
            'image'=>$path,
            'classes_id'=>$classes->id,
            'school_id'=>$school_id,
        ]);
        Toastr::success('تم إنشاء الشعبه بنجاح');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            $count_students=0;
            $count_absences=0;
            $count_teachers=0;
            $count_tables=0;
            $count_notifications=0;
            $count_ExamSchedule=0;
            $count_ExamResult=0;
            foreach($category->students as $student){ 
                $count= count(array($student->id));
                $count_students=$count + $count_students;

                    foreach($student->absences as $absence){
                        $count_2= count(array($absence->id));
                        $count_absences=$count_2 + $count_absences;
                    }
            }
            foreach($category->teachers as $teacher){ 
                $count_3= count(array($teacher->id));
                $count_teachers=$count_3 + $count_teachers;
            }
            foreach($category->tables as $table){ 
                $count_4= count(array($table->id));
                $count_tables=$count_4 + $count_tables;
            }
            foreach($category->notifications as $notific){ 
                $count_5= count(array($notific->id));
                $count_notifications=$count_5 + $count_notifications;
            }
            foreach($category->ExamSchedules as $ExamSchedule){ 
                $count_6= count(array($ExamSchedule->id));
                $count_ExamSchedule=$count_6 + $count_ExamSchedule;
            }
            foreach($category->ExamResults as $ExamResult){
                $count_7= count(array($ExamResult->id));
                $count_ExamResult=$count_7 + $count_ExamResult;
            }
            return view('categories.show',compact('category','count_students','count_absences','count_teachers','count_tables','count_notifications','count_ExamSchedule','count_ExamResult'));
        }
       return view('shared.error');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function edit(Category $category)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            $validatedDate=request()->validate([
                'name'=>'required|min:1',
                'logoo'=>'mimes:png,jpg,jpeg,bmp|max:2000'
            ]);

            $classes=$category->classes;
            if($request->name == $category->name){
                $path=$category->image;
                if($request->hasFile('logoo')){
                    $path='/storage/'.$request->file('logoo')->store('logos',['disk'=>'public']);
                }
                $edit_category=Category::find($category->id)->update([
                    'name'=>request()->name,
                    'image'=>$path,
                ]);
                Toastr::success('تم تعديل الشعبه بنجاح');
                return redirect()->back();
            }
            foreach($classes->Category as $Category){
                if($request->name == $Category->name){
                    Toastr::Warning('الشعبه موجوده بالفعل');
                    return redirect()->back();
                }
            }
            $path=$category->image;
            if($request->hasFile('logoo')){
                $path='/storage/'.$request->file('logoo')->store('logos',['disk'=>'public']);
            }
            $edit_category=Category::find($category->id)->update([
                'name'=>request()->name,
                'image'=>$path,
            ]);
                Toastr::success('تم تعديل الشعبه بنجاح');
                return redirect()->back();
         }
        // return view('shared.error');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Category  $category
     * @return \Illuminate\Http\Response
     */
    public function destroy(Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            $absences=Absence::where('category_id','=',$category->id)->delete();
            $category->delete();
            Toastr::success('تم الحذف بنجاح');
            return redirect()->back();
        }
            return view('shared.error');
    }
    public function search(Request $request ,Classes $classes){
        if($classes->school_id == auth()->user()->school->id){
            $category=$classes->category()->where('name','like','%'.$request->search.'%')->get();
            return view('classes.show',compact('category','classes'));
        }
        return view('shared.error');
        
    }
}
