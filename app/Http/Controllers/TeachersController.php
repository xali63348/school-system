<?php

namespace App\Http\Controllers;

use App\Models\Teachers;
use App\Models\Category;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class TeachersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            $teachers=auth()->user()->school->teachers;
                return view('teachers.create',compact('category','teachers'));
        }
        return view('shared.error');
    }

    public function add(Request $request ,Category $category){
        if($category->school_id == auth()->user()->school->id){
            $validateData=request()->validate([
                'teachers'=>'required',
            ]);
            $category->teachers()->syncWithoutDetaching($request->teachers);
            Toastr::success('تم اضافة الاستاذ بنجاح');
            return redirect()->back();
        }
        return view('shared.error');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Category $category)
    {
        $validateData=request()->validate([
            'name'=>'required|min:2',
            'middle_name'=>'required|min:2',
            'last_name'=>'required|min:2',
            'country'=>'min:2',
            'city'=>'min:2',
            'area'=>'min:2',
            'number_code'=>'min:6|unique:teachers',
            'phone_number'=>'min:8|unique:teachers',
            'age'=>'min:1',
            'logo'=>'mimes:png,jpg,jpeg,bmp|max:2000'
        ]);
        $path=null;
        if($request->hasFile('logo')){
            $path='/storage/'.$request->file('logo')->store('logos',['disk'=>'public']);
        }
        $address_teachers=[
            'country'=>request()->country,
            'city'=>request()->city,
            'area'=>request()->area,
            'extra'=>request()->extra,
        ];
        $school_id=auth()->user()->school->id;
        $teachers=Teachers::create([
            'name'=>$request->name .' '.$request->middle_name.' '.$request->last_name,
            'number_code'=>$request->number_code,
            'phone_number'=>$request->phone_number,
            'address'=>$address_teachers,
            'age'=>$request->age,
            'images'=>$path,
            'school_id'=>$school_id,
           
        ]);
        $teachers->category()->attach($category->id);
        Toastr::success('تم اضافة الاستاذ بنجاح');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            $teachers=$category->teachers()->paginate(8);
            return view('teachers.show',compact('category','teachers'));
        }
        return view('shared.error');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function search(Request $request ,Category $category){
        if($category->school_id == auth()->user()->school->id){
            $teachers=$category->teachers()->where('name','like','%'.$request->search.'%')
            ->paginate(8);
            $search=$request->search;
            return view('teachers.show',compact('search','teachers','category'));
        }
        return view('shared.error');
    }

    public function edit(Teachers $teacher,Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            return view('teachers.edit',compact('teacher','category'));
        }
        return view('shared.error');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teachers $teacher)
    {
        $validateData=request()->validate([
            'name'=>'required|min:2',
            'country'=>'min:2',
            'city'=>'min:2',
            'area'=>'min:2',
            'number_code'=>'min:6|unique:teachers,number_code,'.$teacher->id,
            'phone_number'=>'min:8|unique:teachers,phone_number,'.$teacher->id,
            'age'=>'min:1',
            'logo'=>'mimes:png,jpg,jpeg,bmp|max:2000'
        ]);
        $path=$teacher->images;
        if($request->hasFile('logo')){
            $path='/storage/'.$request->file('logo')->store('logos',['disk'=>'public']);
        }

        $address_teachers=[
            'country'=>request()->country,
            'city'=>request()->city,
            'area'=>request()->area,
            'extra'=>request()->extra,
        ];
        $teachers=Teachers::find($teacher->id)->update([
            'name'=>$request->name .' '.$request->middle_name.' '.$request->last_name,
            'number_code'=>$request->number_code,
            'phone_number'=>$request->phone_number,
            'address'=>$address_teachers,
            'age'=>$request->age,
            'images'=>$path,
        ]);
        Toastr::success('تم تعديل على الاستاذ بنجاح');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teachers  $teachers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teachers $teacher, Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            $teacher->category()->detach($category->id);
            Toastr::success('تم الحذف بنجاح');
            return redirect()->back();
        }
        return view('shared.error');
    }
    public function permanently(Teachers $teacher, Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            $delete_permanently=Teachers::where('id','=',$teacher->id)->delete();
            Toastr::success('تم الحذف بنجاح');
            return redirect()->back();
        }
        return view('shared.error');
    }
}
