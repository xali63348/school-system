<?php

namespace App\Http\Controllers;

use App\Models\School;
use App\Models\User;
use App\Models\Absence;
use App\Models\Teachers;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class SchoolController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        if(empty(auth()->user()->school)){
            return view('schools.create');
        }
        Toastr::Warning('لديك مدرسة بالفعل لا يمكنك إنشاء مدرستين');
        return redirect()->back();
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $validatedDate=request()->validate([
            'name'=>'required|min:3|unique:schools',
            'manager'=>'required|min:3',
            'country'=>'required|min:3',
            'city'=>'required|min:3',
            'area'=>'required|min:3',
        ]);
        $address_school=[
            'country'=>request()->country,
            'city'=>request()->city,
            'area'=>request()->area,
            'extra'=>request()->extra,
        ];
        $school=School::create([
            'name'=>$request->name,
            'manager'=>$request->manager,
            'address'=>$address_school,
        ]);
       
        User::find(auth()->user()->id)->update([
            'school_id'=>$school->id,
        ]);
        Toastr::success('تم انشاء المدرسة بنجاح');
        return redirect('/home');
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function show()
    {
        $classes=auth()->user()->school->classes;
        $school=auth()->user()->school;
        return view('schools.show',compact('classes','school'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function edit()
    {
        $school=auth()->user()->school;
        return view('schools.edit-profile',compact('school'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */

    public function update(Request $request,School $school)
    {
        $validatedData=request()->validate([
            'name'=>'required|min:3|unique:schools,name,'.$school->id,
            'manager'=>'required|min:3',
            'country'=>'required|min:3',
            'city'=>'required|min:3',
            'area'=>'required|min:3',
            'logo'=>'mimes:png,jpg,jpeg,bmp|max:2000',
            'logoo'=>'mimes:png,jpg,jpeg,bmp|max:2000',
        ]);

        $path=$school->image;
        $pathB=$school->background;
        
        if($request->hasFile('logo')){
            $path='/storage/'.$request->file('logo')->store('logos',['disk'=>'public']);
        }
        if($request->hasFile('logoo')){
            $pathB='/storage/'.$request->file('logoo')->store('logos',['disk'=>'public']);
        }
        $edit_address_school=[
            'country'=>request()->country,
            'city'=>request()->city,
            'area'=>request()->area,
            'extra'=>request()->extra,
        ];
        $edit_school=School::find($school->id)->update([
            'name'=>$request->name,
            'manager'=>$request->manager,
            'address'=>$edit_address_school,
            'image'=>$path,
            'background'=>$pathB,
        ]);
        Toastr::success('تم تعديل على الملف الشخصي بنجاح');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\School  $school
     * @return \Illuminate\Http\Response
     */
    public function date(){
        $count_classes=0;
        $count_category=0;
        $count_teachers=0;
        $count_tables=0;
        $count_notifications=0;
        $count_absences=0;
        $count_students=0;
        $count_exam_schedule=0;
        $count_exam_results=0;

        if(isset(auth()->user()->school->classes)){
            $classes=auth()->user()->school->classes;
            foreach($classes as $classes){
                $count= count(array($classes->id));
                $count_classes=$count + $count_classes;
            }
        }

        if(isset(auth()->user()->school->category)){
            $category=auth()->user()->school->category;
            foreach($category as $category){
                $count= count(array($category->id));
                $count_category=$count + $count_category;
            }
        }

        if(isset(auth()->user()->school->teachers)){
            $teachers=auth()->user()->school->teachers;
            foreach($teachers as $teachers){
                $count= count(array($teachers->id));
                $count_teachers=$count + $count_teachers;
            }
        }
        if(isset(auth()->user()->school->tables)){
            $tables=auth()->user()->school->tables;
            foreach($tables as $tables){
                $count= count(array($tables->id));
                $count_tables=$count + $count_tables;
            }
        }
        if(isset(auth()->user()->school->notifications)){
            $notifications=auth()->user()->school->notifications;
            foreach($notifications as $notific){
                $count= count(array($notific->id));
                $count_notifications=$count + $count_notifications;
            }
        }
        if(isset(auth()->user()->school->absences)){
            $absences=auth()->user()->school->absences;
            foreach($absences as $absence){
                $count= count(array($absence->id));
                $count_absences=$count + $count_absences;
            }
        }

        if(isset(auth()->user()->school->students)){
            $students=auth()->user()->school->students;
            foreach($students as $student){
                $count= count(array($student->id));
                $count_students=$count + $count_students;
            }
        }
        if(isset(auth()->user()->school->ExamSchedules)){
            $exam_schedules=auth()->user()->school->ExamSchedules;
            foreach($exam_schedules as $exam_schedule){
                $count= count(array($exam_schedule->id));
                $count_exam_schedule=$count + $count_exam_schedule;
            }
        }
        if(isset(auth()->user()->school->ExamResults)){
        $exam_results=auth()->user()->school->ExamResults;
            foreach($exam_results as $exam_result){
                $count= count(array($exam_result->id));
                $count_exam_results=$count + $count_exam_results;
            }
        }
        return view('schools.date',compact('count_classes','count_category','count_teachers','count_tables','count_notifications','count_absences','count_students','count_exam_schedule','count_exam_results'));
    }
    
    public function destroy()
    {
        $school=auth()->user()->school;
        $absences=Absence::where('school_id','=',$school->id)->delete();
        $teachers=Teachers::where('school_id','=',$school->id)->delete();
        $school->delete();
        Toastr::success('تم حذف المدرسة بنجاح');
        return redirect('/home');
        
    }
}
