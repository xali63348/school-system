<?php

namespace App\Http\Controllers;

use App\Models\Absence;
use App\Models\Category;
use App\Models\Student;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Pagination\LengthAwarePaginator;

class AbsenceController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        $students=$category->students()->paginate(10);
        if($category->school_id == auth()->user()->school->id){
            return view('absences.create',compact('category','students'));
        }
        return view('shared.error');
        
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request ,Student $student)
    {
            $validatedDate=request()->validate([
                'quantity0'=>'required',
                'quantity1'=>'required',
                'quantity2'=>'required',
                'quantity3'=>'required',
                'quantity4'=>'required',
                'quantity5'=>'required',
                'quantity6'=>'required',
                'date'=>'required',
            ]);
                if($request->quantity0 == '--' && $request->quantity1 == '--' && $request->quantity2 == '--' && $request->quantity3 == '--' && $request->quantity4 == '--' && $request->quantity5 == '--' && $request->quantity6 == '--'){
                   Toastr::Warning('عذرًا ، لا يمكنك إكمال العملية بدون إضافة غيابات للطالب');
                   return redirect()->back();
            }

            $days_of_absence=[
                    'lesson1'=>request()->quantity0,
                    'lesson2'=>request()->quantity1,
                    'lesson3'=>request()->quantity2,
                    'lesson4'=>request()->quantity3,
                    'lesson5'=>request()->quantity4,
                    'lesson6'=>request()->quantity5,
                    'allLasson'=>request()->quantity6,
                ];

                foreach($student->absences as $absence){
                    if(request()->date == $absence->date){
                        Toastr::Error('لا يمكنك إضافة الطالب إلى جدول الغياب مرتين في اليوم');
                        return redirect()->back();
                    }
                }
            $school_id=auth()->user()->school->id;
            $category_id=$student->category->id;
            $Absence=Absence::create([
                'days_of_absence'=>$days_of_absence,
                'date'=>request()->date,
                'student_id'=>$student->id,
                'update_data'=>'لايوجد تحديث',
                'category_id'=>$category_id,
                'school_id'=>$school_id,
            ]);
            Toastr::success('تمت إضافة الطالب إلى الغياب');
            return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Absence  $absence
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        $absences= $category->absences()->paginate(10);
        if($category->school_id == auth()->user()->school->id){
            return view('absences.show',compact('absences','category'));
        }
        return view('shared.error');
    }

    public function search(Request $request ,Category $category){

        if($category->school_id == auth()->user()->school->id){
            $students=$category->students()->where('name','like','%'.$request->search.'%')
            ->get();
            $absences=$students['0']->absences()->paginate(10);
            $search=$request->search;
            return view('absences.show',compact('absences','category','search'));
        }
        return view('shared.error');
    }

    public function search1(Request $request ,Category $category){

        if($category->school_id == auth()->user()->school->id){
            $students=$category->students()->where('name','like','%'.$request->search.'%')
            ->paginate(10);
            $search=$request->search;
            return view('absences.create',compact('students','category','search'));
        }
        return view('shared.error');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Absence  $absence
     * @return \Illuminate\Http\Response
     */
    public function edit(Absence $absence)
    {
        $absence=Absence::find($absence->id);
            if($absence->school_id == auth()->user()->school->id){
                return view('absences.edit',compact('absence'));
            }
        return view('shared.error');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Absence  $absence
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Absence $absence)
    {

        $validatedDate=request()->validate([
            'quantity0'=>'required',
            'quantity1'=>'required',
            'quantity2'=>'required',
            'quantity3'=>'required',
            'quantity4'=>'required',
            'quantity5'=>'required',
            'quantity6'=>'required',
            'update_data'=>'required',
        ]);
        if($request->quantity0 == '--' && $request->quantity1 == '--' && $request->quantity2 == '--' && $request->quantity3 == '--' && $request->quantity4 == '--' && $request->quantity5 == '--' && $request->quantity6 == '--'){
            Toastr::Warning('عذرًا ، لا يمكنك إكمال العملية بدون إضافة غيابات للطالب');
            return redirect()->back();
        }
        $update_days_of_absence=[
            'lesson1'=>request()->quantity0,
            'lesson2'=>request()->quantity1,
            'lesson3'=>request()->quantity2,
            'lesson4'=>request()->quantity3,
            'lesson5'=>request()->quantity4,
            'lesson6'=>request()->quantity5,
            'allLasson'=>request()->quantity6,
        ];
        $id=$absence->students->category->id;

        $absence=Absence::find($absence->id)->update([
            'days_of_absence'=>$update_days_of_absence,
            'update_data'=>request()->update_data,
        ]);
        Toastr::success('تم تعديل غيابات الطالب بنجاح');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Absence  $absence
     * @return \Illuminate\Http\Response
     */
    public function destroy(Absence $absence)
    {
        //
    }
}
