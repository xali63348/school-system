<?php

namespace App\Http\Controllers;

use App\Models\User;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Support\Facades\Hash;
use Illuminate\Http\Request;

class UserController extends Controller
{
    public function login(){
        if(auth()->check()){
            return redirect('/home');
        }
        return view('users.login');
    }
    
    public function logintry(){
        $validatedData=request()->validate([
            'email'=>'required',
            'password'=>'required|min:8'
        ]);
        if(auth()->attempt(['email'=>request()->email, 'password'=>request()->password])){
            
            $today=strtotime(date("Y-m-d"));
            $date=strtotime(auth()->user()->date);
                if($today != $date){
                    $days=(int)(($today - $date)/86400);
                    $status=auth()->user()->status;
                    $status_status=$status-$days;
                    $update_date=User::find(auth()->user()->id)->update([
                    'date'=>date("Y-m-d"),
                    'status'=>$status_status,
                ]);
                return redirect('/home');
            }
            return redirect('/home');
        }else{
            Toastr::error('لقد أدخلت بريدًا إلكترونيًا أو كلمة مرور خاطئة');
            return redirect()->back();
        }
    }
    public function logout(){ //دالة لمسح الجلسة
        auth()->logout();
        session()->flush();
        return redirect('/');
    }
    public function store(){
        return view('users.register');
    }
    public function create(Request $request){
        $validatedData=request()->validate([
            'name'=>'required|min:3',
            'email'=>'required|unique:users',
            'number_phone'=>'required|min:8|unique:users',
            'password'=>'required|min:8',
            'confirm'=>'required|same:password',
            'code'=>'required',
        ]);
        if($request->code =='ss-xali63348'){
            $newUser=User::create([
                'name'=>$request->name,
                'email'=>$request->email,
                'number_phone'=>$request->number_phone,
                'password'=>bcrypt(request()->password),
                'email_verified_at'=>now(),
                'status'=>365,
                'date'=>date("Y-m-d"),
            ]);
            if(auth()->attempt(['email'=>request()->email,'password'=>request()->password])){ //التاكد من البريد و الباسورد لتحويل الى صفحة الرئسيه
                return redirect('/schools/create');
            }
            return redirect('/');
        }else{
            Toastr::Warning('عذرا ادخلت كود غير صالح');
            return redirect()->back();
        }
    }
    public function profile(){
        return view('users.profile');
    }
    

    public function editname(Request $request,User $user){
        if($user->id == auth()->user()->id){
            $validatedData=request()->validate([
                'name'=>'required',
               'password'=>'required|min:8',
            ]);
            if(Hash::check(request()->get('password'),auth()->user()->password)){
                $edit_user=User::find($user->id)->update([
                    'name'=>$request->name,
                ]);
                  Toastr::Success('تم تعديل الاسم بنجاح');
                  return redirect()->back();
            }else{
                Toastr::Warning('لقد أدخلت كلمة مرور خاطئة');
                return redirect()->back();
            }
        }
        return view('shared.error');
    }
    public function numberphone(Request $request,User $user){
        if($user->id == auth()->user()->id){
            $validatedData=request()->validate([
                'number'=>'required|min:8|unique:users,number_phone,'.$user->id,
                'password'=>'required|min:8',
            ]);
            if(Hash::check(request()->get('password'),auth()->user()->password)){
                $edit_user=User::find($user->id)->update([
                'number_phone'=>$request->number,
            ]);
                Toastr::Success('تم تعديل رقم الهاتف بنجاح');
                return redirect()->back();
            }else{
            Toastr::Warning('لقد أدخلت كلمة مرور خاطئة');
                return redirect()->back();
            }
        }
        return view('shared.error');
    }

    public function email(Request $request,User $user){
        if($user->id == auth()->user()->id){
            $validatedData=request()->validate([
                'email'=>'required|unique:users,email,'.$user->id,
                'password'=>'required|min:8',
            ]);
            if(Hash::check(request()->get('password'),auth()->user()->password)){
                $edit_user=User::find($user->id)->update([
                'email'=>$request->email,
            ]);
                Toastr::Success('تم تعديل البريد الإلكتروني بنجاح');
                return redirect()->back();
            }else{
            Toastr::Warning('لقد أدخلت كلمة مرور خاطئة');
                return redirect()->back();
            }
        }
        return view('shared.error');
    }

    public function password(Request $request,User $user){
        if($user->id == auth()->user()->id){
            $validatedData=request()->validate([
                'password'=>'required|min:8',
                'newpassword'=>'required|min:8',
                'confirm'=>'required|same:newpassword'
            ]);
            if(Hash::check(request()->get('password'),auth()->user()->password)){
                $edit_user=User::find($user->id)->update([
                'password'=>bcrypt(request()->newpassword),
            ]);
                Toastr::Success('تم تعديل كلمة المرور بنجاح');
                return redirect()->back();
            }else{
            Toastr::Warning('لقد أدخلت كلمة مرور خاطئة');
                return redirect()->back();
            }
        }
        return view('shared.error');
    }
    public function status(){
        return view('users.status');
    }
    
}
