<?php

namespace App\Http\Controllers;

use App\Models\Teachers_categories;
use Illuminate\Http\Request;

class TeachersCategoriesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Teachers_categories  $teachers_categories
     * @return \Illuminate\Http\Response
     */
    public function show(Teachers_categories $teachers_categories)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Teachers_categories  $teachers_categories
     * @return \Illuminate\Http\Response
     */
    public function edit(Teachers_categories $teachers_categories)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Teachers_categories  $teachers_categories
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Teachers_categories $teachers_categories)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Teachers_categories  $teachers_categories
     * @return \Illuminate\Http\Response
     */
    public function destroy(Teachers_categories $teachers_categories)
    {
        //
    }
}
