<?php

namespace App\Http\Controllers;

use App\Models\Exam_Schedule;
use App\Models\Category;
use Illuminate\Http\Request;
use Brian2694\Toastr\Facades\Toastr;
use App\Http\Requests\StoreExam_ScheduleRequest;
use App\Http\Requests\UpdateExam_ScheduleRequest;

class ExamScheduleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create(Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            return view('ExamSchedules.create',compact('category'));
        }
        return view('shared.error');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \App\Http\Requests\StoreExam_ScheduleRequest  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,Category $category)
    {
        $validateDate=request()->validate([
            'lesson1'=>'required',
            'lesson2'=>'required',
            'lesson3'=>'required',
            'lesson4'=>'required',
            'lesson5'=>'required',
            'lesson6'=>'required',
            'date1'=>'required',
            'date2'=>'required',
            'date2'=>'required',
            'date4'=>'required',
            'date5'=>'required',
            'date6'=>'required',
            'day1'=>'required',
            'day2'=>'required',
            'day3'=>'required',
            'day4'=>'required',
            'day5'=>'required',
            'day6'=>'required',
            'month'=>'required',
        ]);
        $new_date=[
            'date1'=>request()->date1,
            'date2'=>request()->date2,
            'date3'=>request()->date3,
            'date4'=>request()->date4,
            'date5'=>request()->date5,
            'date6'=>request()->date6,
            'date7'=>request()->date7,
            'date8'=>request()->date8,
            'date9'=>request()->date9,
            'date10'=>request()->date10,
            'date11'=>request()->date11,
            'date12'=>request()->date12,
        ];
        $new_materials=[
            'lesson1'=>request()->lesson1,
            'lesson2'=>request()->lesson2,
            'lesson3'=>request()->lesson3,
            'lesson4'=>request()->lesson4,
            'lesson5'=>request()->lesson5,
            'lesson6'=>request()->lesson6,
            'lesson7'=>request()->lesson7,
            'lesson8'=>request()->lesson8,
            'lesson9'=>request()->lesson9,
            'lesson10'=>request()->lesson10,
            'lesson11'=>request()->lesson11,
            'lesson12'=>request()->lesson12,
        ];
        $new_days=[
            'day1'=>request()->day1,
            'day2'=>request()->day2,
            'day3'=>request()->day3,
            'day4'=>request()->day4,
            'day5'=>request()->day5,
            'day6'=>request()->day6,
            'day7'=>request()->day7,
            'day8'=>request()->day8,
            'day9'=>request()->day9,
            'day10'=>request()->day10,
            'day11'=>request()->day11,
            'day12'=>request()->day12,
        ];
        $school_id=auth()->user()->school->id;
        $Exam_Schedule=Exam_Schedule::create([
            'days'=>$new_days,
            'materials'=>$new_materials,
            'date'=>$new_date,
            'school_id'=>$school_id,
            'category_id'=>$category->id,
            'month'=>$request->month,
        ]);
        Toastr::success('تم انشاء جدول الامتحانات بنجاح');
        return redirect()->back();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Exam_Schedule  $exam_Schedule
     * @return \Illuminate\Http\Response
     */
    public function show(Category $category)
    {
        if($category->school_id == auth()->user()->school->id){
            $ExamSchedules=$category->ExamSchedules()->paginate(2);
            return view('ExamSchedules.show',compact('category','ExamSchedules'));
        }
        return view('shared.error');
    }
    public function search(Request $request,Category $category){
        if($category->school_id == auth()->user()->school->id){
            $ExamSchedules=$category->ExamSchedules()->where('month','like','%'.$request->search.'%')
            ->paginate(2);
            $search=$request->search;
            return view('ExamSchedules.show',compact('ExamSchedules','category','search'));
        }
        return view('shared.error');
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Exam_Schedule  $exam_Schedule
     * @return \Illuminate\Http\Response
     */
    public function edit(Exam_Schedule $examschedule)
    {
        if($examschedule->school_id == auth()->user()->school->id){
            $category=$examschedule->category;
            return view('ExamSchedules.edit',compact('category','examschedule'));
        }
        return view('shared.error');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \App\Http\Requests\UpdateExam_ScheduleRequest  $request
     * @param  \App\Models\Exam_Schedule  $exam_Schedule
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Exam_Schedule $examschedule)
    {
        $validateDate=request()->validate([
            'lesson1'=>'required',
            'lesson2'=>'required',
            'lesson3'=>'required',
            'lesson4'=>'required',
            'lesson5'=>'required',
            'lesson6'=>'required',
            'date1'=>'required',
            'date2'=>'required',
            'date2'=>'required',
            'date4'=>'required',
            'date5'=>'required',
            'date6'=>'required',
            'day1'=>'required',
            'day2'=>'required',
            'day3'=>'required',
            'day4'=>'required',
            'day5'=>'required',
            'day6'=>'required',
            'month'=>'required',
        ]);
        $edit_date=[
            'date1'=>request()->date1,
            'date2'=>request()->date2,
            'date3'=>request()->date3,
            'date4'=>request()->date4,
            'date5'=>request()->date5,
            'date6'=>request()->date6,
            'date7'=>request()->date7,
            'date8'=>request()->date8,
            'date9'=>request()->date9,
            'date10'=>request()->date10,
            'date11'=>request()->date11,
            'date12'=>request()->date12,
        ];
        $edit_materials=[
            'lesson1'=>request()->lesson1,
            'lesson2'=>request()->lesson2,
            'lesson3'=>request()->lesson3,
            'lesson4'=>request()->lesson4,
            'lesson5'=>request()->lesson5,
            'lesson6'=>request()->lesson6,
            'lesson7'=>request()->lesson7,
            'lesson8'=>request()->lesson8,
            'lesson9'=>request()->lesson9,
            'lesson10'=>request()->lesson10,
            'lesson11'=>request()->lesson11,
            'lesson12'=>request()->lesson12,
        ];
        $edit_days=[
            'day1'=>request()->day1,
            'day2'=>request()->day2,
            'day3'=>request()->day3,
            'day4'=>request()->day4,
            'day5'=>request()->day5,
            'day6'=>request()->day6,
            'day7'=>request()->day7,
            'day8'=>request()->day8,
            'day9'=>request()->day9,
            'day10'=>request()->day10,
            'day11'=>request()->day11,
            'day12'=>request()->day12,
        ];
        $id=$examschedule->category->id;
        $Exam_Schedule=Exam_Schedule::find($examschedule->id)->update([
            'days'=>$edit_days,
            'materials'=>$edit_materials,
            'date'=>$edit_date,
            'month'=>$request->month,
        ]);
        Toastr::success('تم التعديل على جدول الامتحانات بنجاح');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Exam_Schedule  $exam_Schedule
     * @return \Illuminate\Http\Response
     */
    public function destroy(Exam_Schedule $examschedule)
    {
        if($examschedule->school_id == auth()->user()->school->id){
            $examschedule->delete();
             Toastr::success('تم الحذف بنجاح');
             return redirect()->back();
        }
        return view('shared.error');
    }
}
