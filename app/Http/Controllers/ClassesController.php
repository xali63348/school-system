<?php

namespace App\Http\Controllers;

use App\Models\Classes;
use App\Models\School;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;

class ClassesController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request,School $school)
    {
        $validatedDate=request()->validate([
            'name'=>'required|min:4',
        ]);
        foreach($school->classes as $class){ 
            if($request->name == $class->name){
                Toastr::Warning('الصف موجود بالفعل');
                return redirect()->back();
            }
        }
        $newClass=Classes::create([
            'name'=>$request->name,
            'school_id'=>$school->id,
        ]);
        Toastr::success('تم اضافة الصف بنجاح');
        return redirect()->back();
    
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function show(Classes $classes)
    {
        if($classes->school_id == auth()->user()->school->id){  
            $category=$classes->category;
            return view('classes.show',compact('classes','category'));
        }
        
        return view('shared.error');

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function edit(Classes $classes)
    {
        if($classes->school_id == auth()->user()->school->id){
            return view('classes.edit',compact('classes'));
        }
        return view('shared.error');
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Classes $classes)
    {
        $validatedDate=request()->validate([
            'name'=>'required|min:4',
        ]);
        foreach(auth()->user()->school->classes as $class){
            if($request->name == $class->name){
                Toastr::Warning('الصف موجود بالفعل');
                return redirect()->back();
            }
        }
        $editClass=Classes::find($classes->id)->update([
            'name'=>$request->name,
        ]);
        Toastr::success('تم تعديل الصف بنجاح');
        return redirect()->back();
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Classes  $classes
     * @return \Illuminate\Http\Response
     */
    public function destroy(Classes $classes)
    {
        if($classes->school_id == auth()->user()->school->id){    
            $classes->delete();
            Toastr::success('تم الحذف بنجاح');
            return redirect()->back();
        }
        return view('shared.error');
    }
    public function search(Request $request,School $school){
        if($school->id == auth()->user()->school->id){ 
            $classes=$school->classes()->where('name','like','%'.$request->search.'%')->get();
            return view('schools.show',compact('classes','school'));
        }
        return view('shared.error');
    }
}
